package Queues;

class Doubly_Queue{
	
	int size;
	int arr[];
	int front;
	int rear;
	
	public Doubly_Queue(int size) {
		this.size = size;
		arr = new int[size];
		front = -1;
		rear = -1;
	}

	public void push_back(int data) {
		
		if((rear == size -1 && front == 0) ||
				(rear == (front-1) % (size-1)) ) {
			System.out.println("Queue is full.");
			return;
		}
		
		if(front == -1 && rear ==-1) { //first element to push
			front = rear = 0;
		}
		else if(rear == size-1 && front !=0 ) {
			rear = 0;
		}else {
			rear++;
		}
		
		arr[rear] = data;
	}
	
	public void push_front(int data) {
		
		if(( front == 0 && rear == size -1) ||
				(front !=0 && rear == (front-1) % (size-1))) { //queue full checks
			System.out.println("Queue is full.");
			return;
		}
		
		if(front == -1 && rear ==-1) { //first element to push
			front = rear = 0;
		}
		else if(front == 0 && rear != size-1 ) { //to maintain cyclic motion
			front = size-1;
		}else {
			front--;
		}
		
		arr[front] = data;
	}
	
	public int pop_front() {
		if(front == -1) {
			System.out.println("Queue is empty.");
			return -1;
		}
		
		int ans = arr[front];
		arr[front] = -1;
		
		if(front == rear) { // last element to pop so setting to new position
			front = rear = -1;
		}else if(front == size -1 ) { //to maintain cyclic motion
			front = 0;
		}else { //normal condition
			front++;
		} 
		
		return ans;
	}
	
	public int pop_back() {
		if(front == -1) {
			System.out.println("Queue is empty.");
			return -1;
		}
		
		int ans = arr[rear];
		arr[rear] = -1;
		
		if(front == rear) { // last element to pop so setting to new position
			front = rear = -1;
		}else if(rear == 0 && front != size-1 ) { //to maintain cyclic motion
			rear = size-1;
		}else { //normal condition
			rear--;
		} 
		
		return ans;
	}
	
	public void print() {
//		while(front!=rear) {
//			if(front == size-1)
//				front = 0;
//			System.out.print(arr[front] +" ");
//			front++;
//		}
	}
	
}



public class Doubly_ended_queue_using_array {

	public static void main(String[] args) {
		Doubly_Queue dq = new Doubly_Queue(3);
		
		dq.push_back(1);
		dq.push_back(2);
		dq.push_back(3);
		System.out.println("front : "+dq.front +" rear: "+dq.rear );
		dq.pop_front();
		dq.print();
		System.out.println("front : "+dq.front +" rear: "+dq.rear );
		dq.push_back(1);
		System.out.println("front : "+dq.front +" rear: "+dq.rear );
		dq.print();
		dq.push_back(2);
		
		dq.pop_front();
		dq.pop_front();
		dq.pop_front();
		dq.pop_front();
		

		dq.print();
	}

}
