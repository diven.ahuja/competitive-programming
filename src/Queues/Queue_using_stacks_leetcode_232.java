package Queues;

import java.util.Stack;

class MyQueue {

    Stack<Integer> s;
    Stack<Integer> d;
    
    public MyQueue() {
        s = new Stack<>();
        d = new Stack<>();
    }
    
    public void push(int x) {
        s.push(x);
        d.push(x);
    }
    
    public int pop() {
        d = new Stack<Integer>();
        int ans = 0;
        int size = s.size();
        for(int i=0 ; i<size-1; i++){
            d.push(s.pop());
        }
        
        if(!empty()){
            ans = s.peek();
        }else if(d.size() == 1){
        	ans = d.peek();
        	return ans;
        }
        
        s = new Stack<Integer>();
        // saving length and using this in for loop
        // because while pop size keeps on changing
        int length = d.size();
//        because we can't assign like this : new Stack<>(d);
        for(int i=0; i<length; i++) {
        	s.push(d.pop());
        }

        return ans;
    }
    
    public int peek() {
        int top = 0;
        int size = s.size();
        d = new Stack<Integer>();
        for(int i=0 ; i<size-1; i++){
            d.push(s.pop());
        }
        if(!empty()){
            top = s.pop();
            
            // without below line, last element was missing out
            d.push(top);
        }

        s = new Stack<Integer>();
        // saving length and using this in for loop
        // because while pop size keeps on changing
        int length = d.size();
        // because we can't assign like this : new Stack<>(d);
        for(int i=0; i<length; i++)
            s.push(d.pop());
        
        return top;
    }
    
    public boolean empty() {
        if(s.size() == 0)
            return true;
        return false;
    }
}
public class Queue_using_stacks_leetcode_232 {

	public static void main(String[] args) {

		MyQueue mq = new MyQueue();
		mq.push(1);
		mq.push(2);
		
		mq.peek();
		mq.pop();
		System.out.println(mq.empty());
	}

}
