package Queues;

import java.util.ArrayDeque;
import java.util.Queue;

//using std lib
public class UseOfQueue {

	public static void main(String[] args) {
		
		Queue<Integer> q = new ArrayDeque<>();
		
		q.add(17);
		q.add(20);
		q.add(24);
		q.add(25);
		q.add(28);
		System.out.println(q);
		System.out.println(q.size());
		System.out.println(q.isEmpty());
		System.out.println("Peek : "+q.peek());
		q.poll();
		System.out.println("After Polling : "+q);
		System.out.println("Peek : "+q.peek());
		q.remove();
		System.out.println("After removing head : "+q);
		q.poll();
		System.out.println("After Polling : "+q);
		
	}
	
}
