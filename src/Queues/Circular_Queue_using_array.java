package Queues;

class CircularQueue{
	
	int size;
	int arr[];
	int front;
	int rear;
	
	public CircularQueue(int size) {
		this.size = size;
		arr = new int[size];
		front = -1;
		rear = -1;
	}
	
	public void enqueue(int data) {
		
		if(front == -1 && rear ==-1) { //first element to push
			front = rear = 0;
		}
		else if((rear == size -1 && front == 0) ||
				(rear == (front-1) % (size-1)) ) {
			System.out.println("Queue is full.");
		}
		else if(rear == size-1 && front !=0 ) {
			rear = 0;
		}else {
			rear++;
		}
		
		arr[rear] = data;
	}
	
	public int dequeue() {
		if(front == -1) {
			System.out.println("Queue is empty.");
			return -1;
		}
		
		int ans = arr[front];
		arr[front] = -1;
		
		if(front == rear) {
			front = rear = -1;
		}else if(front == size -1 ) {
			front = 0;
		}else { //normal condition
			front++;
		} 
		
		return ans;
		
	}
	
	public void print() {
		if(front == -1) {
			System.out.println("Empty.");
			return;
		}
		for(int i=0; i<size; i++) {
			System.out.print(arr[i] +" ");
		}
		System.out.println();
	}
	
}


public class Circular_Queue_using_array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularQueue cq = new CircularQueue(3);
		cq.enqueue(1);
		cq.enqueue(2);
		cq.enqueue(3);
		System.out.println("front : "+cq.front +" rear: "+cq.rear );
		cq.dequeue();
		System.out.println("front : "+cq.front +" rear: "+cq.rear );
		cq.print();
		System.out.println("front : "+cq.front +" rear: "+cq.rear );
		cq.enqueue(1);
		System.out.println("front : "+cq.front +" rear: "+cq.rear );
		cq.print();
		cq.enqueue(2);	
		
		cq.dequeue();
		cq.dequeue();
		cq.dequeue();
		

		cq.print();
	}

}
