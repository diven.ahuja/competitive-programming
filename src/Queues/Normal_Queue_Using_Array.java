package Queues;

class Queue{
	
	int size;
	int arr[];
	int front;
	int rear;
	
	public Queue() {
		size = 5;
		arr = new int[size];
		front =0;
		rear=0;
	}
	
	//to push data
	public void enqueue(int data) {
		
		if(rear == size) {
			System.out.println("Queue is full.");
			return;
		}else {
			arr[rear] = data;
			rear++;
		}
		
	}
	
	public int dequeue() {
		
		if(front == rear) {
			System.out.println("Queue is empty.");
			return -1;
		}else {
			int ans = arr[front];
			arr[front] = -1;
			front++;
			
			if(front == rear) {
				front = 0;
				rear = 0;
			}
			return ans;
		}
	}
	
	public int front() {
		if(front == rear) {
			System.out.println("Queue is empty.");
			return -1;
		}else {
			return arr[front];
		}
	}
	
	public boolean isEmpty() {
		if(front == rear) {
			System.out.println("Queue is empty.");
			return true;
		}else {
			return false;
		}
	}
	
	public void print() {
		if(rear == front) {
			System.out.println("Empty.");
			return;
		}
		for(int i=front; i<rear; i++) {
			System.out.print(arr[i] +" ");
		}
		
		System.out.println();
	}
	
	
}

public class Normal_Queue_Using_Array {

	public static void main(String[] args) {

		Queue q = new Queue();
//		q.enqueue(1);
//		q.enqueue(2);
//		q.enqueue(3);
//		q.dequeue();
//		q.dequeue();
//		q.front();
//		q.dequeue();
//		System.out.println(q.front);
//		System.out.println(q.rear);
//		System.out.println(q.isEmpty());
//		q.print();
		
		q.dequeue();
		q.enqueue(17);
		q.print();
		q.isEmpty();
		q.enqueue(20);
		q.enqueue(24);
		q.enqueue(25);
		q.enqueue(28);
		q.print();
		System.out.println(q.isEmpty());
		System.out.println("Peek : "+q.front());
		q.dequeue();
		System.out.println("After Polling : ");
		q.print();
		System.out.println("Peek : "+q.front());
		q.dequeue();
		System.out.println("After removing head : ");
		q.print();
		q.dequeue();
		System.out.println("After Polling : ");
		q.print();
		q.dequeue();
		q.dequeue();
		q.print();
		System.out.println(q.isEmpty());
		
	}

}
