package Queues;

class ListNode {
	int data;
	ListNode next;

	public ListNode() {
	}

	ListNode(int data) {
		this.data = data;
		this.next = null;
	}
}

class NormalQueue {

	int size;
	ListNode head;
	int front;
	int rear;

	public NormalQueue(int size) {
		this.size = size;
		front = -1;
		rear = -1;
	}

	// to push data
	public void enqueue(int data) {

		if (isFull()) {
			System.out.println("Queue is full.");
			return;
		}

		if (isEmpty())
			head = new ListNode(data);
		else {
			ListNode newNode = new ListNode(data);
			
			ListNode curr = head;
			while(curr.next !=null)
			{
				curr = curr.next;
			}
			curr.next = newNode;
		}
	}

	public int dequeue() {

		if (isEmpty()) {
			System.out.println("Queue is empty.");
			return -1;
		}

		int val = head.data;
		head = head.next;

		return val;
	}

	public int front() {
		if (isEmpty()) {
			System.out.println("Queue is empty.");
			return -1;
		}

		return head.data;
	}

	public boolean isEmpty() {
		if (head == null)
			return true;
		return false;
	}

	public boolean isFull() {

		ListNode curr = head;
		int count = 0;

		while (curr != null) {
			count++;
			curr = curr.next;
		}

		if (count == size)
			return true;

		return false;
	}

	public void print() {
		ListNode curr = head;
		while (curr != null) {
			System.out.print(curr.data + " -> ");
			curr = curr.next;
		}
		System.out.println();
	}

}

public class Normal_Queue_using_Linked_list {

	public static void main(String[] args) {
		
		NormalQueue q = new NormalQueue(3);
		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		q.print();
		q.enqueue(4);
		q.print();
		
		q.dequeue();
		q.print();
		q.dequeue();
		q.print();
		q.dequeue();
		q.print();
		q.dequeue();
		
		q.print();
	}

}
