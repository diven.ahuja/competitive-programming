import java.util.Set;
import java.util.TreeSet;

public class PrimeNo {

	public static void main(String[] args) {
		
		int first = 2;//Integer.parseInt(args[0]);
		int second = 1000;//Integer.parseInt(args[1]);
		boolean flag = true;
		Set<Integer> primeResult = new TreeSet<>();
	
			for (int i = first; i <= second; i++) { //10 
				flag = true;
				for(int j = 2 ; j<=(i/2);j++) {
					if(i%j==0) {
						//do nothing
						flag = false;
						break;
					}
				}
				if(flag == true)
					primeResult.add(i);
			}
		System.out.println("Count "+ primeResult.size() +" Set " + primeResult);
	}
}
