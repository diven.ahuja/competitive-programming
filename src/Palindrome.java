public class Palindrome {

	public static int isPalindrome(String A) {
		char abc[] = A.toLowerCase().replaceAll(" ", "").toCharArray();
		char newarray[] = new char[A.length()];
		int skip = 0;

		for (int i = 0; i < abc.length; i++) {
			String pattern = "^[a-zA-Z0-9]*$";
			if (!String.valueOf(abc[i]).matches(pattern)) {
				skip++;
				continue;
			} else {
				newarray[i - skip] = abc[i];
			}
		}

		String updatedInput = String.valueOf(newarray).trim();
		StringBuffer sf = new StringBuffer(updatedInput);
		sf.reverse();
		if (sf.toString().equals(updatedInput)) {
			return 1;
		} else {
			return 0;
		}
	}

	public static void main(String[] args) {
		int a = isPalindrome("A man,~!@#$% a plan, a canal: Panama");
		System.out.println(a);
	}
}
