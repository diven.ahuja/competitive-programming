
import java.util.ArrayList;
import java.util.Arrays;

public class Test {
	public static String longestCommonPrefix(ArrayList<String> A) {

		String prefix = "";
		if (A.size() == 1)
			return A.get(0);

		int mini = A.get(0).length();
		int i = 0;
		boolean flag = true;
		int index = 0;
		while (i < mini) { // iterating every character
			
			for (int j = 1; j < A.size(); j++) { // iterating every word
					
				if (i > A.get(j).length()-1) {
					mini = A.get(j).length();
					flag = false;
					continue;
				}else if(i > A.get(j-1).length()-1) {
					mini = A.get(j-1).length();
					flag = false;
					continue;
				}
				
				if (!(A.get(j).charAt(i) + "").equals(A.get(j - 1).charAt(i) + "")) {
					flag = false;
					j = A.size();
				}
				index = j;
			}
			
			if (flag && index == A.size() - 1) {
				prefix += A.get(index).charAt(i);
			}

			i++;
		}
		return prefix;
	}

	public static void main(String[] args) {
//		ArrayList<String> A = new ArrayList<>(Arrays.asList("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
		
		  ArrayList<String> A = new ArrayList<>(Arrays.asList(
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
		 
		System.out.println(longestCommonPrefix(A));
	}
}

// "aaaaaa","aaaa","aaaaaa"
//    public String longestCommonPrefix(ArrayList<String> A) {
//        int count = -1;
//        for(int i = 0; i < A.get(0).length(); i++) {
//            char c = A.get(0).charAt(i); //
//            int j;
//            for(j = 1; j < A.size(); j++) {
//                if(i >= A.get(j).length() || A.get(j).charAt(i) != c) break; 
//            }
//            if(j == A.size()) count++;
//            else break;
//        }
//        
//        return A.get(0).substring(0,count+1);
//    }

/*
 * public class Solution { public String longestCommonPrefix(ArrayList<String>
 * A) {
 * 
 * String common = ""; String prefix = ""; if (A.size() == 1) return A.get(0);
 * 
 * int min = A.get(0).length(); for (int i = 0; i < A.size(); i++) { //Compare
 * elements of array with min if(A.get(i).length() <min) min =
 * A.get(i).length(); }
 * 
 * for (int i = 0; i < min; i++) { for (int j = 0; j < A.size(); j++) { common
 * += A.get(j).charAt(i); // gathering elements at i of every string }
 * 
 * if (allCharactersSame(common)) { // checking if characters gathered are all
 * same or not prefix += common.charAt(0); } common = ""; } return prefix;
 * 
 * }
 * 
 * static boolean allCharactersSame(String s) { int n = s.length(); for (int i =
 * 1; i < n; i++) if (s.charAt(i) != s.charAt(0)) return false;
 * 
 * return true; } }
 */
