package Heap;

public class HeapifyAlgo {

	static class Heap{
		static int arr[] = new int[100];
		static int size;
		
		
		public Heap() {
			size = 0;
		}
		
		public static void insert(int value) {

			// adding element to the last
			size = size + 1;
			arr[size] = value;
			int i = size;

			while (i > 1) {
				int parent = i / 2;

				if (arr[parent] < arr[i]) {
					// swap
					int temp = arr[parent];
					arr[parent] = arr[i];
					arr[i] = temp;

					i = parent; // now will check parent with its parent
				} else
					return;
			}
		} // insert
		
		public static void deleteRoot() {

			if(size == 0)
				return ;
			
			int n = size;
			
			//Step 1 sending last node to root
			arr[1] = arr[n];
			
			//step 2 since we need to delete, size will reduce
			size--;
			
			// step 3 : reorder the root to its appropriate position / rearranging a tree
			int index = 1;
			while(index < n) { // while is used because need to go for every node till leaf and check
				
				int leftChild = index*2;
				int rightChild = index*2 + 1;
				
				if(arr[index] < arr[leftChild]) {
					//swap
					int temp = arr[index];
					arr[index] = arr[leftChild];
					arr[leftChild] = temp;
					
					index = leftChild;
				}
				
				else if (arr[index] < arr[rightChild]) {
					// swap
					int temp = arr[index];
					arr[index] = arr[rightChild];
					arr[rightChild] = temp;

					index = rightChild;
				}
				
				else
					return;
			}
			
		} //delete
		
		
	}
	
	
	public static void heapify(int arr[], int n, int i) {
		
		int largest = i;
		int left = 2*i;
		int right = 2*i + 1;
		
		if(left <= n && arr[left] > arr[largest]) {
			largest = left;
		}
		
		if(right <= n && arr[right] > arr[largest]) {
			largest = right;
		}
	
		if(largest != i) { // base condition for recursion
			
			//swap
			int temp =  arr[largest];
			arr[largest] = arr[i];
			arr[i] = temp;
			
			heapify(arr, n, largest);
		}
	}
	
	public static void heapSort(int arr[]) {
		
		int n = arr.length;
		int size = n-1;
		
		while(size > 1) {
			//swap
			int temp = arr[1];
			arr[1] = arr[size];
			arr[size] = temp;
			
			size--; //reducing the size of array
			
			heapify(arr, size, 1);
			
		}
	}
	
	public static void main(String[] args) {
		
//		Heap heap = new Heap();
//		heap.insert(50);
//		heap.insert(40);
//		heap.insert(30);
//		heap.insert(20);
//		heap.insert(10);
//		heap.insert(5);
//		
//		for (int i = 1; i <= heap.size; i++) {
//			System.out.print( heap.arr[i] +" ");	
//		}
//
//		System.out.println();
//		System.out.println("After Deletion.");
//		heap.deleteRoot();
//		for (int i = 1; i <= heap.size; i++) {
//			System.out.print( heap.arr[i] +" ");	
//		}
//		
//		System.out.println();
//		
		
		int arr[] = new int[] {-1,54,53,55,52,50};
		int n = arr.length-1;

		//Heap creation
		for (int i =n/2 ; i >0 ; i--) {
			heapify(arr, n, i);
		}
		
		for (int i = 1; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		
		//heap sort
		heapSort(arr);
		for (int i = 1; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}

}
