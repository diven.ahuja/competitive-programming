import java.util.HashSet;
import java.util.Set;

public class TP {
	public static void main(String[] args) {
		Set<Employee> s1 = new HashSet<>();
		Employee e1 = new Employee(0,"d");
		Employee e2 = e1;
		s1.add(e1);
		s1.add(new Employee(0,"d"));
		System.out.println(s1.size());
	}
}

class Employee{
	public int id;
	public String name;
	
	public Employee(int id, String name) {
		this.id = id;
		this.name = name;
	}
}