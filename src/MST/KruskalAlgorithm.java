package MST;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class KruskalAlgorithm {

	public static class Node{
		
		public int u;
		public int v;
		public int w;
		
		public Node(int weight, int first, int second) {
			this.w = weight;
			this.u = first;
			this.v = second;
		}

		public int getU() {
			return u;
		}

		public int getV() {
			return v;
		}

		public int getW() {
			return w;
		}

	}

	public static class SortComparator implements Comparator<Node>{

		@Override
		public int compare(Node node1, Node node2) {
			
			if(node1.getW() < node2.getW())
				return -1;
			if(node1.getW() > node2.getW())
				return 1;
			return 0;
		}
		
	}
	
	public static int findPar(int u, int[] parent) {
		if(u == parent[u]) return u;
		
		return findPar(parent[u], parent);
	}
	
	public static void union(int u, int v, int[] parent, int[] rank) {
		u = findPar(u, parent);
		v = findPar(v, parent);
		
		if(rank[u] < rank[v]) {
			parent[u] = v;
		}else if(rank[v] < rank[u]) {
			parent[v] = u;
		}else {
			parent[v] = u;
			rank[u]++;
		}
	}
	
	public static void kruskalAlgo(ArrayList<Node> adj, int N) {

//		-- sorting starts --
		Collections.sort(adj, new SortComparator());
//		-- sorting ends --
		
		int parent[] = new int[N];
		int rank[] = new int[N];
		
		for(int i=0; i<N; i++) {
			parent[i] = i;
			rank[i] = 0;
		}
		
		int totalCost = 0;
		ArrayList<Node> MST = new ArrayList<>();
		
		for(Node node: adj) {
			if(findPar(node.getU(), parent) != findPar(node.getV(), parent)) {
				totalCost += node.getW();
				MST.add(node);
				union(node.getU(), node.getV(), parent, rank);
			}
		}
		
		System.out.println(totalCost);
		
		for(Node node: MST) {
			System.out.println(node.getU() + " " + node.getV());
		}
	}
	
	
	public static void main(String[] args) {
	
//		int n = 10;
//		ArrayList<Node> adj = new ArrayList<>();
		
//		adj.add(new Node(7,1,2));
//		adj.add(new Node(8,1,3));
//		adj.add(new Node(3,2,3));
//		adj.add(new Node(6,2,4));
//		adj.add(new Node(5,4,5));
//		adj.add(new Node(2,4,6));
//		adj.add(new Node(2,5,6));
//		adj.add(new Node(3,3,6));
//		adj.add(new Node(5,2,6));
//		adj.add(new Node(4,3,4));
		
//		int n = 9;
//		ArrayList<Node> adj = new ArrayList<>();
//		
//		adj.add(new Node(7,1,2));
//		adj.add(new Node(0,3,6));
//		adj.add(new Node(1,3,8));
//		adj.add(new Node(1,2,3));
//		adj.add(new Node(1,4,5));
//		adj.add(new Node(2,4,7));

		int n = 10;
		ArrayList<Node> adj = new ArrayList<>();
		
		adj.add(new Node(1,6,7));
		adj.add(new Node(14,3,5));
		adj.add(new Node(11,1,7));
		adj.add(new Node(10,4,5));
		adj.add(new Node(9,3,4));
		adj.add(new Node(8,1,2));
		adj.add(new Node(8,0,7));
		
		adj.add(new Node(7,7,8));
		
		adj.add(new Node(7,2,3));
		
		adj.add(new Node(6,8,6));
		
		adj.add(new Node(4,2,5));
		
		adj.add(new Node(4,0,1));
		
		adj.add(new Node(2,6,5));
		
		adj.add(new Node(2,8,2));
		
		
		kruskalAlgo(adj, n);
	}
	
}
