import java.util.ArrayList;

public class Solution {

	public static String removeConsCharac(String A, int B) {
		int count[] = new int[A.toCharArray().length];
		char letters[] = A.toCharArray();
		String newArr = "";
		String visited = "";
		String abc = "";

		for (int i = 0; i < count.length; i++) {
			abc = String.valueOf(letters[i]);

			if (visited.contains(abc)) {
				continue;
			}
			
			visited = "";

			for (int j = i; j < i + B; j++) { //aaaabcd
				if (j < letters.length && letters[i] == letters[j]) {
					count[i]++;
				}
				visited = visited.concat(abc);
			}

			if (count[i] != B) {
				newArr += letters[i];
			}
		}

		return newArr;
	}

	public static void main(String[] args) {
		System.out.println(removeConsCharac("aaaabcd", 2));
	}
}
