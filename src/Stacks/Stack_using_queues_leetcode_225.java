package Stacks;

import java.util.ArrayDeque;
import java.util.Queue;

class MyStack {

    Queue<Integer> q;
    Queue<Integer> p;

    public MyStack() {
        this.q = new ArrayDeque<>();
        this.p = new ArrayDeque<>();
    }
    
    public void push(int x) {
        q.add(x);
        p.add(x);
    }
    
    public int pop() {
        p = new ArrayDeque<>();
        int ans = 0;
        System.out.println("before "+q);
        for(int i=0; i<=q.size()-1; i++){
            p.add(q.remove());
        }
        if(!empty()){
            ans = q.peek();
        }else if(p.size() == 1){
        	ans = p.peek();
        	return ans;
        }
        q = new ArrayDeque<>(p);
        System.out.println("after"+q);
        return ans;
    }
    
    public int top() {
        int ans = 0;
        for(int i=0; i<=q.size()-1; i++){
            ans = q.remove();
        }
        if(!empty()){
            ans = q.peek();
        }
        q = new ArrayDeque<>(p);
        return ans;
    }
    
    public boolean empty() {
        if(q.isEmpty()){
            return true;
        }
        return false;
    }
}

public class Stack_using_queues_leetcode_225 {

	public static void main(String[] args) {
		
		MyStack m = new MyStack();
		m.push(1);
		m.push(2);
		m.push(3);
		System.out.println(m.pop());
		System.out.println(m.pop());
		System.out.println(m.pop());
		System.out.println(m.empty());
	}

}
