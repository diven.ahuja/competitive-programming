package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Anagram_amazon_leetcode_49 {
	
	public static List<List<String>> groupAnagrams(String[] strs) {
		
		HashMap<HashMap<Character, Integer>, List<String>> outerMap = new HashMap<>(); 
		
		for(String str : strs) {
			HashMap<Character, Integer> frequencyMap = new HashMap<>();
			
			//counting letters in a word
			for(int j=0; j <str.length(); j++) {
				frequencyMap.put(str.charAt(j), frequencyMap.getOrDefault(str.charAt(j), 0) + 1);
			}
			
			if(outerMap.containsKey(frequencyMap)) {
				List<String> words = outerMap.get(frequencyMap);
				words.add(str);
				outerMap.put(frequencyMap, words);
			}else {
				ArrayList<String> words = new ArrayList<String>();
				words.add(str);
				outerMap.put(frequencyMap, words);
			}
			
		}
		
		List<List<String>> listing = new ArrayList<List<String>>(); 
		for ( List<String> entry : outerMap.values()) {
			listing.add(entry);
		}
		
		return listing;
		
//		String[] strsClone = strs.clone();
//		sorting(strs);
//		List<List<String>> finalList = new ArrayList<>();
//		int visited[] = new int[strs.length];
//		
//		for (int i = 0; i < strsClone.length; i++) {
//			List<String> elementsTogether = new ArrayList<>();
//			if(visited[i] == 0) {
//				elementsTogether.add(strsClone[i]);
//				visited[i] = 1;
//				for (int j = i+1; j < strsClone.length; j++) {
//					if(strs[i].equals(strs[j]) && visited[j] == 0) {
//						elementsTogether.add(strsClone[j]);
//						visited[j] = 1;
//					}
//				}
//				finalList.add(elementsTogether);
//			}
//		}
//		
//		return finalList;
	}

//	private static void sorting(String[] strs) {
//		int i =0;
//		for(String str: strs) {
//			char ab[] = str.toCharArray();
//			Arrays.sort(ab);
//			str = new String(ab);
//			strs[i] = str;
//			i++;
//		}
//	}
	
	public static void main(String[] args) {
		
		System.out.println(groupAnagrams(new String[] {"eat", "tea", "tan", "ate", "nat", "bat"}));
		
		
	}
}
