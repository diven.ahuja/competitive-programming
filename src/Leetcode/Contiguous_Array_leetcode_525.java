package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Contiguous_Array_leetcode_525 {

public static int findMaxLength(int[] nums) {
        
        //Using Hashmap
        Map<Integer, Integer> hmap = new HashMap<>();
        hmap.put(0,-1);
        
        int sum = 0;
        int res = 0;
        for(int i=0; i<nums.length; i++){
            
            if(nums[i] == 0){
                sum = sum - 1;
            }else{
                sum = sum + 1;
            }
            
            if(hmap.containsKey(sum)){
                int index = hmap.get(sum);
                int len = i - index;
                if(len > res){
                    res = len;
                }
            }else{
                hmap.put(sum, i);
            }
        }
        
        
        return res;
        
        
        
        //Brute force approach... Time limit exceeded O(n2)
        
//         int count = 0;
//         int n = nums.length;
//         int res = 0;
        
//         for(int i=0; i<nums.length; i++){
            
//             int countOne = 0;
//             int countZero = 0;
//             int max = 0;
            
            
//             for(int j=i; j<nums.length; j++){
            
//                 if(nums[j] == 0)
//                     countZero++;
//                 else
//                     countOne++;
                
//                 if(countOne == countZero)
//                     max = countOne + countZero;
                
//             }
            
//             if(max > res)
//                 res = max;
//         }
        
//         return res;
        
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(findMaxLength(new int[] {0,0,1,1,1,0}));
	}

}
