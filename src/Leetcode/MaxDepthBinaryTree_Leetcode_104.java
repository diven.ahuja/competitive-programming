package Leetcode;

import java.util.Stack;

public class MaxDepthBinaryTree_Leetcode_104 {

	 public static int maxDepth(TreeNode1 root) {
		   
//       WITH RECURSION
      
//       if(root == null){
//           return 0;
//       }

//       int lh = maxDepth(root.left);
//       int rh = maxDepth(root.right);
      
//       int ans = Math.max(lh, rh) + 1;
//       return ans;
      
      //WITHOUT RECURSION
      
      // it is level order traversal.. just incrementing count at new level
//      if(root == null)
//          return 0;
//      
//      Queue<TreeNode1> q = new ArrayDeque<>();
//		q.add(root);
//		q.add(new TreeNode1(Integer.MIN_VALUE)); //this acts as a separator between two levels
//		
//		int count = 0;
//		while(!q.isEmpty()) {
//			
//			TreeNode1 temp = q.peek();
//			q.poll();
//			
//			if(temp.val  == Integer.MIN_VALUE) {
//				count++;
//				if(!q.isEmpty()) {
//					q.add(new TreeNode1(Integer.MIN_VALUE));
//				}
//				
//			}else {
//				
//				if(temp.left != null) {
//					q.add(temp.left);
//				}
//				
//				if(temp.right != null) {
//					q.add(temp.right);
//				}
//			}
//			
//		}
//      return count;
      
      

      //look at the correct code below where we maintain two diff stacks once for parsing and other stack storing its corresponding height value.
      
       //base case
       if(root == null)
           return 0;
      
       Stack<TreeNode1> s = new Stack<>();
       Stack<Integer> values = new Stack<>();
      
       s.push(root);
       values.push(1);
       int max =0;
      
       TreeNode1 temp = root;
      
       while(!s.empty()){
          
           temp = s.pop();
           int tempValue = values.pop();
           max = Math.max(tempValue, max);
          
           if(temp!=null && temp.left!=null) {
               s.push(temp.left);
               values.push(tempValue+1);   
           }
           if(temp!=null && temp.right!=null){ 
               s.push(temp.right);
               values.push(tempValue+1);
           }
       }
      
       return max;
      
      
  }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TreeNode1 root = new TreeNode1();
		root.val =1 ;
		
		TreeNode1 root2 = new TreeNode1();
		root2.val = 2 ;
		
		TreeNode1 root3 = new TreeNode1();
		root3.val = 3 ;
		
		TreeNode1 root4 = new TreeNode1();
		root4.val = 5;
		
		TreeNode1 root5 = new TreeNode1();
		root5.val = 4;
		
		root.left = root2;
		root.right = root3;
		
		root2.left = root4;
		root3.right = root5;
		
		MaxDepthBinaryTree_Leetcode_104 binaryTree_Leetcode_111 = new MaxDepthBinaryTree_Leetcode_104();
		System.out.println(binaryTree_Leetcode_111.maxDepth(root));
	}

}
