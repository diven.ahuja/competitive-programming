package Leetcode;
import java.math.BigInteger;

public class Test3 {
	
	public static final String ONE = "I";
	public static final String FIVE = "V";
	public static final String TEN = "X";
	public static final String FIFTY = "L";
	public static final String HUNDRED = "C";
	public static final String FIVE_HUNDRED = "D";
	public static final String THOUSAND = "M";
	
	public static int romanToInt(String A) {
		String abc = "";
		String number = "";
		for (int i = 0; i < A.length(); i++) {
			int j = i;
			if(i < A.length()-1 && A.charAt(i) == A.charAt(j+1)) {
				while(j < A.length() && A.charAt(i) == A.charAt(j)) {
					abc =  abc + A.charAt(i);
					j++;
				}
				i = j-1;
			
			} else {
				abc = abc + A.charAt(i);
			}
			
			if(abc.contains(THOUSAND)) {
				if(abc.length()==1) {
					number = number + "1";
				}else {
					number = number + abc.length();
				}
				abc = "";
				if(i == A.length()-1) {
					number = number + "000";
				}
			}
			if(abc.contains(FIVE_HUNDRED)) {
				if(abc.length()==1) {
					number = number + "5";
				}else {
					number = number + abc.length();
				}
				abc = "";
				if(i == A.length()-1) {
					number = number + "00";
				}
				
			}
			if(abc.contains(HUNDRED)) {
				boolean flag = true;
				if(A.charAt(i+1) == 'M') {
					number = number + "9";
					i++;
					flag = false ;
				}
				if(A.charAt(i+1) == 'D') {
					number = number + "4";
					i++;
					flag = false ;
				}else if(flag){
					number = number + abc.length();
				}else if(i >= A.length()) {
					number = number + "00";
				}
				abc = "";
			}
			if(abc.contains(FIFTY)) {
				int count = 0;
				int z = i + 1;
				while(A.charAt(z) == 'X') {
					count++;
				}
				int result = 5 + count;
				number = number + String.valueOf(result);
				if(abc.length()==1) {
					number = number + "5";
					i++;
				}
				if(i >= A.length()) {
					number = number + "0";
				}
			}
			if(abc.contains(TEN)) {
				boolean flag = true;
				if(A.charAt(i+1) == 'C') {
					number = number + "9";
					i++;
					flag = false ;
				}
				if(A.charAt(i+1) == 'L') {
					number = number + "4";
					i++;
					flag = false ;
				}else if(flag){
					number = number + abc.length();
				}
				abc = "";
			}
			if(abc.contains(FIVE)) {
				int count = 0;
				int z = i + 1;
				while(A.charAt(z) == 'I') {
					count++;
				}
				int result = 5 + count;
				number = number + String.valueOf(result);
				abc = "";
			}
			
			if(abc.contains(ONE)) {
				boolean flag = true;
				if(i+1 < A.length() && A.charAt(i+1) == 'X') {
					number = number + "9";
					i++;
					flag = false ;
				}
				else if(i+1 < A.length() && A.charAt(i+1) == 'V') {
					number = number + "4";
					i++;
					flag = false ;
				}else if(flag){
					number = number + abc.length();
				}
				abc = "";
			}
			
		}
		return Integer.parseInt(number.trim());
	}
	
	public static void main(String[] args) {
		System.out.println(romanToInt("MMMCCLXXXVII")); //3950
	}

}
