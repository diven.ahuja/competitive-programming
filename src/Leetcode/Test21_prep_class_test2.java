package Leetcode;

import java.util.Arrays;

public class Test21_prep_class_test2 {

	public static int[] checkArray(int a[], int b[], int m) {
		
		Arrays.sort(b); //nlogn
		
		for(int i=0; i<a.length; i++) {  		//O(n)
			
			int res = m - a[i];
			int start = 0, end = a.length-1;
			
			while(start < end) {				//O(logn)
				int mid = (start+end)/2;
				
				if(b[mid] == res) {
					return new int[] { a[i], b[mid] };
				}else if(b[mid] > res) {
					end = mid - 1;
				}else {
					start = mid+1;
				}
			}
			
		}
		return new int[] {0,0};
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int res[] = checkArray(new int[] {15, 5, 3, 15, 1}, new int[] {1, 2, 20, 17, 5}, 20);
		
		for(int num: res) {
			System.out.println(num);
		}
	}

}
