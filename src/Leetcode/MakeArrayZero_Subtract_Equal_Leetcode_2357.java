package Leetcode;

import java.util.Arrays;

public class MakeArrayZero_Subtract_Equal_Leetcode_2357 {

	public static int minimumOperations(int[] nums) {
		   
        // Set<Integer> set = new HashSet<>();
        // for (int a: nums)
        //     if (a > 0)
        //         set.add(a);
        // return set.size();
        
        if(nums.length == 1 && nums[0] != 0){
            return 1;
        }
        
        Arrays.sort(nums); // 0 1 3 5 5
        int count = 0, itr = 0;
        
        for(int i=0; i<nums.length; i++){
            
            if(nums[i] != 0){
                
                int temp = nums[i];
                itr++;
                
                for(int j=i; j<nums.length; j++){
                    nums[j] = nums[j] - temp;
                }
            }else
                continue;
        }
        
        return itr;
        
//         while(count < nums.length-1){
        
//             if(nums[count] != 0){
//                 for(int i=0; i<nums.length; i++){
//                     nums[i] = nums[i] - nums[0];

//                     if(nums[i] == 0)
//                         count++;
//                 }
//                 itr++;
//             }
//         }
        
        // return itr;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
