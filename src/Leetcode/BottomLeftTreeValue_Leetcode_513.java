package Leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class BottomLeftTreeValue_Leetcode_513 {

	private void helper(TreeNode1 root, List<Integer> res, int level){
        if(root ==  null)
            return;
        
        if(res.size() == level)
            res.add(root.val);
        
        helper(root.left, res, level+1);
        helper(root.right, res, level+1);
    }
    
    
    public int findBottomLeftValue(TreeNode1 root) {
//        List<Integer> res = new ArrayList<Integer>();
//        helper(root, res, 0);
//        return res.get(res.size()-1);
    	
        List<Integer> rightView = new ArrayList<Integer>();
        
        if(root == null)
            return 0;
        
        if(root != null && root.left == null && root.right == null)
            return root.val;
        
		Queue<TreeNode1> q = new ArrayDeque<>();
		q.add(root);
		q.add(new TreeNode1(-134)); //this acts as a separator between two levels
		
		int index = 0;
		while(!q.isEmpty()) {
			
			TreeNode1 temp = q.peek();
			q.poll();
			
			if(temp.val  == -134) {
				index++;
				if(!q.isEmpty()) {
					q.add(new TreeNode1(-134));
				}
				
			}else {
				if(rightView.size() == index)
					rightView.add(temp.val);

                if(temp.left != null) {
					q.add(temp.left);
				}
				
				if(temp.right != null) {
					q.add(temp.right);
				}
			}
			
		}
		// System.out.println(root.val);
		return rightView.get(rightView.size()-1);
    	
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BottomLeftTreeValue_Leetcode_513 leetcode_513 = new BottomLeftTreeValue_Leetcode_513();
		
		TreeNode1 root = new TreeNode1();
		root.val = 2147483647;
		
		TreeNode1 root1 = new TreeNode1();
		root1.val = 2147483647;
		
		root.right = root1;
		
		leetcode_513.findBottomLeftValue(root);
		
	}

}
