package Leetcode;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Test6 {

	public static void main(String[] args) throws IOException {


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int testCases = Integer.parseInt(br.readLine());  // Reading input from STDIN

        BigInteger sum;
        int j = 0;
		while (j < testCases) {
			String[] numbers = br.readLine().split(" ");
            BigInteger num1 = new BigInteger(numbers[0]);
            BigInteger num2 = new BigInteger(numbers[1]);

            sum = num1.add(num2);
            System.out.println(sum);
            j++;
        }        
        

        	
        	
        	
        
        
	}

}
