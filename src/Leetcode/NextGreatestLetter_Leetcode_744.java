package Leetcode;

public class NextGreatestLetter_Leetcode_744 {

	public static char nextGreatestLetter(char[] letters, char target) {

        //Binary search approach.
        
        int start = 0;
        int end = letters.length;
        int mid;
        char fin ='@';
        while(start <= end){
            
            mid = (start+end) /2;
            
            if(mid >= letters.length)
                return letters[0];
            
            if(letters[mid] == target)
                start = mid +1;
            
            if(letters[mid] > target){
                fin = letters[mid];
                end = mid -1;
            }else if(letters[mid] < target){
                start = mid +1;
            }
        }
    return fin;        
        

        // Letter Scan Approach. Figured it out on my own.
        
//         for(char letter: letters){ //O(n)
            
//             if(letter > target){
//                 return letter;
//             } 
                        
//         }
//         return letters[0];
    }
	
	
	public static void main(String[] args) {
		nextGreatestLetter(new char[] { 'c','f','j'  }, 'f');
	}
	
	
}
