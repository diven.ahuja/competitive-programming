package Leetcode;
import java.util.Arrays;

public class Test20 {
	public static int coinChange(int[] coins, int amount) {
		
		int countOfCoins = -1;
		
		Arrays.sort(coins);
		
		int largestCoin = amount/coins[coins.length-1];
		countOfCoins += largestCoin + 1;
		
		int remaining = amount - (coins[coins.length-1] * largestCoin) ;
		
		for (int i = 0; i < coins.length; i++) {
			
			if(coins[i] == remaining) {
				countOfCoins++;
				break;
			}
		}
		
		
		return countOfCoins;
	}

	public static void main(String[] args) {
		System.out.println(coinChange(new int[] {1,2,5}, 11));
	}
}
