package Leetcode;

public class Test12 {
	
	public static int removeElement(int[] nums, int val) {
		int count = 0;

		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != val) {
				nums[count] = nums[i];
				count++;
			}
		}

		return (count);
	}

	public static void main(String[] args) {
		
		int[] nums = new int[6];
		nums[0] = 1;
		nums[1] = 1;
		nums[2] = 2;
		nums[3] = 2;
		nums[4] = 2;
		nums[5] = 2;
		removeElement(nums, 2);
		
	}

}
