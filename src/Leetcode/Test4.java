package Leetcode;
import java.math.BigInteger;

public class Test4 {

	public static String addBinary(String A, String B) {
		int max = Math.max(A.length(), B.length());
		int min = Math.min(A.length(), B.length());

		String sum = "";
		int carry = 0;
		for (int j = min; j < max; j++) {
			if(A.length()<B.length())
				A = A + "0";
			else
				B = B + "0";
		}
		System.out.println(A);
		System.out.println(B);
		for (int i =max-1; i>=0; i--) {
			
			if(A.charAt(i) == '0' && B.charAt(i) == '0') {
				if(carry == 0)
					sum = sum + '0';
				else {
					sum = sum + '1';
					carry = 0;
				}
			}
			if (A.charAt(i) == '0' && B.charAt(i) == '1' ||
					A.charAt(i) == '1' && B.charAt(i) == '0') {
				if(carry == 0)
					sum = sum + '1';
				else {
					sum = sum + '1';
					carry = 1;
				}
			}
			if (A.charAt(i) == '1' && B.charAt(i) == '1') {
				sum = sum + '1';
				carry = 1;
			}
			
		}
		return new StringBuffer(sum).reverse().toString();
		
	}

	//1001110001111010101001110
	public static void main(String args[]) {
		System.out.println(addBinary(
				"100", 
				"011"));
//				 1001110001111010101001110
	}

}
