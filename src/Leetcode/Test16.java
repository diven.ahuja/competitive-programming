package Leetcode;
import java.util.Arrays;

public class Test16 {
	public static int singleNumber(int[] nums) {

		if(nums.length == 1) {
			return nums[0];
		}
		
		Arrays.sort(nums);
		
		for (int i = 1; i < nums.length-1; i+=2) {
			if(nums[i-1] != nums[i]) {
				return nums[i-1];
			}
		}
		
		
		return nums[nums.length-1];
		
		
		
		
		
//		ArrayList<Integer> a = new ArrayList<>();
//
//		for (int i = 0; i < nums.length; i++) {
//			
//			if(a.contains(nums[i])) {
//				a.remove(a.indexOf(nums[i]));
//			}else {
//				a.add(nums[i]);
//			}
//			
//		}//end of for
//
//		return a.get(0);
	}

	public static void main(String[] args) {
		
		System.out.println(singleNumber(new int[] {4,1,2,1,2}));
		
	}

}
