package Leetcode;

import java.util.HashSet;
import java.util.TreeSet;

class Node
{
    int data;
    Node next;
    Node(int d) {data = d; next = null; }
}
public class UnionTwoLinkedLists_GFG {
	public static void sort(Node head){
        Node current = head, index = null;
        int temp; 
        while (current != null) {
                // Node index will point to node next to
                // current
                index = current.next;
 
                while (index != null) {
                    // If current node's data is greater
                    // than index's node data, swap the data
                    // between them
                    if (current.data > index.data) {
                        temp = current.data;
                        current.data = index.data;
                        index.data = temp;
                    }
                    	
                    index = index.next;
                }
                current = current.next;
            }
    }
    
	public static Node findUnion(Node head1,Node head2)
	{
		
		// sort(head1); //  instead of this use TreeSet
		   // sort(head2); //  instead of this use TreeSet
		    
		    TreeSet<Integer> ts = new TreeSet<>();
		    
		    Node tmp1 = head1;
		    while(tmp1 != null){
		        ts.add(tmp1.data);
		        tmp1 = tmp1.next;
		    }
		    
		    Node tmp2 = head2;
		    while(tmp2 != null){
		        ts.add(tmp2.data);
		        tmp2 = tmp2.next;
		    }
		    
		    Node done = new Node(-1);
		    Node curr = done;
		    for(Integer i : ts){
		        
		        Node temp = new Node(i);
		        curr.next = temp;
		        curr = curr.next;
		            
		    }
		    
		    return done.next;
		
		//Below approach is correct it's just that sorting two diff link list is a exponential process
		// use a data structure that can solve this problem of yours... TREESET YAyyyyy... use the same approach just use TreeSet
		
//	    sort(head1);
//	    sort(head2);
//	    
//	    Node l1 = head1;
//	    Node l2 = head2;
//	    Node dummy = new Node(0);
//	    Node curr = dummy;
//	    HashSet<Integer> hset = new HashSet<>();
//	    
//	    while(l1 != null && l2!=null){
//	        int min = -1;
//	        
//	        if(l1.data < l2.data)
//	        {
//	            min = l1.data;
//	            l1 = l1.next;
//	        }else {
//	            min = l2.data;
//	            l2 = l2.next;
//	        }
//	        
//	        if(hset.contains(min))
//	        	continue;
//	        else
//	        	hset.add(min);
//	        
//	        Node m = new Node(min);
//	        curr.next = m;
//	        curr = curr.next;
//	        
//	    }
//	    
//	    if(l1!=null){
//	        curr.next = l1;
//	    }
//	   
//	  	if(l2!=null){
//	        curr.next = l2;
//	    }
//	  	
//	   return dummy.next; 
	}
	
	public static void main(String[] args) {

		Node n = new Node(9);
		Node n2 = new Node(6);
		Node n3 = new Node(4);
		Node n4 = new Node(2);
		Node n5 = new Node(3);
		Node n6 = new Node(8);
		
		n.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		
		Node nn = new Node(1);
		Node nn2 = new Node(2);
		Node nn3 = new Node(8);
		Node nn4 = new Node(6);
		Node nn5 = new Node(2);
		
		nn.next = nn2;
		nn2.next = nn3;
		nn3.next = nn4;
		nn4.next = nn5;
		
		
		findUnion(n, nn);
	}
}
