package Leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WordBreak_amazon_leetcode_139 {

	static String old = "";
	
	public static boolean wordBreak(String s, List<String> wordDict) {
		
		int[] dp = new int[s.length()];
		
		for (int i = 0; i < dp.length; i++) {
			for (int j = 0; j <= i; j++) {
				String w2check =  s.substring(j, i+1);
				if(wordDict.contains(w2check)) {
					if(j>0) {
						dp[i] += dp [j-1];
					}else {
						dp[i] += 1;
					}
				}
			}
		}
		
		return dp[s.length()-1] > 0;
		
//		old =  new String(s);
//        for(int i =0 ; i<wordDict.size(); i++){
//            s = new String(old);
//            for(int j=i; j<wordDict.size(); j++){
//            
//                s = s.replaceAll(wordDict.get(j), "  ");
//
//                if(s.trim().equals("") || s.equals(" ")){
//                    return true;
//                }
//            }
//        }
//        return false;
		
		
		
		
//		String old = new String(s);
//	      for(int i =0 ; i<wordDict.size(); i++){
//	            s = new String(old);
//	            
//	            while(!s.trim().equals("")) {
//		            for(int j=0; j<wordDict.size(); j++){
//		            	
//		                s = s.replaceFirst(wordDict.get(j), "");
//		                
//		                if(s.trim().equals("") || s.equals(" ")){
//		                    return true;
//		                }
//		            }
//		            
//		            break;
//	            }
//	        }
//	        return false;
//		
//		return recursionHelper(s, wordDict);
    }
	
	private static boolean recursionHelper(String s, List<String> wordDict) {
		
		if(s.trim().equals(""))
			return true;
		
		
		for (int i = 0; i < wordDict.size(); i++) {
			
			if(s.contains(wordDict.get(i)) && s.indexOf(wordDict.get(i)) == 0 ) {
   				s = s.replaceFirst(wordDict.get(i), "");
    		}
		}

		if(s.length() == old.length())
    		return false;
		
		s = new String(old);
		return recursionHelper(s, wordDict);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(wordBreak("ccaccc", Arrays.asList(new String[]{"cc","ac"})));
//		System.out.println(wordBreak("aaaaaaaaaaaa", Arrays.asList(new String[]{"aaaa","aaa"})));
//		System.out.println(wordBreak("cbca", Arrays.asList(new String[]{"bc","ca"})));
//		System.out.println(wordBreak("catsandog", Arrays.asList(new String[]{"cats","dog", "sand", "and", "cat"})));
//		System.out.println(wordBreak("leetcode", Arrays.asList(new String[]{"leet","code"})));
	}

}
