package Leetcode;

public class Delete_N_nodes_after_m_nodes_leetcode_1474 {

	public static ListNode deleteNodes(ListNode head, int m, int n) {

		if (head == null)
			return head;

		ListNode curr = new ListNode();
		curr.next = head;
		while (curr != null) {
			for (int i = 0; i < m && curr != null; i++) {
				curr = curr.next;
			}
			
			if(curr != null) {
				ListNode dummy = curr.next;
				for (int i = 0; i < n && dummy != null; i++) {
					dummy = dummy.next;
				}
	
				curr.next = dummy;
			}
		}

		print(head);
		return head;

	}

	public static void print(ListNode node) {

		while (node != null) {
			System.out.print(node.val + " - > ");
			node = node.next;
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(2);
		ListNode list3 = new ListNode(3);
		ListNode list4 = new ListNode(4);
		ListNode list5 = new ListNode(5);

		lN.next = list2;
		list2.next = list3;
		list3.next = list4;
		list4.next = list5;

		deleteNodes(lN, 1, 3);
	}

}
