package Leetcode;


class ListNod {
	int val;
	ListNod next;

	ListNod() {
	}

	ListNod(int val) {
		this.val = val;
	}

	ListNod(int val, ListNod next) {
		this.val = val;
		this.next = next;
	}
}

public class AddTwoNumbers_leecode_2_link_list {
	public static ListNod addTwoNumbers(ListNod l1, ListNod l2) {
		 	ListNod s1 = new ListNod();
	        ListNod curr = s1;
	        int carry = 0;

	        	while(l1 != null || l2 !=null || carry == 1){
	                int sum = 0;
	                
	                if(l1!= null){
	                    sum += l1.val;
	                    l1 = l1.next;
	                }
	                
	                if(l2!= null ){
	                    sum += l2.val;
	                    l2 = l2.next;
	                }
	                
	                sum  += carry;
	                int rem = sum % 10;
	                carry = sum /10;
	                
	                curr.next = new ListNod(rem);
	                curr = curr.next;
	            }
	            
	            return s1.next;
	        
	        
//	        while(l1 != null && l2 != null){
//	            
//	            int sumo = l1.val + l2.val + carry; //after debug
//	            
//	            int rem = sumo;
//	            carry = 0;
//	            if(sumo > 9){
//	                carry = sumo / 10;
//	                rem = sumo % 10;
//	            }
//
//	            curr.next = new ListNod(rem);
//	            curr = curr.next;
//	            
//	            l1 = l1.next;
//	            l2 = l2.next;
//	            
//	            if(l1 == null && l2 == null){
//	               	int sums = carry;
//	                if(sums==0)
//	                    break;
//	                    if(sums > 9){
//	                        curr.next = new ListNod(carry);
//	                        curr = curr.next;
//	                    }else
//	                        curr.next = new ListNod(sums);  //after debug
//
//	                    carry = 0;
//	            } //handling carry at the end
//	            
//	        } //while loop
//	        
//	        while(l1!=null){
//	            
//	            int s = l1.val + carry ;
//	            int rem = s;
//	            carry = 0;
//	            if(s>9){
//	                carry = s / 10;
//	                rem = s % 10;
//	            }
//	            
//	            curr.next = new ListNod(rem);
//	            curr = curr.next;
//	            
//	            
//	            l1 = l1.next;
//	            
//	            if(l1 == null){
//	                int sum = curr.val + carry;
//	                if(sum!=curr.val){
//	                    if(sum > 9){
//	                        curr.next = new ListNod(carry);
//	                        curr = curr.next;
//	                    }else
//	                        curr.next = new ListNod(sum);  //after debug
//	                }
//	                carry = 0;
//	            }
//	        } // while l1 not null
//	        
//	        while(l2!=null){
//	            
//	            int s = l2.val + carry ;
//	            
//	            int rem = s;
//	            carry = 0;
//	            if(s>9){
//	                carry = s / 10;
//	                rem = s % 10;
//	            }
//	            
//	            curr.next = new ListNod(rem);
//	            curr = curr.next;
//	            
//	            
//	            l2 = l2.next;
//	            
//	            if(l2 == null){
//	                int sum = curr.val + carry;
//	                if(sum!=curr.val){
//	                    if(sum > 9){
//	                        curr.next = new ListNod(carry);
//	                        curr = curr.next;
//	                    }else
//	                        curr.next = new ListNod(sum); //after debug
//	                }
//	                carry = 0;
//	            }
//	        } // while l2 not null
//	        
//	        
//	        return s1.next;
	}
	
	public static void print(ListNod nodes) {
		ListNod temp = nodes;
		while(temp != null) {
			System.out.print(temp.val +" -> ");
			temp = temp.next;
		}
		System.out.print("null");
		System.out.println();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListNod lN = new ListNod(3);
		ListNod list2 = new ListNod(3);
		ListNod list3 = new ListNod(3);
//		ListNod list4 = new ListNod(9);
//		ListNod list5 = new ListNod(9);
//		ListNod list6 = new ListNod(9);
//		ListNod list7 = new ListNod(9);
		
		lN.next = list2;
		list2.next = list3;
//		list3.next = list4;
//		list4.next = list5;
//		list5.next = list6;
//		list6.next = list7;
		

		ListNod listN = new ListNod(3);
		ListNod listN2 = new ListNod(3);
		ListNod listN3 = new ListNod(8);
//		ListNod listN4 = new ListNod(9);
		
		listN.next = listN2;
		listN2.next = listN3;
//		listN3.next = listN4;
		
		addTwoNumbers(lN, listN);
		
	}

}
