package Leetcode;

import java.util.Arrays;

public class WordSearch_leetcode_amazon_and_array_79 {
	
	static String finalWord = "";
	static String searchWord = "";
	static char[][] old = null;
	
	public static boolean exist(char[][] board, String word) {

		char letters[] = word.toCharArray();
		boolean flag = false;
		searchWord = word;
		int count = 0; 

		for (int i = 0; i < board.length; i++) {
			System.out.println("i = "+i);
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == letters[count]) {
					flag = dfs(board, i, j, letters, count);
					if(flag == true || finalWord.equals(word))
						return true;
					finalWord = "";
				}
			}
		}
		return flag;
	}

	public static boolean dfs(char[][] board, int i, int j, char[] letters, int index){
        
        if(i < 0 || j < 0|| i > board.length-1  || j > board[0].length-1 || index > letters.length-1) return false;
            
        if(finalWord.equals(searchWord)) {
        	return true;
        }
        
        if(board[i][j] != letters[index]) return false;
        
        if(finalWord.length() == searchWord.length() && !finalWord.equals(searchWord))
        	return false;

        finalWord += letters[index];
        char temp = new Character(board[i][j]);
        board[i][j] = '0';
        index+=1;
        
        
        dfs(board, i + 1, j, letters, index);
		dfs(board, i - 1, j, letters, index);
		dfs(board, i, j + 1, letters, index);
		dfs(board, i, j - 1, letters, index);
		
		if(finalWord.equals(searchWord)) {
        	return true;
        }
		
		board[i][j] = temp;
		finalWord = finalWord.substring(0, finalWord.length()-1);
        return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		char board[][] = new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}};
		char board[][] = new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}};
//		char board[][] = new char[][]{{'C','A','A'},
//									  {'A','A','A'},
//									  {'B','C','D'}};
//		char board[][] = new char[][]{{'A','B','C','E'},
//									  {'S','F','E','S'},
//									  {'A','D','E','E'}};
		System.out.println(exist(board, "SEE"));
	}

}
