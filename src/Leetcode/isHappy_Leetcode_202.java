package Leetcode;

import java.util.HashSet;

public class isHappy_Leetcode_202 {

	//Intuition behind this Approach:
    // Data Structure i thought of using Hashing. Because 
    // hashing search operation complexity is O(1). One must remember the properties of Data 
    // Structures, because it plays keep role in deciding the complexity of your program
    // 1. While loop to process until it encounters a same element/detecting a cycle
    // 2. getSum() function i made it to return the next number using the basic/standard 
    // modulo/division logic. One must definitely know this because it is used at many places.
    
//     public boolean isHappy(int n) {
//         HashSet<Integer> hs = new HashSet<>();
        
//         while(!hs.contains(n)){
//             hs.add(n);
//             n = getSum(n);
//             if(n == 1)
//                 return true;
//         }
//         return false;
//     }
    
//     private int getSum(int n){
        
//         int sum = 0;
//         while(n>0){
//             int digit = n%10;
//             sum += (digit * digit);
//             n = n/10;
//         }
//         return sum;
//     }
    
    //Floyd Cycle Algorithm
    // It basicallly states that if two people are running in a circle
    // and if one person is running faster and other person is running slower
    // then they are bound to meet at some point in the circle
    // otherwise, if they dont meet ever that means they are not running in a circle
    // Same approach is used below,
    // we keep on comparing the slow and fastRunner
    // The base condition to break the while loop is that
    //     - either it reached to a point where sum = 1;
    //     - or fast and slow runner met at a point in a circle
    // so if the fastrunner == 1 then it will return true otherwise false.. meaning they are stuck in a cycle
    public static boolean isHappy(int n) {
        int slowRunner = n;
        int fastRunner = getSum(n);
        
        while(fastRunner != 1 && slowRunner != fastRunner){
            fastRunner = getSum(getSum(fastRunner));
            slowRunner = getSum(slowRunner);
        }
        
        return fastRunner == 1;
    }
    
    private static int getSum(int n){
        int sum = 0;
        while(n>0){
            int digit = n%10;
            sum += (digit * digit);
            n = n/10;
        }
        return sum;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isHappy(1999999999));
	}

}
