package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class MergeInterval_56 {

	static ArrayList<ArrayList<Integer>> merged = new ArrayList<ArrayList<Integer>>();

	public static int[][] merge(int[][] intervals) {

		if(intervals.length == 1) {
			return intervals;
		}
        
		Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));
		
        recursionHelper(intervals, intervals[0], intervals[1], 1);

		int[][] arr = new int[merged.size()][merged.get(0).size()];

		for (int k = 0; k < merged.size(); k++) {
			for (int l = 0; l < merged.get(0).size(); l++) {
				arr[k][l] = merged.get(k).get(l);
			}
		}
		System.out.println(merged);
		return arr;
	}

	private static void recursionHelper(int[][] intervals, int[] array1, int[] array2, int k) {
		for (int j = 0; j < 2; j += 2) {

			if ((array1[j + 1] >= array2[j] && array1[j + 1] <= array2[j + 1])

					|| (array1[j] >= array2[j] && array1[j] <= array2[j + 1])

					|| (array1[j] <= array2[j] && array1[j + 1] >= array2[j])

					|| (array1[j] >= array2[j] && array1[j] <= array2[j + 1])) {

				ArrayList<Integer> a = new ArrayList<Integer>();
				int min = Math.min(array1[j], array2[j]);
				int max = Math.max(array2[j + 1], array1[j + 1]);
				a.add(min);
				a.add(max);

				if (merged.size() > 0) {
					merged.remove(merged.size() - 1);
				}
				merged.add(a);

				k += 1;
				if (k >= intervals.length) {
					return;
				}
				recursionHelper(intervals, new int[] { min, max }, intervals[k], k);
			} else {
				ArrayList<Integer> a = new ArrayList<Integer>();
				if (merged.isEmpty()) {
					a.add(array2[j]);
					a.add(array2[j + 1]);
				
					ArrayList<Integer> b = new ArrayList<Integer>();
					b.add(array1[j]);
					b.add(array1[j + 1]);
					
					merged.add(b);
				}else {
					a.add(array2[j]);
					a.add(array2[j + 1]);
				}

				merged.add(a);

				int num1 = merged.get(merged.size() - 1).get(0);
				int num2 = merged.get(merged.size() - 1).get(1);

				k += 1;
				if (k >= intervals.length) {
					return;
				}

				recursionHelper(intervals, new int[] { num1, num2 }, intervals[k], k);
			}

		}
	}

	public static void main(String[] args) {
		merge(new int[][] { {1, 4}, {4,5}});
	}
}
