package Leetcode;

import java.util.HashMap;
import java.util.Map.Entry;

public class Logger_rate_limiter_leetcode_359_microsoft {
	 HashMap<Integer, String> hmap;

	public Logger_rate_limiter_leetcode_359_microsoft() {

		hmap = new HashMap<>();
	}

	public  boolean shouldPrintMessage(int timestamp, String message) {

		if (hmap.containsValue(message)) {
			int hashTimestamp = -1;

			for (Entry<Integer, String> entry : hmap.entrySet()) {
				if (entry.getValue().equals(message)) {
					hashTimestamp = entry.getKey();
					break;
				}
			}
			System.out.println(hashTimestamp);
			int sub = timestamp - hashTimestamp;

			if (sub < 10) {
				return false;
			}

			hmap.put(timestamp, message);
			return true;
		} else {
			hmap.put(timestamp, message);
			return true;
		}
	}

}


 class A{
	
	 public static void display() {
		Logger_rate_limiter_leetcode_359_microsoft lo = new Logger_rate_limiter_leetcode_359_microsoft();
	
		lo.shouldPrintMessage(0, "A");
		lo.shouldPrintMessage(0, "B");
		lo.shouldPrintMessage(0, "C");
		lo.shouldPrintMessage(0, "A");
	 }
	 
	 public static void main(String[] args) {
			// TODO Auto-generated method stub
		 display();
		}
	 
 } 