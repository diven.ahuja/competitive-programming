package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GroupAnagrams_Leetcode_49 {

    public static List<List<String>> groupAnagrams(String[] strs) {

    	HashMap<HashMap<Character, Integer>, List<String>> hmap = new HashMap<>();
    	List<List<String>> res = new ArrayList<>();
    	
    	for(String str: strs) {
    		HashMap<Character, Integer> freqMap = new HashMap<>();
    		
    		for(int i=0; i<str.length(); i++) {
    			freqMap.put(str.charAt(i), freqMap.getOrDefault(str.charAt(i), 0) + 1);
    		}
    		
    		if(hmap.containsKey(freqMap) == false) {
    			List<String> temp = new ArrayList<>();
    			temp.add(str);
    			hmap.put(freqMap, temp);
    		}else {
    			List<String> temp = hmap.get(freqMap);
    			temp.add(str);
    			hmap.put(freqMap, temp);
    		}
    	}
    	
    	for(List<String> li : hmap.values()) {
			res.add(li);
		}
    	
    	return res;
    	//        int letters[] = new int[26];
//        
//        HashMap<String, HashMap<Character, Integer>> hmap = new HashMap<>();
//        List<List<String>> result = new ArrayList<>();
//        
//        for(String str : strs){
//            StringBuilder res = new StringBuilder();
//            HashMap<Character, Integer> hm = new HashMap<>();
//            
//            for(int i=0; i<str.length(); i++){
//            	int count = 1, index = i;
//                
//                while(index+1 < str.length() && str.charAt(index) == str.charAt(index+1)){
//                    count++;
//                    index++;
//                }
//                
//                if(index != 1)
//                	i += index;
//                hm.put(str.charAt(index), count);
//            }
//            
//            hmap.put(str, hm);
//        }
//        System.out.println(hmap);
//        
//        for(String str: strs){
//            List<String> a = new ArrayList<>();
//            for(String strss:strs){
//                if(hmap.get(str).equals(hmap.get(strss))){
//                    a.add(strss);
//                }             
//            }
//            if(!result.contains(a))
//            	result.add(a);
//        }
//         return result;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(groupAnagrams(new String[] {"ddddddddddg","dgggggggggg"}));
	}

}
