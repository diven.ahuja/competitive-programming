package Leetcode;

public class Length_of_last_word_leetcode_58 {
	
	public static int lengthOfLastWord(String s) {
	     
        String a = s.trim();
        int count =0;
        
        for(int i=a.length()-1; i>=0; i--){
            while(a.charAt(i) != ' '){
                count++;
                i--;
            }    
            break;
        }
        
        return count;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		lengthOfLastWord("   fly me   to   the moon  ");
	}

}
