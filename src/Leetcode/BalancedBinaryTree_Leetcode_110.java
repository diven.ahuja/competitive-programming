package Leetcode;

public class BalancedBinaryTree_Leetcode_110 {

	//recursive approach
    // this is returning height if diff between subtree is at most 1. 
    // other wise it is returning -1
    public int height(TreeNode root){
        
        if(root == null)
            return 0;
        
        int lh = height(root.left);
        if(lh == -1) return -1;
        int rh = height(root.right);
        if(rh == -1) return -1;
        
        if(Math.abs(lh - rh) > 1) return -1;
        return Math.max(lh, rh) + 1;
        
    }
    
    public boolean isBalanced(TreeNode root) {
    	// if -1 is returned, it returns false
        // else true
        return height(root) != -1;
        
// below approach i did thinking that height balanced tree is a tree where we need to check height of left and right subtree of ROOT ONLY.
//But in height balanced tree, at EVERY NODE 
//we need to check if height of left and right subtree is less than equal to 1.

//         if(root == null || (root.left == null && root.right == null))
//             return true;
        
//         Queue<TreeNode> q1 = new LinkedList<>();
//         Queue<TreeNode> q2 = new LinkedList<>();
        
//         if(root.left != null) {
//             q1.add(root.left);
//             q1.add(new TreeNode(-134));
//         }
        
//         if(root.right != null) {
//             q2.add(root.right);
//             q2.add(new TreeNode(-134));
//         }
        
//         int count = 0;
        
//         while(!q1.isEmpty()){
//             TreeNode temp = q1.remove();
//             if(temp.val == -134){
//                 count++;
                
//                 if(!q1.isEmpty()){
//                     q1.add(new TreeNode(-134));
//                 }
//             }else{
//                 if(temp.left != null) q1.add(temp.left);
//                 if(temp.right != null) q1.add(temp.right);
//             }
//         }
        
//         int count2 = 0;
//         while(!q2.isEmpty()){
//             TreeNode temp = q2.remove();
            
//             if(temp.val == -134){
//                 count2++;
                
//                 if(!q2.isEmpty()){
//                     q2.add(new TreeNode(-134));
//                 }
//             }else{
//                 if(temp.left != null) q2.add(temp.left);
//                 if(temp.right != null) q2.add(temp.right);
//             }
//         }
        
//         if(Math.abs(count - count2) > 1)
//             return false;
//         return true;
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
