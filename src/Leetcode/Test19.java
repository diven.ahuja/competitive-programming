package Leetcode;
import java.util.Arrays;

public class Test19 {

	 public static void moveZeroes(int[] nums) {
//	        int count= 0 ;
//	        for(int i=0;i<nums.length;i++){
//	            if(nums[i] == 0){
//	                count++;
//	            }
//	        }
	        
				int index = 0;

				for (int j = 0; j < nums.length; j++) {
					if (nums[j] != 0) {
						nums[index] = nums[j];
						index++;
					}
				}

//				int last = nums.length;
				for (int k = index; k < nums.length; k++) {
					nums[k] = 0;
				}
	        System.out.println(nums);
	  	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		moveZeroes(new int[] {0,0,2,1,12});
	}

}
