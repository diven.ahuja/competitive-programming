package Leetcode;

public class Test1 {

	/*
	 * The count-and-say sequence is the sequence of integers beginning as follows:
	 * 1, 11, 21, 1211, 111221, ... 1 is read off as one 1 or 11. 11 is read off as
	 * two 1s or 21. 21 is read off as one 2, then one 1 or 1211.
	 * 
	 * Given an integer n, generate the nth sequence.
	 * 
	 * Note: The sequence of integers will be represented as a string.
	 * 
	 * Example:
	 * 
	 * if n = 2, the sequence is 11.
	 */

	public static String countAndSay(int A) {
		
		if(A == 1){
            return "1";
        }
		if(A == 2){
            return "11";
        }
		
		char letter[] = null;
		int count = 0;
		String finalWord = "1";
		StringBuilder s = new StringBuilder();
		
		for (int j = 1; j < A; j++) {
			s.append("1"); //from here
			
			letter =  s.toString().toCharArray();
			count = 1;
			
			for(int i= 0; i<letter.length;i++) {
				
				if((i != letter.length-1) && (letter[i] + "").equals((letter[i + 1] + ""))) {
					count++;
					continue;
				}
				
				s.append(count).append(letter[i]);
			}
		}
		System.out.println(s);
		return finalWord;
	}

	private static String giveNextString(String string) {
		char letter[] =  string.toCharArray();
		int count = 1;
		String finalWord = "";
		boolean flag = true;
		
		for(int i= 0; i<letter.length;i++) {
			if (i != letter.length - 1) {
				if ((letter[i] + "").equals((letter[i + 1] + ""))) {
					count++;
					flag = false;
				} else {
					flag = true;
				}
			}
			if(flag || i == letter.length-1) {
				if (count > 1)
					finalWord += count + "" + letter[i];
				else
					finalWord += "1" + letter[i];
			}
		}

		return finalWord;
	}

	public static void main(String[] args) {
		countAndSay(19);
	}
}
