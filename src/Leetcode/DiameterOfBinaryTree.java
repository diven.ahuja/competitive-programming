package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class DiameterOfBinaryTree {

	static int count = 0;
	static List<Integer> numTemp = new ArrayList<>();
	static int max = 0;
	static int height = 0;
	
	public static int diameterOfBinaryTree(TreeNode root) {
		
		int diameter[] = new int[1];
		findHeight(root, diameter);
		return diameter[0];
		
	}

	private static int findHeight(TreeNode root, int[] diameter) {

		if(root == null) {
			return 0;
		}
		
		int lh = findHeight(root.left, diameter);
		int rh = findHeight(root.right, diameter);
		
		diameter[0] = Math.max(diameter[0], lh+rh);
		
		return 1+(Math.max(lh, rh));
	}

	public static void main(String[] args) {

//		TreeNode root = new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3));
		TreeNode root = new TreeNode(4, new TreeNode(2, new TreeNode(3), new TreeNode(1, new TreeNode(5), null)), null);
//		TreeNode root = new TreeNode(1, null, new TreeNode(2)); //, new TreeNode(4), new TreeNode(5)),new TreeNode(3));
//		TreeNode root = new TreeNode(2, null, new TreeNode(3, null, new TreeNode(1)));
		System.out.println(diameterOfBinaryTree(root));
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}

	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}