package Leetcode;

import java.util.Stack;

public class PathSum_Leetcode_112 {

	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public boolean hasPathSum(TreeNode root, int targetSum) {

		// non recursive approach using two stacks
		// Saw the ppt/slide on leetcode solution, understood it and
		// wrote it on my own.

		if (root == null)
			return false;

		Stack<TreeNode> s = new Stack<>();
		Stack<Integer> values = new Stack<>();

		s.push(root);
		values.push(targetSum - root.val);

		while (!s.isEmpty()) {

			TreeNode curr = s.pop();
			int value = values.pop();

			if (curr == null)
				return false;

			if (curr.left == null && curr.right == null) {
				if (value == 0)
					return true;
			}

			if (curr.right != null) {
				s.push(curr.right);
				values.push(value - curr.right.val);
			}

			if (curr.left != null) {
				s.push(curr.left);
				values.push(value - curr.left.val);
			}

		}

		return false;

		// recursion
		// saw video pepcoding on YT

//         if(root == null)
//             return false;

//         if(root.left == null && root.right == null)
//             return (targetSum - root.val) == 0;

//         return hasPathSum(root.left, targetSum - root.val) 
//             || hasPathSum(root.right, targetSum - root.val);
	}

	public static void main(String[] args) {
		
	}
}
