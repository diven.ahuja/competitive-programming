package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Reverse_Linked_list_Leetcode_206 {

	public static ListNode reverseList(ListNode head) {

		  ListNode prev = null;
	      ListNode curr = head;
	        while (curr != null) {
	        	ListNode future = curr.next;
	        	curr.next = prev;
	        	prev = curr;
	        	curr = future;
	        }
	        print(prev);
	        return prev;
		
//		Stack curr = head;
//		Stack reverse = null;
//		List<Integer> li = new ArrayList<Integer>();
//
//		while (curr != null) {
//			li.add(curr.val);
//			curr = curr.next;
//		}
//
//		Collections.reverse(li);
//		reverse = new Stack(li.get(0));
//		Stack temp = reverse;
//		li.remove(0);
//
//		for (int l : li) {
//			Stack element = new Stack(l);
//			temp.next = element;
//			temp = element;
//		}
//		print(reverse);
//		return reverse;
	}

	public static void print(ListNode head) {
		
		while(head!=null) {
			System.out.print(head.val +" -> ");
			head = head.next;
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(3);
		ListNode list3 = new ListNode(4);
		list3.next = null;
		
		lN.next = list2;
		list2.next = list3;

		ListNode listN = new ListNode(1);
		ListNode listN2 = new ListNode(2);
		ListNode listN3 = new ListNode(4);
		listN3.next = null;
		
		listN.next = listN2;
		listN2.next = listN3;
		
		reverseList(lN);
	}

}
