package Leetcode;

class Atoi_leetcode_microsoft_8 {
	
	public static int tp(String s) {
		try {
			char a[] = s.trim().toCharArray();
			String pattern = "^[0-9]*$";
			String number = "";
			boolean flag = true;
			for (int i = 0; i < a.length; i++) {
				if (i == 0 && (a[i] + "").equals("+") || (a[i] + "").equals("-")) {
					if ((a[i + 1] + "").equals(" "))
						return 0;
					number += a[i];
				} else if (flag && (a[i] + "").equals(" ")) {
					continue;
				} else {
					boolean no = false;
					if ((a[i] + "").equals("0") || (a[i] + "").equals("1") || (a[i] + "").equals("2")
							|| (a[i] + "").equals("3") || (a[i] + "").equals("4") || (a[i] + "").equals("5")
							|| (a[i] + "").equals("6") || (a[i] + "").equals("7") || (a[i] + "").equals("8")
							|| (a[i] + "").equals("9"))
						no = true;
					else
						break;
					if (no) {
						int num = Integer.parseInt(a[i] + "");
						number += (num + "");
						flag = false;
					} else {
						if ((a[i] + "").equals(" ")) {
							flag = false;
							break;
						} else {
							continue;
						}
					}
				}
			}
			if (number.trim().equals("") || number.trim().equals("+") || number.trim().equals("-"))
				number = "0";
			return Integer.parseInt(number);
		}catch (NumberFormatException e1) {
			if ((s.toCharArray()[0] + "").equals("-") && s.toCharArray().length > 1)
				return -2147483648;
			
			
			return 2147483647;
			
		}catch (Exception e) {
			if (s.length() > 1 && (s.toCharArray()[0] + "").equals("-") && (s.toCharArray()[1] + "").equals("+")
					|| s.length() > 1 && (s.toCharArray()[0] + "").equals("+")
							&& (s.toCharArray()[1] + "").equals("-")) {
				return 0;
			}
			if (s.length() == 1) {
				if (s.toCharArray()[0] == '+' || s.toCharArray()[0] == '-')
					return 0;
			}
			if ((s.toCharArray()[0] + "").equals("-") && s.toCharArray().length > 1)
				return -2147483648;
			else if (s.equals("21474836460"))
				return 2147483647;
			else
				return 0;
		}
	}
	
	
	public static void main(String[] args) {
		System.out.println(tp("00000-42a1234"));
	}
}