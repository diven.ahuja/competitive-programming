package Leetcode;
import java.util.ArrayList;

public class Test7 {

	public static int solve(String A) {

        int i =0 ;
        int close = 0 ;
        
        ArrayList<String> letter = new ArrayList<String>();
        
        while(i < A.length()){

        	if(A.charAt(i) == '('){
            	letter.add("(");
            }else {
				if (A.charAt(i) == ')') {
					if (!letter.isEmpty()) {
						System.out.println(i);
						letter.remove(letter.size()-1);
					} else {
						close++;
					}
				}
            }

            i++;
        }
        
        return letter.size()+close;
	}
	
	public static void main(String[] args) {
		System.out.println(solve("()(()(()"));
	}

}
