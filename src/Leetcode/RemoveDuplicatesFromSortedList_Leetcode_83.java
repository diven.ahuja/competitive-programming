package Leetcode;

public class RemoveDuplicatesFromSortedList_Leetcode_83 {

	public ListNode deleteDuplicates(ListNode head) {

		ListNode curr = head;
		while (curr != null) {
			if (curr.next != null && curr.val == curr.next.val)
				curr.next = curr.next.next;
			else
				curr = curr.next;
		}

		return head;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
