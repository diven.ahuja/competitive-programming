package Leetcode;

public class SwapNodesInPairs_Leetcode_24 {

	static ListNode dumb = null;

	public static ListNode helper(ListNode firstNode, ListNode secondNode) {

		if(firstNode == null || secondNode == null)
			return dumb;

		//swapping values between nodes
		// dummy nodes value will be updated since it is the same linked list
		int temp = secondNode.val;
        secondNode.val = firstNode.val;
        firstNode.val = temp;
		
        // taking node forward to swap next pair. 
        // so changing curr and curr.next
        // when next pair will come and be swapped 
        // it will automatically be updated in dummy linkedlist since
        // they all are a single linked list
        secondNode = secondNode.next;
        
        // setting dummy node to firstNode which is LinkedList with first 
        // pair swapped
        if(dumb == null)
        	dumb = firstNode;
        	
		if(secondNode == null || secondNode.next == null)
			return dumb;
		
		// going to next pair
		return helper(secondNode, secondNode.next);
	}

	public static ListNode swapPairs(ListNode head) {

		if (head == null || head.next == null)
			return head;

//		ListNode firstNode = head;
//		ListNode secondNode = head.next;
//
//		int temp = secondNode.val;
//		secondNode.val = firstNode.val;
//		firstNode.val = temp;
//
//		if (dumb == null)
//			dumb = firstNode;
//
//		if (secondNode.next == null || secondNode.next.next == null)
//			return dumb;
//
//		return swapPairs(secondNode.next);

		return helper(head, head.next);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ListNode listNode = new ListNode();
		listNode.val = 1;
		ListNode list2 = new ListNode();
		list2.val = 2;
		ListNode list3 = new ListNode();
		list3.val = 3;
		ListNode list4 = new ListNode();
		list4.val = 4;

		list3.next = list4;
		list2.next = list3;

		listNode.next = list2;

		swapPairs(listNode);

		System.out.println(listNode.val);
	}

}
