package Leetcode;
public class Test2 {

	public static final String ONE = "I";
	public static final String FIVE = "V";
	public static final String TEN = "X";
	public static final String FIFTY = "L";
	public static final String HUNDRED = "C";
	public static final String FIVE_HUNDRED = "D";
	public static final String THOUSAND = "M";
	
	 public static String intToRoman(int A) { //A = 55 or A =90
		 
		 int len_A = String.valueOf(A).length();
		 int A_copy = A;
		 String bcd = "";
		 int number[] = new int[len_A];
		 int j = 0;
		 if(len_A == 1) {

				
				if(A>0 && A < 4) {
					for (int i = A; i > 0; i--) {
						bcd = bcd + ONE;
					}
				}else if(A == 4) {
					bcd = bcd + ONE + FIVE;
				}else if(A == 5) {
					bcd = bcd + FIVE;
				}else if(A >5 && A <9) {
					bcd = bcd + FIVE;
					
					int diff = A - 5;
					
					for (int i = diff; i > 0; i--) {
						bcd = bcd + ONE;
					}
					
				} else if(A == 9) {
					bcd = bcd + ONE + TEN;
				}
			return bcd;
		 }

		 while (A > 0 && j < len_A) {
			    number[j] = A % 10;
			    A = A / 10;
			    j++;
			}
		 	
		 		 
			for (int k = (number.length - 1); k >= 0; k--) {
				if(len_A == 4) {
					if ((k == number.length - 1) && number[k] <= 3) {
						for (int i = number[k]; i > 0; i--) {
							bcd = bcd + THOUSAND;
						}
					}
				
					if ((k == number.length - 2)) {
						if (number[k]>0 && number[k] < 4) {
							for (int i = number[k]; i > 0; i--) {
								bcd = bcd + HUNDRED;
							}
						}else if(number[k] == 4) {
							bcd = bcd + HUNDRED + FIVE_HUNDRED;
						}else if(number[k] == 5) {
							bcd = bcd + FIVE_HUNDRED;
						}else if(number[k] > 5 && number[k] < 9 ) {
							bcd =  bcd + FIVE_HUNDRED;
							
							int diff = number[k] - 5;
							
							for (int i = diff; i > 0; i--) {
								bcd = bcd + HUNDRED;
							}
						}else if(number[k] == 9) {
							bcd = bcd + HUNDRED + THOUSAND;
						}
					}
					
					if ((k == number.length - 3)) {
							
							if(number[k]>0 && number[k] < 4) {
								for (int i = number[k]; i > 0; i--) {
									bcd = bcd + TEN;
								}
							}else if(number[k] == 4) {
								bcd = bcd + TEN + FIFTY;
							}else if(number[k] == 5) {
								bcd = bcd + FIFTY;
							}else if(number[k] >5 && number[k] <9) {
								bcd = bcd + FIFTY;
								
								int diff = number[k] - 5;
								
								for (int i = diff; i > 0; i--) {
									bcd = bcd + TEN;
								}
								
							} else if(number[k] == 9) {
								bcd = bcd + TEN + HUNDRED;
							}
					}
					
					if ((k == number.length - 4)) {
						
						if(number[k]>0 && number[k] < 4) {
							for (int i = number[k]; i > 0; i--) {
								bcd = bcd + ONE;
							}
						}else if(number[k] == 4) {
							bcd = bcd + ONE + FIVE;
						}else if(number[k] == 5) {
							bcd = bcd + FIVE;
						}else if(number[k] >5 && number[k] <9) {
							bcd = bcd + FIVE;
							
							int diff = number[k] - 5;
							
							for (int i = diff; i > 0; i--) {
								bcd = bcd + ONE;
							}
							
						} else if(number[k] == 9) {
							bcd = bcd + ONE + TEN;
						}
					}
				}else if (len_A == 3) {
					
					
					if ((k == number.length - 1)) {
						if (number[k]>0 && number[k] < 4) {
							for (int i = number[k]; i > 0; i--) {
								bcd = bcd + HUNDRED;
							}
						}else if(number[k] == 4) {
							bcd = bcd + HUNDRED + FIVE_HUNDRED;
						}else if(number[k] == 5) {
							bcd = bcd + FIVE_HUNDRED;
						}else if(number[k] > 5 && number[k] < 9 ) {
							bcd =  bcd + FIVE_HUNDRED;
							
							int diff = number[k] - 5;
							
							for (int i = diff; i > 0; i--) {
								bcd = bcd + HUNDRED;
							}
						}else if(number[k] == 9) {
							bcd = bcd + HUNDRED + THOUSAND;
						}
					}
					
					if ((k == number.length - 2)) {
							
							if(number[k]>0 && number[k] < 4) {
								for (int i = number[k]; i > 0; i--) {
									bcd = bcd + TEN;
								}
							}else if(number[k] == 4) {
								bcd = bcd + TEN + FIFTY;
							}else if(number[k] == 5) {
								bcd = bcd + FIFTY;
							}else if(number[k] >5 && number[k] <9) {
								bcd = bcd + FIFTY;
								
								int diff = number[k] - 5;
								
								for (int i = diff; i > 0; i--) {
									bcd = bcd + TEN;
								}
								
							} else if(number[k] == 9) {
								bcd = bcd + TEN + HUNDRED;
							}
					}
					
					if ((k == number.length - 3)) {
						
						if(number[k]>0 && number[k] < 4) {
							for (int i = number[k]; i > 0; i--) {
								bcd = bcd + ONE;
							}
						}else if(number[k] == 4) {
							bcd = bcd + ONE + FIVE;
						}else if(number[k] == 5) {
							bcd = bcd + FIVE;
						}else if(number[k] >5 && number[k] <9) {
							bcd = bcd + FIVE;
							
							int diff = number[k] - 5;
							
							for (int i = diff; i > 0; i--) {
								bcd = bcd + ONE;
							}
							
						} else if(number[k] == 9) {
							bcd = bcd + ONE + TEN;
						}
					}
				}else if (len_A == 2) {
					if ((k == number.length - 1)) {
						
						if(number[k]>0 && number[k] < 4) {
							for (int i = number[k]; i > 0; i--) {
								bcd = bcd + TEN;
							}
						}else if(number[k] == 4) {
							bcd = bcd + TEN + FIFTY;
						}else if(number[k] == 5) {
							bcd = bcd + FIFTY;
						}else if(number[k] >5 && number[k] <9) {
							bcd = bcd + FIFTY;
							
							int diff = number[k] - 5;
							
							for (int i = diff; i > 0; i--) {
								bcd = bcd + TEN;
							}
							
						} else if(number[k] == 9) {
							bcd = bcd + TEN + HUNDRED;
						}
				}
				
				if ((k == number.length - 2)) {
					
					if(number[k]>0 && number[k] < 4) {
						for (int i = number[k]; i > 0; i--) {
							bcd = bcd + ONE;
						}
					}else if(number[k] == 4) {
						bcd = bcd + ONE + FIVE;
					}else if(number[k] == 5) {
						bcd = bcd + FIVE;
					}else if(number[k] >5 && number[k] <9) {
						bcd = bcd + FIVE;
						
						int diff = number[k] - 5;
						
						for (int i = diff; i > 0; i--) {
							bcd = bcd + ONE;
						}
						
					} else if(number[k] == 9) {
						bcd = bcd + ONE + TEN;
					}
				}
				}
				}
				
				 
		 return bcd;
	 }
	
	public static void main(String[] args) {
		System.out.println(intToRoman(1500));
	}
}
