package Leetcode;

import java.util.HashMap;

public class Isomorphic_Strings_leetcode_205 {

	public boolean isIsomorphic(String s, String t) {

//      if(s.length() != t.length())
//          return false;

//      HashMap<Character, Character> hmap = new HashMap<>();
//      HashMap<Character, Character> hmap2 = new HashMap<>();

//      boolean flag1 = true, flag2= true;

//      for(int i=0; i<s.length(); i++){

//          if(!hmap.containsKey(s.charAt(i)))
//              hmap.put(s.charAt(i), t.charAt(i));
//          else 
//              if(hmap.get(s.charAt(i)) != t.charAt(i))
//                  flag1 = false;

//          if(!hmap2.containsKey(t.charAt(i)))
//              hmap2.put(t.charAt(i), s.charAt(i));
//          else 
//              if(hmap2.get(t.charAt(i)) != s.charAt(i))
//                  flag2 = false;

//          if(!flag1 && !flag2)
//              return false;
//      }

//      return (flag2 && flag1);

		if (s.length() != t.length())
			return false;

		HashMap<Character, Character> hmap = new HashMap<>();
		boolean flag1 = true, flag2 = true;

		for (int i = 0; i < s.length(); i++) {
			if (!hmap.containsKey(s.charAt(i)))
				hmap.put(s.charAt(i), t.charAt(i));
			else if (hmap.get(s.charAt(i)) != t.charAt(i)) {
				flag1 = false;
				break;
			}
		}

		hmap = new HashMap<>();
		for (int i = 0; i < s.length(); i++) {
			if (!hmap.containsKey(t.charAt(i)))
				hmap.put(t.charAt(i), s.charAt(i));
			else if (hmap.get(t.charAt(i)) != s.charAt(i)) {
				flag2 = false;
				break;
			}
		}

		return (flag2 && flag1);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
