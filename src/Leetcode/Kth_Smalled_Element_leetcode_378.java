package Leetcode;

public class Kth_Smalled_Element_leetcode_378 {

	public static int kthSmallest(int[][] matrix, int k) {

		int n = matrix.length;
		int low = matrix[0][0];
		int high = matrix[n - 1][n - 1];
		int  mid;
		
		while (low < high) {
			
			mid = low + (high - low)/2;
			
			int rank = lessEqual(mid, matrix);
			
			if(rank < k)
				low = mid + 1;
			else
				high = mid;
		}
		return low;
	}
	
	//returning the rank of the element in the matrix or can say, the count of elements <= target element in the matrix
	private static int lessEqual(int target, int[][] matrix) {
		
		int n = matrix.length;
		int count = 0;
		int index =0 ;
		int i =0;
		while(i<n && index < matrix[0].length) {
			if(matrix[i][index] <= target)
				count++;
			if(index == matrix[0].length-1) {
				index = -1;
				i++;
			}
			index++;
			
		}
		
		return count;
		
	}
	
// 	My attempt after understanding from video and then doing it on my own. I think the issue is with rank method, it doesn't rank properly,
	// so now in the above method i'head be using the logic from the video to count the number of elements less than or equal to the element
	// and return that.
	
//	public static int kthSmallest(int[][] matrix, int k) {

//		int low = matrix[0][0];
//		int high = matrix[matrix.length - 1][matrix.length - 1];
//
//		while (low <= high) {
//
//			int mid = (low + high) / 2; // by value it is happening
//			int[] rank = getRank(mid, matrix);
//
//			if (rank[1] == k) {
//				return rank[0];
//			}
//
//			if (rank[1] < k) {
//				low = mid + 1;
//			}
//
//			if (rank[1] > k) {
//				high = mid - 1;
//			}
//		}
//		return high;
//	}

//	private static int[] getRank(int target, int[][] matrix) {
//		int row = matrix.length;
//		int col = matrix[0].length;
//		int low = 0;
//		int high = (row * col) - 1;
//		int just = 0;
//		int numVal = 0;
//		while (low <= high) {
//			int mid = (low + high) / 2; // 5
//			numVal = matrix[mid / col][mid % col]; // this is to convert index of virtual array
//			// to index of matrix to get that element
//			
//			if (numVal == target) {
//				int a[] = new int[2];
//				a[0] = numVal;
//				a[1] = mid;
//				return a;
//			}
//
//			if (numVal < target) {
//				low = mid + 1;
//			}
//
//			if (numVal > target) {
//				just = mid;
//				high = mid - 1;
//			}
//		}
//		int a[] = new int[2];
//		a[0] = numVal;
//		a[1] = just+1;
//		return a;
//	}

	public static void main(String[] args) {
		System.out.println(kthSmallest(new int[][] {{-5,-4},{-5,-4}}, 2));
		// -5 -5 -4 -4
	}
}
