package Leetcode;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Test10 {

	public static int[] solve(int[] nums, int target ){
		
		int end[] = new int[2];
		
		for(int i =0;i<nums.length;i++) {
			
			if(i == nums.length-1) {
				return end;
			}
			
			HashSet<Integer> a = new HashSet<Integer>();
			
			for (int j = i+1; j < nums.length; j++) {
				
				int sum = nums[i] + (nums[j]);
				if(sum == target) {
					end[0] = i;
					end[1] = j;
				}
				
			}
			
			
		}
		
		return end;
	} 
	
	public static void main(String[] args) {
		ArrayList<Integer> nums = new ArrayList<Integer>();
		nums.add(1);
		nums.add(2);
		nums.add(2);
		nums.add(4);
		nums.add(5);
		int num[] = new int[4];
		num[0] = 3;
		num[1] = 2;
		num[2] = 4;
		num[3] = 15;
		System.out.println(solve(num, 6));
		
	}

}
