package Leetcode;

import java.util.Stack;

public class Largest_Rectangle_in_Histogram_leetcode_84 {

	public static int[] previousSmallerElements(int[] arr, int n){
        Stack<Integer> s = new Stack<>();
		int[] res = new int[n]; 
		
		for(int i=0; i<n; i++) {
			
			while(!s.empty() && arr[s.peek()] >= arr[i] ) {
				s.pop();
			}
			
			if(!s.empty())
				res[i] = s.peek();
			else
				res[i] = -1;
			
			s.push(i);
		}
        
        return res;
    }
    
    public static int[] nextSmallerElements(int[] arr, int n){
        Stack<Integer> s = new Stack<>();
		int[] res = new int[n]; 
		
		for(int i=n-1; i>=0; i--) {
			
			while(!s.empty() && arr[s.peek()] >= arr[i] ) {
				s.pop();
			}
			
			if(!s.empty())
				res[i] = s.peek();
			else
				res[i]= -1;
			
			s.push(i);
		}
        
        return res;
    }
    
    public static int largestRectangleArea(int[] heights) {
        
        //O(n) Using stacks and next Smaller element technique
        
        int num = heights.length;
        
        int p[] = previousSmallerElements(heights, num);
        
        int n[] = nextSmallerElements(heights, num);
        
        int maxArea = Integer.MIN_VALUE;

        for(int i=0; i<heights.length; i++){
            
            int h = heights[i];
            
            if(n[i] == -1)
                n[i] = num;
            
            int w = (n[i] - p[i] - 1);
            int area = w * h;
            
            maxArea = Math.max(area, maxArea);
        }

        return maxArea;
        
        
        //Brute Force done with O(n2) but time limit exceeded
        // Let's try it in O(n) with the use of stacks
        
//         int n = heights.length;
//         int maxArea = -1;
//         for(int i=0; i<n; i++){
            
//             int index = i-1;
//             int width = 1;
//             //left side of current index
//             while(index >= 0){
                
//                 if(heights[index] >= heights[i]){
//                     width++;
//                 }else{
//                     break;
//                 }
                
//                 index--;
//             }
            
//             //right side of current index
//             int z = i+1;
//             while(z < n){
                
//                 if(heights[z] >= heights[i]){
//                     width++;
//                 }else{
//                     break;
//                 }
                
//                 z++;
//             }
            
//             int area = heights[i] * width;
//             maxArea = Math.max(area, maxArea);
            
//         }
        
//         return maxArea;
    }
	
	public static void main(String[] args) {
		largestRectangleArea(new int[] {2,1,5,6,2,3});
	}

}
