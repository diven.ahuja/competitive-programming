package Leetcode;

public class Maximum_avg_subarray_leetcode_643 {

public double findMaxAverage(int[] nums, int k) {
        
        double maxAvg = 0;
        
        if(nums.length == 0)
            return maxAvg;
        
        if(nums.length == 1)
            return nums[0]/1 ;
        
        double sum=0;;
        for(int i=0; i<k; i++){
            sum += nums[i];         
        }
        
        maxAvg = sum;
        for(int i=k; i<nums.length ; i++){
            sum += nums[i] - nums[i-k];
            
            maxAvg = Math.max(maxAvg, sum);            
        }
        
        
        return maxAvg/k;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
