package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class IsSubset_GFG {

	public static String isSubset(long a1[], long a2[], long n, long m) {
		
		Map<Long, Integer> hmap = new HashMap<>();
		
		for (int i = 0; i < n; i++) {
			hmap.put(a1[i], 0);
		}
		
		for (int i = 0; i < m; i++) {
			if(!hmap.containsKey(a2[i]))
				return "No";
		}
		
		return "Yes";
//		Arrays.sort(a1);
//		Arrays.sort(a2);
//		
//		for (int i = 0; i < m; i++) {
//			boolean flag = false;
//			for (int j = 0; j < n; j++) {
//			
//				if(a2[i] == a1[j])
//					flag = true;
//			}		
//
//			if(!flag) {
//				return "No";
//			}
//		}
//
//		return "Yes";

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isSubset(new long[] {8, 4 ,5, 3, 1, 7, 9}, new long[] {5, 1, 3, 7, 9}, 7, 5));
	}

}
