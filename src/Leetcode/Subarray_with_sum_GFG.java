package Leetcode;

import java.util.ArrayList;

public class Subarray_with_sum_GFG {
    static ArrayList<Integer> subarraySum(int[] arr, int n, int s) 
    {
    	//USING TWO POINTERS WITH O(N) IN ONE PASS
    	//V GOOD TECHNIQUE
    	ArrayList<Integer> finalee = new ArrayList<>();
        if (arr.length == 1) {
            	 finalee.add(1);
            	 finalee.add(1);
            	 return finalee;   
        }
            
        
        int start = 0, end = start + 1;
        int sum = arr[start] + arr[end];
    	 while(end < n) {
             if(sum < s) {
            	 end++;
            	 if(end < n)
            		 sum += arr[end];
             }else if(sum == s) {
            	 finalee.add(start+1);
            	 finalee.add(end+1);
            	 return finalee;
             }else if(sum > s) {
            	 sum -= arr[start];
            	 start++;
             }
    	 }
         finalee.add(-1);
         return finalee;
    	
//             while(sum <= s && j<arr.length){
//            	 sum += arr[j];
//                 if(sum == s){
//                     ArrayList<Integer> finalee = new ArrayList<>();
//                     finalee.add(i+1);
//                     finalee.add(j+1);
//                     return finalee;
//                 }
//                 j++;
//             }
             
//         }
    	 
    	 
//         ArrayList<Integer> fin = new ArrayList<>();
//         fin.add(-1);
//         return fin;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(subarraySum(new int[] {135,101,170,125,79,159,163,65,106,146,82,28,162,92,196,143,28,37,192,5,103,154,93,183,22,117,119,96,48,127,172,139,70,113,68,100,36,95,104,12,123,134}, 42, 468));

	}

}
