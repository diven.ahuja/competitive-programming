package Leetcode;
import java.util.Arrays;

public class Test14 {

	public static int[] plusOne(int[] digits) {
        String numInString = Arrays.toString(digits).replace("[", "").replace("]", "");
        
        int a = 0;
        
        if(numInString.charAt(numInString.length()-1) != '9') {
        	a = Integer.parseInt(numInString.charAt(numInString.length()-1)+"") + 1;
        	digits[digits.length-1] = a;
        	return digits;
        }else {
        	
        	int[] finisher = new int[digits.length+1];
        	
        	digits[digits.length-1] = a;
        	int carry = 1;
        
        	if(digits.length>1) {
        		for(int i=digits.length-2; i>= 0;i--) {
        			if(digits[i] != 9) {
        				digits[i] = digits[i] + 1;
        				return digits;
        			}else if(digits[i] == 9 && i == 0) {
        				finisher[i] = 1;
        				return finisher;
        			}else {
        				digits[i] = 0;
        			}
        		}
        	}else {
        		finisher[0] = a + carry;
        		finisher[1] = a;
        		return finisher;
        	}
        	
        }
        
        
        
        
//        if(digits[digits.length-1] !=9) {
//        	digits[digits.length-1] = digits[digits.length-1] + 1; 
//        }else {
//        	int i = digits.length-1;
//        	while(i>=0) {
//        		if(digits[i - 1] !=9) {
//                	digits[i - 1] = digits[i -1] + 1; 
//                }else {
//                	digits[i] = 0;
//                	digits
//                }
//        		i--;
//        	}
//        }
        
//        
//        int num = Integer.parseInt(numInString);
//        
//        int total = num + 1;
//        
//        
//        int[] finalNum =  new int[(total+"").length()];
//        for(int i=(total+"").length()-1;i>=0;i--){
//            int a  = total % 10;
//            total = total /10;
//             finalNum[i] = a;  
//        }
        
        return digits;
	}
	
	public static void main(String[] args) {

		int[] a = new int[10];
//		int[] b = new int[1];
//		b[0] = 9;
		a[0] = 9;
		a[1] = 9;
		a[2] = 9;
		a[3] = 9;
		a[4] = 9;
		a[5] = 9;
		a[6] = 9;
		a[7] = 9;
		a[8] = 9;
		a[9] = 9;
		plusOne(a);
		
	}

}
