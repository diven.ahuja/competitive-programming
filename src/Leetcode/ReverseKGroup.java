package Leetcode;

public class ReverseKGroup {

	 public static ListNode reverseKGroup(ListNode head, int k) {
	     
		 ListNode dummy = new ListNode();
	        dummy.next = head;
	        
	        int count = 1;
	        
	        ListNode cur = head;
	        while(cur.next !=null){
	            cur = cur.next;
	            count++;
	        }
	        
	        ListNode curr = dummy;
	        ListNode next = dummy;
	        ListNode prev = dummy;
	        
	        while(count>=k){ //will ignore the group if count is less than k.
	            curr = prev.next;
	            next = curr.next;
	            
	            for(int i=1; i<k; i++){
	            	System.out.println(count);
	                curr.next = next.next;
	                next.next = prev.next;
	                prev.next = next;
	                next = curr.next;    
	            }

	            prev = curr;
	            count -= k;
	        }
	        
	        print(dummy.next);
	        return dummy.next;
	       
		 
		 
//	        Stack dummy = head;
//	        Stack curr = dummy;
//	        Stack prev = null;
//	        
//	        while(curr!=null && curr.next!=null){
//	        	
//	        	Stack change = new Stack(curr.val);
//	        	for(int i=1; i<k; i++) {
//	        		curr = curr.next;
//	        		change.next = new Stack(curr.val);
//	        	}
//	        	curr = curr.next;
//	        	
//	        	print(change);
//	            while(change != null){
//	                Stack future = change.next;
//		        	change.next = prev;
//		        	prev = change;
//		        	change = future;
//	            }
//	            print(prev);
//	            
//	            Stack point = prev;
//	            while(point.next!=null) {
//	            	point = point.next;
//	            }
//	            point.next = curr;
//	            
//	            print(prev);
//	        }
//	        return prev;
	    }
	
	 public static void print(ListNode head) {
			
			while(head!=null) {
				System.out.print(head.val +" -> ");
				head = head.next;
			}
			System.out.println();
		}
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(2);
//		Stack list3 = new Stack(3);
//		Stack list4 = new Stack(4);
//		Stack list5 = new Stack(5);

		lN.next = list2;
//		list2.next = list3;
//		list3.next = list4;
//		list4.next = list5;

		
		reverseKGroup(lN, 2);
	}

}
