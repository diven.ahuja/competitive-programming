package Leetcode;

import java.util.Stack;

public class TimeToBuyTickets_Leetcode_2073 {
    static int count = 0;
    
    public static int recurstionHelper(int arr[], int k){
        
        for(int i=0; i<arr.length; i++){
        	if(arr[i] > 0){
                arr[i] -= 1;
                count++;
            }
            if(arr[k] == 0)
                return count;
        }
        
        return recurstionHelper(arr, k);
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		recurstionHelper(new int[] {84,49,5,24,70,77,87,8}, 3);

	}

}
