package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.PriorityQueue;

public class MergeKsortedLists {
	
	static class ListNode {
		      int val;
		      ListNode next;
		      ListNode() {}
		      ListNode(int val) { this.val = val; }
		      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
		  }
	
	
	//This method is used to sort the linked list
    private void sortList(ListNode head) {  
        //Node current will point to head  
        ListNode current = head, index = null;  
        int temp;  
          
        if(head == null) {  
            return;  
        }  
        else {  
            while(current != null) {  
                //Node index will point to node next to current  
                index = current.next;  
                  
                while(index != null) {  
                    //If current node's data is greater than index's node data, swap the data between them  
                    if(current.val > index.val) {  
                        temp = current.val;  
                        current.val = index.val;  
                        index.val = temp;  
                    }  
                    index = index.next;  
                }  
                current = current.next;  
            }      
        }  
    }
    // In this approach, 
	//1. we merge all the lists into one list without changing any element's order
	//2. we sort the linked list by sending it to the function sortList() 
//     public ListNode mergeKLists(ListNode[] lists) {
//         ListNode res = new ListNode();
//         ListNode temp = res;
//         for(int i=0; i<lists.length; i++){
            
//             ListNode l = lists[i];
//             while(l != null){
//                 temp.next = new ListNode(l.val);
//                 l = l.next;
//                 temp = temp.next;
//             }
            
//         } //for loop
//         sortList(res.next);
//         return res.next;
//     }
    
    //Better Approach than above.. 
	 // In this approach, 
	//1. we put all the linked lists values in an array as we iterate them
	//2. we use the in-built library sort function to sort the Collection
	//3 Put the sorted array in LinkedList and return it.
//     public ListNode mergeKLists(ListNode[] lists) {
//          // PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
//         ArrayList<Integer> pq = new ArrayList<>();
        
//         ListNode res = new ListNode();
//         // ListNode temp = res;
//         ListNode dum = res;
//         for(int i=0; i<lists.length; i++){
            
//             ListNode l = lists[i];
//             while(l != null){
//                 pq.add(l.val);
//                 l = l.next;

//             }// while
            
//         } //for loop
        
//         // Object arr[] = pq.toArray();
//         // Arrays.sort(arr);
//         Collections.sort(pq);
        
//         for(int i=0; i<pq.size(); i++){
//             dum.next = new ListNode(pq.get(i));
//             dum = dum.next;
//         }
        
//         return res.next;
//     }
    
    //Better than above Approach using priority queue
	// In this approach, we use priority queue
	//1. We add the first element of all the list in the priority queue
	// and the PQ will keeep the smallest element on top as we are using Min Heap
	//2. We pop the first/Smallest element from queue and 
	//3. We point to the next element of that Linked List and add it in the queue.
	// Once the element is added, priority queue will adjust again
	// and will bring the smallest element on top
	// we will again pop the top/smallest element and whole process repeats again
	public static ListNode mergeKLists(ListNode[] lists) {
		
        int n = lists.length;
        if (lists == null || n == 0) return null;
        
		PriorityQueue<ListNode> pq = new PriorityQueue<>((ListNode a, ListNode b) -> a.val - b.val);
		for(ListNode li : lists) {
			if(li != null) pq.add(li);
		}
		
		ListNode head = new ListNode(-1);
		ListNode dummy = head;
		
		while(!pq.isEmpty()) {
			dummy.next = pq.poll();
			
			dummy = dummy.next;
			if(dummy.next!=null) pq.add(dummy.next);
		}
		return head.next;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
