package Leetcode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class Test18 {

	private Map<String, List<Integer>> idxMap = new HashMap<>();

	public Test18(String[] dic) {
		for (int i = 0; i < dic.length; i++) {
			
			if(idxMap.containsKey(dic[i])) {
				List<Integer> list1 = idxMap.get(dic[i]);
				list1.add(i);
				idxMap.put(dic[i], list1);
			}else {
				List<Integer> list1 = new ArrayList<Integer>();
				list1.add(i);
				idxMap.put(dic[i], list1);
			}
		
		}
		
		System.out.println(idxMap);
	}

	public int shortest(String word1, String word2) {
		
		int distance = Integer.MAX_VALUE;
		List<Integer> word1Size = idxMap.get(word1);
		List<Integer> word2Size =  idxMap.get(word2);
		
		for(int i=0;i<word1Size.size();i++) {
			for(int j=0; j<word2Size.size();j++) {
				int sum = Math.abs(word1Size.get(i) - word2Size.get(j));
				if(sum <  distance){
					distance = sum;
				}
				
				if(distance == 1) {
					word1Size = null;
					word2Size = null;
					idxMap.clear();
					return 1;
				}
				
			}
		}
		
		return distance;
	
	}

	public static void main(String[] args) {
		
		Test18 test18 = new Test18(new String[] {"a","b","c","d","a"});
		System.out.println(test18.shortest("b", "d"));
		
		
	}

}
