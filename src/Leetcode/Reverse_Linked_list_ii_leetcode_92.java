package Leetcode;

public class Reverse_Linked_list_ii_leetcode_92 {
	public static ListNode reverseBetween(ListNode head, int left, int right) {

		ListNode dummy = new ListNode();
		dummy.next = head;
		ListNode prev = dummy;
		ListNode curr = dummy;
		ListNode next = dummy;

		int count = 0;
		while (count < left) {
			prev = curr;
			curr = curr.next;
			next = curr.next;
			count++;
		}

		while (left < right && curr != null && next!=null) {
			curr.next = next.next;
            next.next = prev.next;
            prev.next = next;
            next = curr.next;
            left++;
		}
		
		
		print(dummy.next);
		return dummy.next;

	}
	 public static void print(ListNode head) {
			
			while(head!=null) {
				System.out.print(head.val +" -> ");
				head = head.next;
			}
			System.out.println();
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(2);
		ListNode list3 = new ListNode(3);
		ListNode list4 = new ListNode(4);
		ListNode list5 = new ListNode(5);

		lN.next = list2;
		list2.next = list3;
		list3.next = list4;
		list4.next = list5;

		
		reverseBetween(lN, 2, 4);
		
		
	}

}
