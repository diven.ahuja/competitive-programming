package Leetcode;

public class SortedArrayToBst {

	private static void helper(TreeNode1 lastNode, int start, int end, int[] nums) {

		if(start > end) {
			return;
		}
		
		int mid = (start + end) / 2;
		
		if (nums[mid] < lastNode.val) {
			lastNode.left = new TreeNode1(nums[mid]);
			helper(lastNode.left, start, mid - 1, nums);
			helper(lastNode.left, mid + 1, end, nums);
		} 
		else if (nums[mid] > lastNode.val) {
			lastNode.right = new TreeNode1(nums[mid]);
			helper(lastNode.right, start, mid - 1, nums);
			helper(lastNode.right, mid + 1, end, nums);
		}
		

	}

	public static TreeNode1 sortedArrayToBST(int[] nums) {

		int mid = nums.length / 2;
		int end = nums.length-1;

		TreeNode1 root = new TreeNode1(nums[mid]);

		helper(root, 0, mid - 1, nums);
		helper(root, mid + 1, end, nums);
		
		return root;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sortedArrayToBST(new int[] { -10, -3, 0, 5, 9 });

	}

}
