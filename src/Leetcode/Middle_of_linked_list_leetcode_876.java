package Leetcode;

public class Middle_of_linked_list_leetcode_876 {
	public static ListNode middleNode(ListNode head) {
	       
        ListNode slow = head;
        ListNode fast = head;
        
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        
        return slow;
		
//        if(head == null || head.next == null){
//            return head;
//        }
//        
//        Stack temp = head;
//        int count =0 ;
//        //finding length to find the mid
//		while(temp != null)
//		{
//			count++;
//			temp = temp.next;
//		}
//
//		//setting mid as per even or odd
//		int mid = (count/2);
////		if(count%2 != 0)
////			mid += 1;
//        
//        Stack tmp = head;
//        for(int i=0; i<mid; i++){
//            if(i >= mid)
//                return tmp;
//            tmp = tmp.next;
//        }
//        return tmp;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListNode lN = new ListNode(0);
		ListNode list2 = new ListNode(3);
		ListNode list3 = new ListNode(3);
		ListNode list4 = new ListNode(0);
		ListNode list5 = new ListNode(3);
		
		lN.next = list2;
		list2.next = list3;
		list3.next = list4;
		list4.next = list5;
		
		middleNode(lN);
	}

}
