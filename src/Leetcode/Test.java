package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
	
	public static List<List<Integer>> quadruple(int arr[], int k) {
		
		List<List<Integer>> res = new ArrayList<>();

		//edge case
		if(arr.length < 4) {
			return res;	 // returning an empty list
		}
		
		// incase if length is same.
		if(arr.length == 4) {
			int sum = 0;
			for (int i = 0; i < arr.length; i++) {
				sum += arr[i];
			}
			
			if(sum != k) {
				return res; // returning an empty list
			}
		}
		
		Arrays.sort(arr); // sorting the array because we are concerned with the element value and not the index and 
						 // it would help us realise if we need to increment it or decrement the value based on two pointers.
		
//		for (int i = 0; i < arr.length; i++) { // to iterate the array
//			int addition = 0;
//			for (int j = 0; j < arr.length; j++) {
//				ArrayList<Integer> temp = new ArrayList<Integer>();
//				for(int l = 0; l<4; l++) {
//						addition += arr[l];
//						temp.add(arr[l]);
//				}
//				
//				if(addition == k && !res.contains(temp)) {
//					res.add(temp);
//				}
//			}
//		}
		
		for (int i = 0; i < arr.length - 3; i++) {
			if (i > 0 && arr[i] == arr[i - 1])
				continue; // avoid duplicates
			
			for (int j = i + 1; j < arr.length - 2; j++) {
				
				if (j > i + 1 && arr[j] == arr[j - 1])
					continue; // avoid duplicates
				
				int start = j + 1;
				int end = arr.length - 1;
				
				while (start < end) {
					long sum = arr[i] + arr[j] + arr[start] + arr[end];
					if (sum == k) {
						res.add(Arrays.asList(arr[i], arr[j], arr[start], arr[end]));
						while (start < end && arr[start] == arr[start + 1])
							start++; // keep on moving forward to avoid duplicates
						while (start < end && arr[end] == arr[end - 1])
							end--; // keep on moving forward to avoid duplicates
						start++;
						end--;
					} else if (sum < k) {
						start++;
					} else
						end--;
				}
			}
		}
		
		System.out.println(res);
			
		return res;
	}
	
	public static void main(String[] args) {
		quadruple(new int[] {10,2,2,3,4,5,7,8}, 23);
	}
}
