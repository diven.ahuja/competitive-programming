package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HighFive_Leetcode_1086 {

	public static int[][] highFive(int[][] items) {
//		My solution Approach. Runtime 3ms faster than 99%: Space less than 96%.
//		Hmap structure: Integer, and ArrayList
//
//		    Iterate through items, keep putting studentid in hmap as keys and keep adding the marks in the ArrayList for that key.
//		    Iterate through hmap:
//
//		    arr = Get the value(ArrayList in this case) from hmap.
//		    Sort the ArrayList
//		    iterate over array and get the sum and calculate average for top 5.
//		    put the key and average in result array.

		
		Map<Integer, ArrayList<Integer>> hmap = new HashMap<>();

		for (int i = 0; i < items.length; i++) {

			ArrayList<Integer> arr = null;
			arr = hmap.get(items[i][0]);
			if (arr == null)
				arr = new ArrayList<Integer>();
			arr.add(items[i][1]);

			hmap.put(items[i][0], arr); // hmap ready
		}

		int fin[][] = new int[hmap.size()][2];
		int i = 0;
		for (Entry<Integer, ArrayList<Integer>> entry : hmap.entrySet()) {

			ArrayList<Integer> arr = entry.getValue();
			Collections.sort(arr);

			int sum = 0;
			for (int j = arr.size()-1; j>=arr.size()-5 ; j--) {
				sum += arr.get(j);
			}

			int avg = sum / 5;
			fin[i] = new int[] {entry.getKey(), avg};
			i++;
		}
		return fin;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		highFive(new int[][] {{1,84},{1,72},{1,47},{1,43},{1,78},{2,79},{2,4},{2,23},{2,88},{2,79},{3,75},{3,80},{3,38},{3,73},{3,4}});
	}

}
