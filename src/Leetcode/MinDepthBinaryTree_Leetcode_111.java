package Leetcode;

import java.util.Stack;
import java.util.TreeSet;

public class MinDepthBinaryTree_Leetcode_111 {
    public static int minDepth(TreeNode1 root) {
        
        //with recursion . dry run/ debug it and understand it done. watched YT video
    	
		if (root == null) {
			return 0;
		}
		else if(root.left == null && root.right == null) {
			return 1;
		}
		else if (root.left == null) {
			return minDepth(root.right) + 1;
		}
		else if (root.right == null) {                                                                                            
			return minDepth(root.left) + 1;
		}
		else {
			int a = minDepth(root.left);
			int b = minDepth(root.right);
			int ans = Math.min(a, b) + 1;			
			return ans;
		}
 
        
        
        //level order traversal without recursion
//         if(root == null)
//           return 0;
      
//       Queue<TreeNode1> q = new ArrayDeque<>();
// 		q.add(root);
// 		q.add(new TreeNode1(Integer.MIN_VALUE)); //this acts as a separator between two levels
		
// 		int count = 0;
// 		while(!q.isEmpty()) {
			
// 			TreeNode1 temp = q.peek();
// 			q.poll();
			
// 			if(temp.val  == Integer.MIN_VALUE) {
// 				count++;
// 				if(!q.isEmpty()) {
// 					q.add(new TreeNode1(Integer.MIN_VALUE));
// 				}
				
// 			}else {
				
//                 if(temp.left == null && temp.right == null){
//                     return count+1;
//                 }
                
// 				if(temp.left != null) {
// 					q.add(temp.left);
// 				}
				
// 				if(temp.right != null) {
// 					q.add(temp.right);
// 				}
// 			}
			
// 		}
//       return count+1;
        
        
        //Two Stack approach
//         if(root == null)
//             return 0;
        
//         Stack<TreeNode1> s = new Stack<>();
//         Stack<Integer> values = new Stack<>();
        
//         s.push(root);
//         values.push(1);
//         int max =0;
        
//         TreeNode1 temp = root;
//         int min = Integer.MAX_VALUE;
        
//         while(!s.empty()){
            
//             temp = s.pop();
//             int tempValue = values.pop();
//             max = Math.max(tempValue, max);

//             if(temp!=null && temp.left!=null) {
//                 s.push(temp.left);
//                 values.push(tempValue+1);   
//             }
//             if(temp!=null && temp.right!=null){ 
//                 s.push(temp.right);
//                 values.push(tempValue+1);
//             }
            
//            if(temp.left == null && temp.right == null){
//                 if(tempValue < min)
//                     min = tempValue;
//             }
//         }
        
//         return min;
    }
    
    public static void main(String[] args) {
    	TreeNode1 root = new TreeNode1();
		root.val =1 ;
		
		TreeNode1 root2 = new TreeNode1();
		root2.val = 2 ;
		
		TreeNode1 root3 = new TreeNode1();
		root3.val = 3 ;
		
		TreeNode1 root4 = new TreeNode1();
		root4.val = 5;
		
		TreeNode1 root5 = new TreeNode1();
		root5.val = 4;
		
		root.left = root2;
		root.right = root3;
		
//		root2.left = root4;
		root3.right = root5;
		
		MinDepthBinaryTree_Leetcode_111 binaryTree_Leetcode_111 = new MinDepthBinaryTree_Leetcode_111();
		System.out.println(binaryTree_Leetcode_111.minDepth(root));
	}

}
