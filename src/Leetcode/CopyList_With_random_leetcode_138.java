package Leetcode;

public class CopyList_With_random_leetcode_138 {

	// Definition for a Node.
	static class Node {
		int val;
		Node next;
		Node random;

		public Node(int val) {
			this.val = val;
			this.next = null;
			this.random = null;
		}
	}

	public static Node copyRandomList(Node head) {

		Node dummy = new Node(-1);
		Node prev = dummy;
		Node curr = head;

		while (curr != null) {

			Node temp = new Node(curr.val);
			temp.next = curr.next;
			temp.random = curr.random;

			prev = temp;
			prev = prev.next;
			curr = curr.next;
		}

		return dummy.next;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
