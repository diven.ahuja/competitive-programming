package Leetcode;

public class DesignAdd_and_Search_Words_Leetcode_211 {

	static class TrieNode {

		char val;
		TrieNode children[];
		boolean isTerminal;

		TrieNode(char ch) {
			val = ch;
			children = new TrieNode[26];
			isTerminal = false;
		}
	}

	static class WordDictionary {
		static TrieNode root;

		public WordDictionary() {
			root = new TrieNode('\0');
		}

		//insert basic trie implementation
		private static void insertUtil(TrieNode root, String word) {
			if (word.length() == 0) {
				root.isTerminal = true;
				return;
			}

			TrieNode child;
			
			int index = word.charAt(0) - 'a';

			// present
			if (root.children[index] != null) {
				child = root.children[index];
			} else {
				child = new TrieNode(word.charAt(0));
				root.children[index] = child;
			}

			insertUtil(child, word.substring(1));
		}

		public static void addWord(String word) {
			insertUtil(root, word);
		}

		private static boolean searchUtil(TrieNode root, String w) {
	        if(w.length() == 0){
	            if(root.isTerminal)
	                return true;
	            return false;
	        }
	        
	        TrieNode child;
	        
	        //if there is a '.', basically starting a new recursion chain where it will check if the word chain is formed from any
	        // of the root's children. so it tries to form a chain with root's every children.
	        if(w.charAt(0) == '.'){
	            for(int i=0; i<26; i++){
	                if(root.children[i] != null){
	                    if(searchUtil(root.children[i], w.substring(1)))
		                    	return true;
	                }
	            }
	            // else is basic trie implementation if it is present then go for next, else return false
	        }else{
	            int index = w.charAt(0) - 'a';
	        
	            if(root.children[index] != null){
	                child = root.children[index];
	            }else{
	                return false;
	            }

	            return searchUtil(child, w.substring(1));
	        }
	            
	        return false;
	    }


		public static boolean search(String word) {
			return searchUtil(root, word);
		}

		public static void main(String[] args) {

			WordDictionary dictionary = new WordDictionary();
			dictionary.addWord("at");
			dictionary.addWord("bat");
//			dictionary.addWord("a");
//			dictionary.addWord("and");
//			dictionary.addWord("an");
//			dictionary.addWord("add");
//			dictionary.addWord("a");
//			dictionary.addWord(".at");
//			dictionary.addWord("bat");
//			dictionary.addWord("an.");
//			dictionary.addWord("a.d.");
//			dictionary.addWord("b.");
//			dictionary.addWord("a.d");
//			dictionary.addWord(".");
			
//			System.out.println(dictionary.search("aa"));
			System.out.println(dictionary.search(".at"));
			System.out.println();
			
		}

	}
}
