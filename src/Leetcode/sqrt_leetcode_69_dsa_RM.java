package Leetcode;

public class sqrt_leetcode_69_dsa_RM {

	
public static int mySqrt(int x) {
        
        //BINARY SEARCH APPROACH
        if(x < 2)
            return x;
        
        int start = 2;
        int end = x/2;
        int mid;
        
        while(start <= end){
            mid = (start + end)/2;
            long num =  (long) mid*mid;

            if(num > x){
                end = mid-1;
            }else if(num < x){
                start = mid + 1; 
            }else
                return mid;
        }

        return end;
        
        
        //TIME LIMIT EXCEEDED
//         if(x == 1 || x == 2)
//             return 1;
       
//         for(int i=1; i <= Math.ceil(x/2.0); i++){
            
//             if( i*i == x)
//                 return i;
            
//             if(i*i > x)
//                 return i-1;
            
//         }
//         return 0;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(mySqrt(8));
	}

}
