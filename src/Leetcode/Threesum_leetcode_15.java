package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Threesum_leetcode_15 {

	public static List<List<Integer>> threeSum(int[] nums) {

        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
            
        for(int i=0; i<nums.length-2; i++){
            
            int start = i+1, end = nums.length - 1, sum = 0 - nums[i];
            
            if(i == 0 || ( i> 0 && nums[i] != nums[i-1])){
                while(start < end){

                    if(nums[start] + nums[end] == sum){

                        result.add(Arrays.asList(nums[i], nums[start], nums[end]));

                        start++;
                        end--;
                    }
                    else if(nums[start] + nums[end] < sum) start++;
                    else end--;
                }
            }
        }
        
        return result;
        
    }
	
	
//	static HashMap<Integer, Integer> hmap = new HashMap<>();
	
//	public static List<List<Integer>> threeSum(int[] nums) {

//		List<List<Integer>> combined = new ArrayList<List<Integer>>();
//
//		if (nums.length < 3) {
//			return new ArrayList<>();
//		}
//
//		Arrays.sort(nums);
//		List<List<Integer>> finalList = new ArrayList<>();
//
//		for (int i = 0; i < nums.length; i++) {
//
//			if (nums[0] > 0)
//				return new ArrayList<>();
//
//			if (i == 0 || nums[i] != nums[i - 1]) {
//				twoSums(nums, finalList, i);
//			}
//		}
//		// TODO:
//		return finalList;

//List<List<Integer>> combined = new ArrayList<List<Integer>>();
//        
//        for(int i=0; i<nums.length; i++){
//            List<Integer> temp = new ArrayList<>();
//            int fixed = nums[i];
//
//            int[] indexed = twoSumsIndexed(fixed , i, nums, 0, combined);
//            
//            if(indexed[0] == 0  && indexed[1] ==0){
//            	continue;
//            }
//                temp.add(nums[indexed[0]]);
//                temp.add(nums[indexed[1]]);
//                temp.add(fixed);
//                Collections.sort(temp);
//                if(!combined.contains(temp))
//                    combined.add(temp);
//        }
//        return combined;
//		
//		
//	}

	private static void twoSums(int[] nums, List<List<Integer>> finalList, int i) {
		int lo = i + 1;
		int hi = nums.length - 1;

		while (lo < hi) {
			int sum = nums[i] + nums[lo] + nums[hi];

			if (sum < 0) {
				lo++;
			} else if (sum > 0) {
				hi--;
			} else {

				finalList.add(Arrays.asList(nums[i], nums[lo], nums[hi]));

				lo++;
				hi--;

				while (lo < hi && nums[lo] == nums[lo - 1])
					++lo;

			}
		}
	}

//	private static int[] twoSumsIndexed(int fixed, int index, int[] nums, int target, List<List<Integer>> combined) {
//		
//		hmap.put(fixed, index);
//
//		for (int i = 0; i < nums.length; i++) {
//			List<Integer> temp = new ArrayList<>();
//
//			if (index == i)
//				continue;
//			// 0 = fixed + x + y
//			// y = 0 - fixed - x (nums[i]);
//
//			int sub = target - fixed - nums[i];
//			
//
//			System.out.println(sub + "--- "+ fixed + "---- "+nums[i]);
//			if (hmap.containsKey(sub)) {
//				temp.add(nums[hmap.get(sub)]);
//				temp.add(nums[i]);
//				temp.add(fixed);
//				
//				if(hmap.get(sub) == i || hmap.get(sub) == index)
//					continue;
//				
//				Collections.sort(temp);
//				if(!combined.contains(temp))
//					combined.add(temp);
//			}
//			
//			hmap.put(nums[i], i);
//		}
//
//		return new int[] { 0, 0 };
//	}

	public static void main(String[] args) {

		ArrayList<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(2);
		a.add(1, 3);
		System.out.println(a);
				System.out.println(threeSum(new int[] {-1,0,1,2,-1,-4}));
	}

}
