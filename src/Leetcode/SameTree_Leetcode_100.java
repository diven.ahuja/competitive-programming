package Leetcode;

public class SameTree_Leetcode_100 {
	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public boolean isSameTree(TreeNode p, TreeNode q) {

		// solved it on my own
		// My intuition: since we needed to check every node, any traversal would work
		// so i went with PreOrder, thats why in return calling right and left for both the treeNode
		// used && because if any of them returns false then it should go false
		// and if both are true then only it should return true
		// then two base conditions if both are null then true
		// if anyone node is null or if value is mismatch then false
		if (p == null && q == null)
			return true;

		if (p == null || q == null || p.val != q.val)
			return false;

		return isSameTree(p.right, q.right) && isSameTree(p.left, q.left);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
