package Leetcode;

public class winner_tic_tac_leetcode_1275_microsoft {

	static int board[][] = new int[3][3];
	static int player = 1;
	static int count = 0;
	
	public static String tictactoe(int[][] moves) {
		
		for (int i = 0; i < moves.length; i++) {
			int row = moves[i][0];
			int col = moves[i][1];
			count++;
			board[row][col] = player;
			
			
			if(checkRow(row) || checkCol(col) || checkDiag() 
					|| checkAntiDiag()) {
				return player == 1 ? "A" : "B";
			}
			
			player *= -1;
		}
		
		if(count < 9) {
			return "Pending";
		}
		
		
		return "Draw";
	}

	private static boolean checkAntiDiag() {
		for(int i =0 ;i < 3; i++){
			if(board[i][3 - i - 1 ] != player) 
				return false;
		}
		return true;
	}

	private static boolean checkDiag() {
		for(int i =0 ;i < 3; i++){
			if(board[i][i] != player) 
				return false;
		}
		return true;
	}

	private static boolean checkCol(int col) {
		for(int i =0 ;i < 3; i++){
			if(board[i][col] != player) 
				return false;
		}
		return true;
	}

	private static boolean checkRow(int row) {

		for(int i =0 ;i < 3; i++){
			if(board[row][i] != player) 
				return false;
		}		
		return true;
	}
	
	
	public static void main(String[] args) {
		int[][] moves = {{0,0},{1,1},{0,1},{0,2},{1,0}, {2,0}}; //,{0,1},{0,2},{2,2}};
		System.out.println(tictactoe(moves));
	}
	
}
