package Leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

public class LevelOrderTraversal {

	public static int levelTraversal(TreeNode1 root) {

		// using two stacks 
//		Stack<TreeNode1> nodes = new Stack<>();
//		Stack<Integer> values = new Stack<>();
//
//		nodes.push(root);
//		System.out.println(root.val);
//		values.push(1);
//		int max = 0;
//
//		while (!nodes.empty()) {
//
//			TreeNode1 curr = nodes.pop();
//			int temp = values.pop();
//			max = Math.max(max, temp);
//
//			if (curr.left != null) {
//				nodes.push(curr.left);
//				System.out.println(curr.left.val);
//				values.push(temp + 1);
//			}
//			;
//			if (curr.right != null) {
//				nodes.push(curr.right);
//				System.out.println(curr.right.val);
//				values.push(temp + 1);
//			}
//			;
//
//		}
//
//		return max;
		
		
		// using queue
		
		Queue<TreeNode1> que = new ArrayDeque<>();
		que.add(root);
		que.add(new TreeNode1(-134));
		System.out.print(root.val);
		System.out.println();
		int count = 0;
		
		while(!que.isEmpty()) {
			TreeNode1 polledNode = que.remove();
			
			if(polledNode.val == -134) {
				System.out.println();
				count++; 
				
				if (!que.isEmpty()) {
					que.add(new TreeNode1(-134));
				}
			}
			else {
				if(polledNode.left!=null) {
					System.out.print(polledNode.left.val);
					que.add(polledNode.left);
				}
				
				if(polledNode.right!=null) {
					System.out.print(polledNode.right.val);
					que.add(polledNode.right);
				}
			}
		} //while
		
		

		return count;
	}

	public static void main(String[] args) {

		TreeNode1 root = new TreeNode1();
		root.val = 1;

		TreeNode1 root2 = new TreeNode1();
		root2.val = 2;

		TreeNode1 root3 = new TreeNode1();
		root3.val = 3;

		TreeNode1 root4 = new TreeNode1();
		root4.val = 4;

		TreeNode1 root5 = new TreeNode1();
		root5.val = 5;

		root.left = root2;
		root.right = root3;

		root2.left = root4;
		root3.right = root5;

		System.out.println(levelTraversal(root));
	}

}
