package Leetcode;

class ListNode {
	      int val;
	      ListNode next;
	      ListNode() {}
	      ListNode(int val) { this.val = val; }
	      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
	  }

public class Merge_Two_Lists_Leetcode_21 {

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        
//        if(l1 == null) return l2;
//        if(l2 == null) return l1;
//        if(l1.val > l2.val){
//            Stack temp = l1;
//            l1 = l2;
//            l2 = temp;
//        }
//            
//        Stack result = l1;
//        while(l1 != null && l2 != null){
//            
//           Stack tmp = null;
//            
//            while(l1 !=null && l1.val <= l2.val){
//                tmp = l1;
//                System.out.println("result: " +result);
//                System.out.println("l1: "+l1);
//                System.out.println("tmp: " +tmp);
//                l1 = l1.next;
//            }
//            tmp.next = l2; // tmp here is used to remember the last node.
//            // and is updating the value of result, as and when tmp gets updated.
//            // because tmp and result have same memory reference as l1.
//            
//            //swap
//            Stack temp = l1;
//            l1 = l2;
//            l2 = temp; 
//        }
//        return result;
    	
        if(l1 == null) return l2;
        if(l2 == null) return l1;
        
        ListNode temp = new ListNode();
        ListNode prev = temp;
        
        while(l1 != null && l2 != null){
        	
            int mins=0;
        
            if(l1.val < l2.val){
                mins = l1.val;
                l1 = l1.next;
            }else{
                mins = l2.val;
                l2 = l2.next;
            }
            
            ListNode ans = new ListNode();
            ans.val = mins;
            prev.next = ans;
            prev = ans;
        }
        
        if(l1 != null) {
        	prev.next = l1;
        }
        if(l2 != null) {
        	prev.next = l2;
        }
        
        
        return temp.next;
    	
    }
	
	public static void main(String[] args) {
		
		ListNode listNode = new ListNode();
		listNode.val = 1;
		ListNode list2 = new ListNode();
		list2.val = 3;
		ListNode list3 = new ListNode();
		list3.next = null;
		list3.val = 4;
		
		listNode.next = list2;
		list2.next = list3;

		ListNode listN = new ListNode();
		listN.val = 1;
		ListNode listN2 = new ListNode();
		listN2.val = 2;
		ListNode listN3 = new ListNode();
		listN3.next = null;
		listN3.val = 4;
		
		listN.next = listN2;
		listN2.next = listN3;
		
		
		listNode = mergeTwoLists(listNode, listN);
		System.out.println(listNode.val);
		System.out.println(listNode.next.val);
		System.out.println(listNode.next.next.val);
		System.out.println(listNode.next.next.next.val);
		System.out.println(listNode.next.next.next.next.val);
		System.out.println(listNode.next.next.next.next.next.val);
	}

}
