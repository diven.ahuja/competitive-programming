package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class BinaryTree_from_inorder_preorder_leetcode_105 {

    //Time Complexity O(n) Space Complexity O(n)
    //Dry run it and understand it more better
	public static TreeNode buildTree(int[] preorder, int[] inorder) {
		Map<Integer, Integer> hmap = new HashMap<>();
		for (int i = 0; i < inorder.length; i++) {
			hmap.put(inorder[i], i);
		}
		
		return treeBuilder(preorder, 0, preorder.length-1, inorder, 0, 
				inorder.length-1, hmap);
		
	}

	private static TreeNode treeBuilder(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd,
			Map<Integer, Integer> hmap) {
		// TODO Auto-generated method stub
		
		if(preStart > preEnd || inStart > inEnd) return null;
		
		TreeNode root = new TreeNode(preorder[preStart]);
		
		int inRoot = hmap.get(root.val); // returning index of element
		int numsLeft = inRoot - inStart;
		
		root.left = treeBuilder(preorder, preStart + 1 , preStart + numsLeft, 
				inorder, inStart, inRoot - 1, hmap);
		
		root.right = treeBuilder(preorder, preStart + numsLeft + 1, preEnd, 
				inorder, inRoot + 1, inEnd, hmap);
		return root;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
