package Leetcode;

public class Test17 {
	
	public static int shortestDistance(String[] wordsDict, String word1, String word2) {

		int index1 = 0, index2 = 0;
		int count = 0, sum = 0;
		for (String word : wordsDict) {
			count++;
			if (word.equals(word1)) {
				index1 = count;
			}
			if (word.equals(word2)) {
				index2 = count;
			}

			if (index1 > 0 && index2 > 0) {
				if (sum == 0) {
					sum = Math.abs((index1 - index2));
				}
				int golmaal = Math.abs((index1 - index2));
				if (golmaal < sum) {
					sum = golmaal;
				}

			}
		}

		return sum;
	}
	
	public static void main(String[] args) {
		System.out.println(shortestDistance(new String[] {"a","b","c","a"}, "a", "c"));
	}
}
