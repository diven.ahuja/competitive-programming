package Leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;

public class kClosesttoOrigin_leetcode_973 {

	public static int[][] kClosest(int[][] points, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(k, new Comparator<int[]>(){
            
            public int compare(int[] a, int[] b){
            	// calculating euclidean distance of each array and then comparing 
            	// and then forming a maxheap with max on top of heap
                return ((b[0]*b[0] + b[1]*b[1]) - (a[0]*a[0] + a[1]*a[1]));
            }
        });

        for(int point[]: points){
            pq.add(point);
            
            if(pq.size() > k)
                pq.poll();
        }
        
        return pq.toArray(new int[0][0]);
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[][] points = new int[][] {{1,1},{2,2},{2,2},{2,-2}};
		
		kClosest(points, 2);

	}

}
