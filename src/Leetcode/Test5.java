package Leetcode;

public class Test5 {

	
	public static String solve(String A) {
		
		
		char letter[] = A.trim().toCharArray();
		String[] words = breakIntoWords(letter);
		String finalWord = "";
		for(int k = words.length-1 ; k >= 0 ; k -- ) {
			if (words[k] !=null) {
				finalWord = finalWord + ' ' + words[k];
			}
				
		}
		
				
		return finalWord.trim();
	}
	
	private static String[] breakIntoWords(char[] letter) {
		int i = 0;
		String word = "";
		String[] words = new String[letter.length];
		
		
		for (int j = 0; j < letter.length; j++) {
			
			while(i < letter.length && letter[i] != ' ') {
				word = word + letter[i];
				i++;
			}
			i++;
			if(word != null && word != "") {
				words[j] = word;
				word = "";
			}
		}
		
		return words;
	}
	public static void main(String[] args) {

		System.out.println(solve("   the sky is blue   "));
	}

}
