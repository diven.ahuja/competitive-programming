package Leetcode;

import java.util.Arrays;

public class Sort_colors_0_1_2_leetcode {

public static void sortColors(int[] nums) {
    //Optimal Dutch flag algorithm
	  int low=0, mid =0, high=nums.length-1;
      int temp;
      
      while(mid <= high){
          
          switch(nums[mid]){
              case 0:
                  temp = nums[low];
                  nums[low] = nums[mid];
                  nums[mid] = temp;
                  low++;
                  mid++;
                  break;
              case 1:
                  mid++;
                  break;
              case 2:
                  temp = nums[mid];
                  nums[mid] = nums[high];
                  nums[high] = temp;
                  high--;
                  break;
          }

          
          
      }
      
      
      // slightly better, using count solution
//       int count_0 = 0;
//       int count_1 = 0;
//       int count_2 = 0;
      
//       for(int i =0 ; i< nums.length; i++){
//           if(nums[i] == 0){
//               count_0++;        
//           }else if(nums[i] == 1){
//               count_1++;
//           }else{
//               count_2++;
//           }
//       }
      
//       int index = 0;
//       for(int j=0;j<count_0; j++){
//           nums[index] = 0;
//           index++;
//       }
      
//       for(int k=0; k<count_1; k++){
//           nums[index] = 1;
//           index++;
//       }
//       for(int l=0; l<count_2; l++){
//           nums[index] = 2;
//           index++;
//       }
      
      // using brute force sorting algo o(n2)
//        for(int i=0; i < nums.length; i++){
//           int min = nums[i];
//           int index = i;
//           boolean flag = false;
//           for(int j=i+1; j < nums.length; j++){
//               if(nums[j] < min){
//                   min = nums[j];
//                   index = j;
//                   flag = true;
//               }
//           }
//            if(flag) {
//           	int temp = nums[i];
//           	nums[i] = min;
//           	nums[index] = temp;
//           }
//       }
        
    }
	
	public static void main(String[] args) {
		sortColors(new int[] {5,4,3,2,1});
	}
}
