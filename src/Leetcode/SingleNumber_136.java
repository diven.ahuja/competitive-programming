package Leetcode;

import java.util.Arrays;
import java.util.HashMap;

public class SingleNumber_136 {

	public static int singleNumber(int[] nums) {
	    
		HashMap<Integer , Integer> hashMap = new HashMap<Integer, Integer>();
		
		hashMap.put(nums[0], 1);
		
		for (int i = 1; i < nums.length; i++) {
			if(hashMap.containsKey(nums[i])) {
				hashMap.put(nums[i], 2);
			}else {
				hashMap.put(nums[i], 1);
			}
		}
		
		 for(int i:hashMap.keySet())
	        {
	            if(hashMap.get(i)==1)
	            {
	                return i;
	            }
	        }
		 
		return 0;
//        Arrays.sort(nums);
//        
//        for(int i =0; i< nums.length-1;i+=2){
//            if(nums[i] != nums[i+1]){
//                return nums[i];
//            }
//        }
//        
//        if(nums[nums.length-1] != nums[nums.length-2]){
//            return nums[nums.length-1];
//        }
//        
//        return 0;
    }
	
	public static void main(String[] args) {
		System.out.println(singleNumber(new int[] {4,1,2,1,2}));
	}
}
