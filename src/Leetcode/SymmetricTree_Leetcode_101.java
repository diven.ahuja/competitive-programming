package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class SymmetricTree_Leetcode_101 {

	public static boolean isSymmetric(TreeNode root) {   
		if(root == null)
        return true;
    
    Queue<TreeNode> q = new LinkedList<>(); // not using ArrayDeque because 
    // ArrayDeque cannot accept null elements.
    q.add(root);
    q.add(root);
    
    while(!q.isEmpty()){
        
        TreeNode t1 = q.remove();
        TreeNode t2 = q.remove();
        
        if(t1 == null && t2 == null)
            continue;
        
        if(t1 == null || t2 == null)
            return false;
        
        if(t1.val != t2.val)
            return false;
        
        q.add(t1.left);
        q.add(t2.right);
        q.add(t1.right);
        q.add(t2.left);
        
    }
    
    return true;        
    
    // below is a messy approach... above is the same solution approach but written in a much better and a clean way.
//       if(root == null)
//        return false;
   
//    Queue<TreeNode> q = new ArrayDeque<TreeNode>();
//    q.add(root);
//    q.add(new TreeNode(-134));
   
//    List<TreeNode> leftList = new ArrayList<>();
//    List<TreeNode> rightList = new ArrayList<>();
   
//    while(!q.isEmpty()){
//        TreeNode temp = q.remove();
           
//        if(temp.val == -134){
           
//            if(leftList.isEmpty() && !rightList.isEmpty() 
//         		   || !leftList.isEmpty() && rightList.isEmpty()
//              || leftList.size() != rightList.size()) {
//                return false; 
//            }

//            Collections.reverse(rightList);
//            if(!leftList.isEmpty() && !rightList.isEmpty()) {
//					for (int i = 0; i < leftList.size(); i++) {
                    
//						if(leftList.get(i).left == null 
//                       && leftList.get(i).right == null
//                       && rightList.get(i).right != null 
//                       && rightList.get(i).left != null
//                       )
//							return false;
                    
//						if(leftList.get(i).val != rightList.get(i).val)
//							return false;
//					}
               
//                leftList = new ArrayList<>();
//                rightList = new ArrayList<>();
//        	    }
           
//            if(!q.isEmpty()){
//                q.add(new TreeNode(-134));
//            }
//        }else{
//                if(temp.left != null){
//                    leftList.add(temp.left);
//                    q.add(temp.left);
//                }
//                if(temp.right != null) {
//                    rightList.add(temp.right);
//                    q.add(temp.right);
//                }
//        }
       
//    } //while
   
//    return true;

}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeNode root = new TreeNode();
		root.val = 1;

		TreeNode root2 = new TreeNode();
		root2.val = 2;

		TreeNode root3 = new TreeNode();
		root3.val = 2;

		TreeNode root4 = new TreeNode();
		root4.val = 3;

		TreeNode root5 = new TreeNode();
		root5.val = 3;

		TreeNode root6 = new TreeNode();
		root6.val = 4;
		
		TreeNode root7 = new TreeNode();
		root7.val = 4;

		TreeNode root8 = new TreeNode();
		root8.val = 5;
		
		TreeNode root9 = new TreeNode();
		root9.val = 5;
		
		root.left = root2;
		root.right = root3;

		root2.left = root4;
		root3.right = root5;
		
		root2.right = root6;
		root3.left = root7;
		
		root6.right = root8;
		root5.right = root9;
				
		
		System.out.println(isSymmetric(root));
	}

}
