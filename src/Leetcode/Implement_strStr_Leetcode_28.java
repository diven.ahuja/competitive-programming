package Leetcode;

public class Implement_strStr_Leetcode_28 {

public static int strStr(String haystack, String needle) {
        
        char[] haystackChar = haystack.toCharArray(); 
        char[] needleChar = needle.toCharArray();
        int index = 0;
        
        if(needle.equals(""))
            return -1;
        
        if(haystack.equals(needle))
            return 0;
        
        for(int i=0; i<haystackChar.length; i++){
            
            if(index == needleChar.length){
                return i-index;
            }
            
            if(haystackChar[i] == needleChar[index])
            {
                index++;
            }else{
            	i = i - index;
                index = 0;
            }
        }
    
        if(index == needleChar.length){
                return haystackChar.length-index;
            }
        
        return -1;
    }
	
public static void main(String[] args) {
	System.out.println(strStr("bbaabbbbbaabbaabbbbbbabbbabaabbbabbabbbbababbbabbabaaababbbaabaaaba",
			"babaaa"));

}
}
