package Leetcode;

public class Zigzag_conversion_leetcode_6 {
    public static String convert(String s, int numRows) {
        
        int len = s.length();
        int col = 0;
        int gap = numRows - 2;
        
        //finding number of columns
        while(len>0){
            
            len -= numRows;
            col++;
            
            if((len - gap) > 0){
                len -= gap;
                col++;
            }
                
        }
        
        //starting to put in 2D array        
        int index = 0;
        char data[][] = new char[numRows][col];
        int column = 0;
        
        while(index < s.length()){
            for(int i=0; i<numRows && index < s.length(); i++){
            	System.out.println(s.charAt(index));
                if(column%gap <= gap){ // this is for single element. leaving it here
                    data[1][column] = s.charAt(index);
                    index++;                    
                    break;
                }else{
                    data[i][column] = s.charAt(index);
                    index++;
                }
            }
            column++;
        } //while
        
        String result = "";
        for(int i=0; i< numRows; i++){
            for(int j=0; j< col; j++){
            	if(data[i][j]!='\0')
            		result += data[i][j];
            }
        }
        
        
        return result;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(convert("PAYPALISHIRING", 3));
	}

}
