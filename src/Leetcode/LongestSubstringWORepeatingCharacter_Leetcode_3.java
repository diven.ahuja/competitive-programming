package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class LongestSubstringWORepeatingCharacter_Leetcode_3 {

	public static int lengthOfLongestSubstring(String password) {
		
		int result = 0;
		
		if(!password.isEmpty()) {
			result = 1;
		}

		HashSet<Character> hset = new HashSet<>();
		for(int i=0; i<password.length(); i++) {
			if(hset.contains(password.charAt(i))) {
				hset.clear();
				System.out.println(password.charAt(i));
				result++;
			}
			hset.add(password.charAt(i));
		}
		
		return result;
//		int max = 0;
//		int i = 0, n = s.length();
//		Map<Character, Integer> hmap = new HashMap<>();
//
//		for (int j = 0; j < n; j++) {
//			if (hmap.containsKey(s.charAt(j))) {
//				i = Math.max(hmap.get(s.charAt(j)), i);
//			}
//			hmap.put(s.charAt(j), j + 1);
//			max = Math.max(max, j - i + 1);
//		}
//
//		return max;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(lengthOfLongestSubstring("aabbbsaewpop"));
	}

}
