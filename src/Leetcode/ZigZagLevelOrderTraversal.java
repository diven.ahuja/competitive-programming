package Leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class ZigZagLevelOrderTraversal {

public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        
        List<List<Integer>> zigzagList = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        
        if(root == null)
            return zigzagList;
        
        Queue<TreeNode> q = new ArrayDeque<TreeNode>();
        q.add(root);
        q.add(new TreeNode(-134));
        list.add(root.val);
        zigzagList.add(list);
        
        int player = -1;
        
        List<Integer> nodeList = new ArrayList<>();
        while(!q.isEmpty()){
            TreeNode temp = q.remove();
            
            if(temp.val == -134){
            	
            	if(!nodeList.isEmpty()) {
                    zigzagList.add(nodeList);
                    nodeList = new ArrayList<>();
            	}
            	
            	player = player * -1;
                if(!q.isEmpty()){
                    q.add(new TreeNode(-134));
                }
            }else{
                if(player == 1){
                    if(temp.left != null){
                        nodeList.add(temp.left.val);
                        q.add(temp.left);
                    }
                    if(temp.right != null) {
                        nodeList.add(temp.right.val);
                        q.add(temp.right);
                    }
                }else{
                    if(temp.right != null) {
                        nodeList.add(temp.right.val);
                        q.add(temp.right);
                    }
                    if(temp.left != null){
                        nodeList.add(temp.left.val);
                        q.add(temp.left);
                    }
                }
            }
            
        } //while
        
        return zigzagList;
        
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeNode root = new TreeNode();
		root.val = 1;

		TreeNode root2 = new TreeNode();
		root2.val = 2;

		TreeNode root3 = new TreeNode();
		root3.val = 3;

		TreeNode root4 = new TreeNode();
		root4.val = 4;

		TreeNode root5 = new TreeNode();
		root5.val = 5;

		root.left = root2;
		root.right = root3;

		root2.left = root4;
		root3.right = root5;
		System.out.println(zigzagLevelOrder(root));
	}

}
