package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger_Leetcode_13 {

static Map<String, Integer> values = new HashMap<>();
    
    static {
        values.put("M", 1000);
        values.put("D", 500);
        values.put("C", 100);
        values.put("L", 50);
        values.put("X", 10);
        values.put("V", 5);
        values.put("I", 1);
        
        values.put("CM", 900);
        values.put("CD", 400);
        
        values.put("XL", 40);
        values.put("XC", 90);
        
        values.put("IX", 9);
        values.put("IV", 4);
    }
    
    public static  int romanToInt(String s) {
        
        int res = 0;
        StringBuilder resultString = new StringBuilder();
            
        for(int i=0; i< s.length(); i++){
            String temp = s.charAt(i)+"" + s.charAt(i+1)+"";
            int val = values.get(temp);
            if(val == 0)
                val = values.get(s.charAt(i)+"");
            
            resultString.append(val);
        }

        return Integer.parseInt(resultString.toString());
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
