package Leetcode;

import java.util.HashMap;
import java.util.Iterator;

public class HitCounter_Leetcode_microsoft_362 {

	    HashMap<Integer, Integer> count = new HashMap<>();
	    HashMap<Integer, Integer> countAsWeGo = new HashMap<>();
	    
	    public HitCounter_Leetcode_microsoft_362() {
	        
	    }
	    
	    public void hit(int timestamp) {
	     
	        if(count.containsKey(timestamp)) {
	        	int a = count.get(timestamp);
	        	a += 1;
	        	count.put(timestamp, a);
	        }else {
	        	count.put(timestamp, 1);
	        }
	        	
	        
	    }
	    
	    public int getHits(int timestamp) {
	        int i = timestamp < 300 ? 1 : 1 + (timestamp-300);
	        int finalCount = 0;
	        
	        Iterator <Integer> it = count.keySet().iterator();
	        while(it.hasNext())  
	        {  
	        	int key=(int)it.next();
	        	if(key >= i) {
	        		finalCount += count.get(key);
	        	}
	        }	        
	        
	        
//	    	while(i <= timestamp) {
//	        	if(count.containsKey(i)) {
//	        		finalCount += count.get(i);
//	        		countAsWeGo.put(i, finalCount);
//	        	}
//	        	i++;
//	        }
	    	
	    	return finalCount;
	    }
	}

	/**
	 * Your HitCounter object will be instantiated and called as such:
	 * HitCounter obj = new HitCounter();
	 * obj.hit(timestamp);
	 * int param_2 = obj.getHits(timestamp);
	 */

class ab{

	
	public static void main(String[] args) {
		
		HitCounter_Leetcode_microsoft_362 counter_Leetcode_microsoft_362= new HitCounter_Leetcode_microsoft_362();
		counter_Leetcode_microsoft_362.hit(1);
		counter_Leetcode_microsoft_362.hit(2);
		counter_Leetcode_microsoft_362.hit(3);
		System.out.println(counter_Leetcode_microsoft_362.getHits(4));
		counter_Leetcode_microsoft_362.hit(300);
		System.out.println(counter_Leetcode_microsoft_362.getHits(300));
//		counter_Leetcode_microsoft_362.hit(301);
		System.out.println(counter_Leetcode_microsoft_362.getHits(301));
	}
}


