package Leetcode;
import java.util.HashSet;

public class Test11 {

	public static int removeDuplicates(int[] nums) {
        
        HashSet<Integer> a = new HashSet<Integer>();
        int[] finalnums = nums;
        
        
        
        int count = 0;
        for(int i =0;i<finalnums.length;i++){
            if(!a.contains(finalnums[i])){
                a.add(finalnums[i]);
                nums[count] = finalnums[i];
                count++;
            }
        }
        
        return a.size();
    }
	
	public static void main(String[] args) {

		int[] nums = new int[3];
		nums[0] = 1;
		nums[1] = 1;
		nums[2] = 2;
		removeDuplicates(nums);
	}

}
