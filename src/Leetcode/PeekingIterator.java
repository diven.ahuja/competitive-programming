package Leetcode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class PeekingIterator implements Iterator<Integer> {
	
	Integer num = null;
	int count = 0;
	List<Integer> list = null;

	
	public PeekingIterator(Iterator<Integer> iterator) {
	    // initialize any member here.
		list = new ArrayList<Integer>();
		
		while(iterator.hasNext()) {
			list.add(iterator.next());
		}
	}
	
    // Returns the next element in the iteration without advancing the iterator.
	public Integer peek() {
		
        return list.size()==0? null: list.get(0);
	}
	
	// hasNext() and next() should behave the same as in the Iterator interface.
	// Override them if needed.
	@Override
	public Integer next() {
		
		num = list.get(0);
		list.remove(0);
		return num;
	}
	
	@Override
	public boolean hasNext() {
	    return list.isEmpty();
	}
}