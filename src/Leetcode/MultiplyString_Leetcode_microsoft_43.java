package Leetcode;

import java.util.ArrayList;

public class MultiplyString_Leetcode_microsoft_43 {
	public String multiply(String A, String B) {
		int sum = 0;

		int carry = 0;

		String max = "", min = "";

		if (A.equals("0") || B.equals("0"))

			return "0";

		if (A.length() > B.length()) {

			max = A;

			min = B;

		}

		else {

			min = A;

			max = B;

		}

		String row = "";

		ArrayList<String> values = new ArrayList<String>();

		int count = 0, z = 0;

		for (int i = min.length() - 1; i >= 0; i--) { // A = 4678935

			int a = Integer.parseInt(min.charAt(i) + ""); // 5 last digit

			while (z < count) {

				row = row + "0";

				z++;

			}

			z = 0;

			count++;

			for (int j = max.length() - 1; j >= 0; j--) { // B = 10347

				int b = Integer.parseInt(max.charAt(j) + "");

				sum = a * b;

				if (carry == 0)

					carry = sum / 10;

				else {

					sum = sum + carry;

					carry = (sum) / 10;

				}

				row = row + sum % 10 + "";

				if (j == 0 && carry != 0) {

					row = row + carry;

				}

			}

			StringBuffer sb = new StringBuffer(row).reverse();

			values.add(sb.toString());

			carry = 0;

			sum = 0;

			row = "";

			if (!values.isEmpty() && values.size() == 2) {

				String num1 = values.get(0);

				String num2 = values.get(1);

				if (num1.length() != num2.length()) {

					if (num1.length() > num2.length()) {

						int diff = num1.length() - num2.length();

						String abc = "%0" + diff + "d%s";

						num1 = String.format(abc, 0, num2);

					} else {

						int diff = num2.length() - num1.length();

						String abc = "%0" + diff + "d%s";

						num1 = String.format(abc, 0, num1);

					}

				}

				String maximum = num1.length() > num2.length() ? num1 : num2;

				int carrier = 0;

				for (int j = maximum.length() - 1; j >= 0; j--) {

					int a1 = 0;

					int b1 = 0;

					a1 = Integer.parseInt(num1.charAt(j) + "");

					b1 = Integer.parseInt(num2.charAt(j) + "");

					int total = a1 + b1;

					if (carrier == 0)

						carrier = total / 10;

					else {

						total = total + carrier;

						carrier = (total) / 10;

					}

					row = row + total % 10 + "";

					if (j == 0 && carrier != 0) {

						row = row + carrier;

					}

				}

				values.remove(1);

				values.remove(0);

				StringBuffer sb1 = new StringBuffer(row).reverse();

				values.add(sb1.toString());

				row = "";

				carrier = 0;

			}

		}

		int countnum = 0;

		for (int i = 0; i < values.get(0).length(); i++) {

			if (values.get(0).charAt(i) == '0') {

				countnum++;

			} else {

				break;

			}

		}

		return values.get(0).substring(countnum) + "";
	}
}
