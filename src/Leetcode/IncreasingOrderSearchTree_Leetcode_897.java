package Leetcode;

import java.util.Iterator;
import java.util.TreeSet;

public class IncreasingOrderSearchTree_Leetcode_897 {

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	TreeSet<Integer> hset = new TreeSet<Integer>();

	public TreeSet<Integer> traverse(TreeNode root) {
		if (root == null)
			return hset;

		hset.add(root.val);
		traverse(root.left);
		traverse(root.right);

		return hset;
	}

	public TreeNode increasingBST(TreeNode root) {
		traverse(root);

		TreeNode temp = new TreeNode(-1);
		TreeNode dump = temp;

		Iterator<Integer> it = hset.iterator();

		while (it.hasNext()) {
			TreeNode curr = new TreeNode(it.next());
			dump.right = curr;
			dump = dump.right;
		}
		return temp.right;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
