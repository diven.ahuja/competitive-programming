package Leetcode;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Sort_array_by_frequency_leetcode_1636 {
public static int[] frequencySort(int[] nums) {
        
        HashMap<Integer, Integer> hmap = new LinkedHashMap<>();
        
        for(int i : nums){
            hmap.put(i, hmap.getOrDefault(i, 0) + 1);
        }
        int[] arr = new int[nums.length];
       hmap = sortHashMapByValues(hmap);
        
        int index = 0;
        for(Map.Entry<Integer,Integer> entry : hmap.entrySet()){
            for(int i=0; i< entry.getValue(); i++){
                arr[index] = entry.getKey();
                index++;
            }
        }
        
        return arr;
    }
    
    public static HashMap<Integer, Integer> sortHashMapByValues(HashMap<Integer, Integer> hmap) {
            
        List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer> >(hmap.entrySet());
        
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>(){
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2)
            {
                if(o1.getValue() == o2.getValue()){
                	System.out.println("Key 1: " + o1.getKey()+" key 2: " +o2.getKey()+" subtract : "+ (o2.getKey() - o1.getKey()));
                    return o2.getKey() - o1.getKey();
                }
                System.out.println("Value 1: " + o1.getValue()+" Value 2: " +o2.getValue()+" subtract : "+ (o1.getValue() - o2.getValue()));
                return o1.getValue() - o2.getValue();
            }
        });
        
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for(Map.Entry<Integer, Integer> curr : list){
            temp.put(curr.getKey(), curr.getValue());
        }
        
        return temp;
        
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		frequencySort(new int[] { 3, 1, 2, 2, 3 });
	}

}
