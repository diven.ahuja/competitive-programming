package Leetcode;

public class Maximum_No_Words_Leetcode_2114 {

	public int mostWordsFound(String[] sentences) {
		int maxi = Integer.MIN_VALUE;

		for (String s : sentences) {
			maxi = Math.max(s.split(" ").length, maxi);
		}

		return maxi;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
