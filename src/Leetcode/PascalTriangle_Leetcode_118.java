package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalTriangle_Leetcode_118 {

    public static List<List<Integer>> generate(int numRows) {
        
        List<List<Integer>> finalList = new ArrayList<>();
        
        if(numRows == 1){
            finalList.add(Arrays.asList(1));
            return finalList;
        }
        if(numRows == 2){
            finalList.add(Arrays.asList(1));
            finalList.add(Arrays.asList(1,1));
            return finalList;
        }
        
		finalList.add(Arrays.asList(1));
		finalList.add(Arrays.asList(1, 1));
		recursionHelper(finalList, numRows);
            
        return finalList;
    }
	
	private static void recursionHelper(List<List<Integer>> finalList, int numRows) {
		
		if(finalList.size() == numRows) {
			return;
		}

		List<Integer> abc = finalList.get(finalList.size()-1);
		List<Integer> initList = new ArrayList<>();
		
		initList.add(1);
		for(int i=1; i<abc.size(); i++) {
			initList.add(abc.get(i) + abc.get(i-1));
		}
		initList.add(1);
		finalList.add(initList);

		recursionHelper(finalList, numRows);
	}

	public static void main(String[] args) {
		
		System.out.println(generate(5));
		
	}

}
