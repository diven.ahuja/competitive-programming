package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class CountBinarySubstring_Amazon_Leetcode_696 {

	public static int countBinarySubstrings(String s) {

//      char ab[] = s.toCharArray();
//      int current=1, prev=0, ans=0;

//      for(int i=1; i<s.length(); i++){
//          if(ab[i] != ab[i-1]){
//              ans += (Math.min(current, prev));
//              prev = current;
//              current = 1;
//          }else{
//              current++;
//          }
//      }

//      return ans + Math.min(prev, current);

		char ab[] = s.toCharArray();
		int current = 1, ans = 0;
		List<Integer> a = new ArrayList<Integer>();
		
		for (int i = 1; i < s.length(); i++) {
			if (ab[i] != ab[i - 1]) {
				a.add(current);
				current =1;
			}else {
				current++;
			}
		}
		
		a.add(current);

		for (int i = 1; i < a.size(); i++) {
			ans += Math.min(a.get(i), a.get(i-1));
		}
		
		return ans;
	}

	public static void main(String[] args) {

		System.out.println(countBinarySubstrings("0001110"));
	}

}
