package Leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

 class TreeNode1 {
     int val;
     TreeNode1 left;
      TreeNode1 right;
      TreeNode1() {}
      TreeNode1(int val) { this.val = val; }
      TreeNode1(int val, TreeNode1 left, TreeNode1 right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }

public class RightSideViewBinaryTree_Leetcode_199 {
	
	private void helper(TreeNode1 root, List<Integer> result, int level) {
		if(root == null)
			return;

		if(result.size() == level)
			result.add(root.val);
			
		helper(root.right, result, level+1);
		helper(root.left, result, level+1);
	}
	
	

	public List<Integer> rightSideView(TreeNode1 root) {
		
		//recursive way
		List<Integer> rightView = new ArrayList<Integer>();
		helper(root, rightView, 0);
		return rightView;
		
		
		//non recursive way
		
//		List<Integer> rightView = new ArrayList<Integer>();
//		
//		Queue<TreeNode1> q = new ArrayDeque<>();
//		q.add(root);
//		q.add(new TreeNode1(Integer.MIN_VALUE)); //this acts as a separator between two levels
//		
//		int index = 0;
//		while(!q.isEmpty()) {
//			
//			TreeNode1 temp = q.peek();
//			q.poll();
//			
//			if(temp.val  == Integer.MIN_VALUE) {
//				index++;
//				if(!q.isEmpty()) {
//					q.add(new TreeNode1(Integer.MIN_VALUE));
//				}
//				
//			}else {
//				if(rightView.size() > index)
//					rightView.remove(index);
//				rightView.add(index, temp.val);
//				if(temp.left != null) {
//					q.add(temp.left);
//				}
//				
//				if(temp.right != null) {
//					q.add(temp.right);
//				}
//			}
//			
//		}
//		
//		return rightView;
    }
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RightSideViewBinaryTree_Leetcode_199 leetcode_199 = new RightSideViewBinaryTree_Leetcode_199();
		
		TreeNode1 root = new TreeNode1();
		root.val =1 ;
		
		TreeNode1 root2 = new TreeNode1();
		root2.val = 2 ;
		
		TreeNode1 root3 = new TreeNode1();
		root3.val = 3 ;
		
		TreeNode1 root4 = new TreeNode1();
		root4.val = 5;
		
		TreeNode1 root5 = new TreeNode1();
		root5.val = 4;
		
		root.left = root2;
		root.right = root3;
		
		root2.left = root4;
		root3.right = root5;
		
		
		System.out.println(leetcode_199.rightSideView(root));
	}

}
