package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class IsPlaindrom_Linked_List_Leetcode_234 {

	public static boolean isPalindrome(ListNode head) {

		//Optimal approach with no extra space
		int count = 0;
		ListNode temp = head;
		
		//finding length to find the mid
		while(temp != null)
		{
			count++;
			temp = temp.next;
		}

		//setting mid as per even or odd
		int mid = (count/2);
		if(count%2 != 0)
			mid = (count/2) + 1;
		
		//creating second Half
		ListNode secondHalf = null;
		ListNode tmp = head;
		for(int i=0; i<count; i++) {
			if(i >= mid) {
				secondHalf = tmp;
				break;
			}
			tmp = tmp.next;
		}
		
		//reversing the second half
		ListNode secondRev = secondHalf;
		ListNode prev = null;
		while(secondRev != null) {
			ListNode future = secondRev.next;
			secondRev.next = prev;
			prev = secondRev;
			secondRev = future;
		}
		
		//comparing first half and second half
		for(int i=mid; i<count; i++) {
			if(head.val != prev.val)
				return false;
			prev = prev.next;
			head = head.next;
		}
		
		
		return true;
		
		
		//This is another naive approach but this i couldnt
		// think of because i though they only
		// wanted us to do this with linked list
		
//		List<Integer> a = new ArrayList<>();
//		while(head!=null) {
//			a.add(head.val);
//			head = head.next;
//		}
//		
//		int j = a.size()-1;
//		for(int i=0; i< a.size()/2; i++) {
//			if(a.get(i)!=a.get(j))
//				return false;
//			j--;
//		}
//		return true;
		
		
		//Below approach correct but time exceed for last 3 test case
		// problem is i'm every time going till the tail 
		// to compare it with head.. to find a palindrome
		// which is increasing complexity a lot
//		if (head == null || head.next == null)
//			return true;
//
//		while (head != null) {
//
//			int temp1 = head.val;
//
//			Stack temp = head;
//			Stack prev = null;
//			while (temp.next != null) {
//				prev = temp;
//				temp = temp.next;
//			}
//
//			int temp2 = temp.val;
//			if (prev != null && prev.next != null)
//				prev.next = null;
//
//			if (temp1 != temp2)
//				return false;
//
//			head = head.next;
//
//		}
//		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ListNode lN = new ListNode(0);
		ListNode list2 = new ListNode(3);
		ListNode list3 = new ListNode(3);
		ListNode list4 = new ListNode(0);
//		Stack list5 = new Stack(0);
		
		lN.next = list2;
		list2.next = list3;
		list3.next = list4;
//		list4.next = list5;
		System.out.println(isPalindrome(lN));
	}

}
