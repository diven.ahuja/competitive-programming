package Leetcode;

public class Rotate_Linked_List_61 {

public static ListNode rotateRight(ListNode head, int k) {
	// below is the most optimal
	// where it just points the last to head..
	// forming a circular Linked list
	// and then setting k - (k%length) node's next to null 
	// thus de-chaining the circular linked list
	
	if(head == null || head.next == null)
        return head;
        
  	ListNode temp = head;
	int count = 1;
	// step 1: counting the length 
    while(temp.next != null) {
		temp = temp.next;
		count++;
	}
	
    // and making it a circular node    
	temp.next = head;
	
        
    // find the start of the final linklist    
	ListNode cutting = head;
	k = (k%count);
	k = count - k;
	for(int i =0; i< k ; i++) {
		cutting = cutting.next;
	}
    //setting the start to head    
	head = cutting;
     
    // from head till length we are forming the 
    // final link list
	ListNode tmp = head;
	ListNode prev = null;
	
	for(int i =0; i< count ; i++) {
		prev = tmp;
		tmp = tmp.next;
	}
	prev.next = null;

    // returning final link list    
	return head;
	
	// a much better approach
	// only change done here is avoiding k no of unwanted rotations by doing
	// k % length of nodes
	// since after k = length rotations it comes to its original place

//    if(head == null || head.next == null)
//        return head;
//    
//    int length=1;
//    Stack last = head;
//    while(last.next!=null){
//        last=last.next;
//        length++;
//    }
//    
//    int i = 0;
//    Stack tail = head;
//    
//    while(i < (k%length)){
//    	Stack prev = null;
//    	//got the tail
//        while(tail.next != null){
//        	prev = tail;
//            tail = tail.next;
//        }
//        prev.next = null; // removing tail from head
//        
//        Stack newNode = tail; // putting tail in newNode
//        newNode.next = head; // appending the entire modified body to the tail node
//        head = newNode;
//
//        i++;
//    }
//    print(head);
//    return head;
        
	
//	Brute force approach.. running actual k times. 
	
//        Stack tail = head;
//        
//        int i = 0;
//        while(i < k){
//        	
//        	Stack prev = null;
//        	//got the tail
//            while(tail.next != null){
//            	prev = tail;
//                tail = tail.next;
//            }
//            prev.next = null; //only previous inside
//            // last node is deleted from head
//            // last node is saved in tail
//            
//            Stack newNode = tail; // aa gaya isme tail
//            newNode.next = head; // appending the entire body to the tail node
//            head = newNode;
//
//            i++;
//        }
//        print(head);
//        return head;
        
    }
	
	public static void print(ListNode nodes) {
		while(nodes != null) {
			System.out.print(nodes.val + " -> ");
			nodes = nodes.next;
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(3);
		ListNode list3 = new ListNode(4);
		list3.next = null;
		
		lN.next = list2;
		list2.next = list3;
		
		rotateRight(lN, 2);
	}

}
