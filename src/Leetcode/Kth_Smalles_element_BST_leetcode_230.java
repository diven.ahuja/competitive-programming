package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class Kth_Smalles_element_BST_leetcode_230 {
	
	List<Integer> values = new ArrayList<>();
	int target = 0;

	public void helper(TreeNode root) {
		if (root == null)
			return;

		if (values.size() > (target - 1)) {
			return;
		}

		helper(root.left);
		values.add(root.val);
		helper(root.right);
	}

	public int kthSmallest(TreeNode root, int k) {
		this.target = k;
		helper(root);
		return values.get(k - 1);
	}

	// below is the recursion version of preorder traversal
	// the PROBLEM with this approach is that
	// when we get the list we have to SORT the list and then get the kth element
	// this increase complexity.
	// therefor the above approach is better as we also get to learn that
	// inorder traversal gives us elements ascending order
	// how? You Dry - Run and find out ;)

//     List<Integer> values = new ArrayList<>();

//     public List<Integer> helper(TreeNode root){
//         if(root == null)
//             return values;

//         values.add(root.val);
//         helper(root.left);
//         helper(root.right);

//         return values;
//     }

//     public int kthSmallest(TreeNode root, int k) {
//         helper(root);

//         Collections.sort(values);

//         for(int i=0; i<values.size(); i++)
//             if(i == k-1) return values.get(i);
//         return 0;
//     }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
