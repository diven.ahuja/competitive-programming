package Leetcode;

public class Longest_Palindromic_Substring_Leetcode_5 {
	public static int expandFromCenter(String s, int i, int j) {

		while (i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)) {
			i--;
			j++;
		}

		return j - i - 1;

	}

    public static String longestPalindrome(String s) {
     
        int start = 0, end = 0;
        
        for(int i=0; i <s.length(); i++){
            
            int len1 = expandFromCenter(s, i , i+1);
            int len2 = expandFromCenter(s, i , i);
        
            int len = Math.max(len1, len2);     

            if((end-start) < len){
                start = i - (len)/2;
                end = i + len/2;
            }
            System.out.println(s.substring(start, end+1));
        }
        
       return s.substring(start, end+1);
        
    }        
	
        
	       // DP solution.. using the gap method in 2d array
//	       boolean dp[][]= new boolean[s.length()][s.length()];
//	        String result = "";
	        
	        //this for loop maintains the gap and increase it till s.length()-1
//	       for(int g=0; g<s.length(); g++){
//	           
//	           for(int i=0, j=g; j<dp.length; i++, j++){
//	               
//	               if(g==0){
//	                   dp[i][j] = true;
//	               }else if(g==1){
//	                   dp[i][j] = s.charAt(i) == s.charAt(j);
//	               }else{
//	                   if(s.charAt(i) == s.charAt(j) && dp[i+1][j-1] == true){
//	                       dp[i][j] = true;
//	                   }else{
//	                       dp[i][j] = false;
//	                   }
//	               }
//
//	               if(dp[i][j]){
//	                    result = s.substring(i,j+1);
//	           }
//	       } 
//	       }
//	    return result;        
	
//        int max = 0;
//        String finalString = "";
//        for(int i=0; i<s.length(); i++){
//            for(int j=i+1; j<s.length(); j++){
//                
//                String sub = s.substring(i, j);
//                if(isPalindrome(sub)){
//                    if(max < sub.length())
//                        finalString = sub;
//                }
//            }
//        } //end of outer most loop
//        return finalString;
//    } //end of method
    
    public static boolean isPalindrome(String sub){
        
        int i=0;
        int j= sub.length()-1;
        
        while(i<j){
            if(sub.charAt(i)!=sub.charAt(j)) //bab
                return false;
            i++;
            j--;
        }
        return true;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println( longestPalindrome("abbba"));
	}

}
