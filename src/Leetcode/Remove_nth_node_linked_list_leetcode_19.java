package Leetcode;

public class Remove_nth_node_linked_list_leetcode_19 {
	public static ListNode removeNthFromEnd(ListNode head, int n) {

		// this approach is optimal O(n)
        //Algo:
        // start with dummy node that is pointing towards head.
        // take fast to its next for n times.
        // take slow one step forward and fast one step forward till fast reach end
        // by doing this we will get our element to delete.
        // just point slow.next = slow.next.next to skip deleted element
        ListNode dummy = new ListNode();
        dummy.next = head;
        
        ListNode slow = dummy;
		ListNode fast = dummy;
        
        for(int i =0; i < n; i++)
            fast = fast.next;
        
        while(fast!=null && fast.next!=null){
            fast = fast.next;
            slow = slow.next;
        }
        
		// while(fast!=null && fast.next !=null) {
		// 	fast = fast.next;
		// 	if(i>=n)
		// 		slow = slow.next;
		// 	i++;
		// }
        
	    slow.next = slow.next.next;
		return dummy.next;
      
		
		
//		  if(head == null)
//	            return head;
//	        
//	        if(head.next == null)
//	            return null;
//	        
//	        //1. reverse the LL
//	        //2. delete the element
//	        //3. reverse
//	        
//	        Stack temp = head;
//	        Stack prev = null;
//	        while(temp !=null){
//	            Stack future = temp.next;
//	            temp.next = prev;
//	            prev = temp;
//	            temp = future;
//	        }
//	        
//	        int i=0;
//	        Stack head = prev;
//	        Stack tmp = null;
//	        while(i<n-1){
//	            tmp = head;
//	            head = head.next;
//	            i++;
//	        }
//	        head = head.next;
//	        if(tmp!=null && tmp.next !=null)
//	            tmp.next = head;
//	        else
//	            prev = head;
//	        
//	        Stack temp1 = prev;
//	        Stack prev1 = null;
//	        while(temp1 !=null){
//	            Stack future = temp1.next;
//	            temp1.next = prev1;
//	            prev1 = temp1;
//	            temp1 = future;
//	        }
//	        
//	        print(prev1);
//	        return prev1;
	        
	        // ALGO: 1. find length
	        //       2. find actual index (from start, index number)
	        //       3. go till that element and then point directly to the after deleted node from head. 
	        //O(n) + O(n)
	        
//	         if(head == null)
//	             return head;
//	        
//	         if(head.next == null)
//	             return null;
//	        
//	    		int len = 0;
//	         //to find length
//	 		Stack temp = head;
//	 		while (temp != null) {
//	 			len++;
//	 			temp = temp.next;
//	 		}
//
//	 		int actualIndex = len - n;
//	         //to handle only two element case
//	         if(actualIndex == 0)
//	             return head.next;
//	        
//	 		Stack curr = head;
//	 		Stack prev = head;
//	 		int i = 0;
//	         // to reach the element that needs to be deleted
//	 		while (i < actualIndex) {
//	 			prev = curr;
//	 			curr = curr.next;
//	 			i++;
//	 		}
//	 		curr = curr.next;
//	        
//	         // removing the connection of prev element to the deleted element
//	         // and joining prev element with the next one.
//	 		prev.next = curr;
//			
//	 		return head;
	      

	}
	 public static void print(ListNode temp)
	    {
	        while(temp != null){
	            System.out.print(temp.val +" -> ");
	            temp =  temp.next;
	        }
	        System.out.println();
	        
	    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(2);
		ListNode list3 = new ListNode(3);
		ListNode list4 = new ListNode(4);
		
		lN.next = list2;
		list2.next = list3;
		list3.next = list4;
		
		removeNthFromEnd(lN, 2);
	}

}
