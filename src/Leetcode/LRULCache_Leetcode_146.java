package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class LRULCache_Leetcode_146 {

	static class LRUCache{
		
		static class Node{
			int key;
			int val;
			Node next;
			Node prev;
			
			public Node(int key, int val) {
				this.key = key;
				this.val = val;
				next = null;
				prev = null;
			}
		}// Node class
		
		
		Map<Integer, Node> keyMap;
		Node dummyHead;
		Node dummyTail;
	    final int capacity; // Capacity of the cache
		
		public Node insertAtFront(int key, int val) {
			Node node = new Node(key, val);
			
			//Changing pointers of first, middle and next element. Since element was added in the middle
			Node temp = dummyHead.next;
			dummyHead.next = node;
			node.prev = dummyHead;
			node.next = temp;
			temp.prev = node;
			
			return node;
		}
		
		
		private void moveToFront(Node node) {
			
			Node next = node.next;
			Node prev = node.prev;
			
//			//adding it to the front
//			Node temp = dummyHead.next;
//			dummyHead.next = node;
//			node.next = temp;
//			node.prev = dummyHead;
//			temp.prev = node;
//			
//			//removing connection from old position
//			Node temp1 = prev;
//			next.prev = prev;
//			temp1.next = next;
			
            node.prev.next = node.next;
            node.next.prev = node.prev;
            Node temp = dummyHead.next;
            dummyHead.next = node;
            node.prev = dummyHead;
            node.next = temp;
            temp.prev = node;
		}
		
		private Node deleteFromLast() {
			 if (dummyTail.prev == dummyHead) {
		            // Safety check: do not delete sentinel nodes!
		            return null;
		    }
			 
			Node temp = dummyTail.prev;
			temp.prev.next = dummyTail;
			dummyTail.prev = temp.prev;
			return temp;
		}
		
		public LRUCache(int capacity) {
			this.capacity = capacity;
			keyMap = new HashMap<>();
			dummyHead = new Node(-1, -1);
			dummyTail = new Node(-1, -1);
			dummyHead.next = dummyTail;
			dummyTail.prev = dummyHead;
		}

		public int get(int key) {
			if(!keyMap.containsKey(key)) {
				return -1;
			}
			
			moveToFront(keyMap.get(key));
			return keyMap.get(key).val;
		}
		
		public void put(int key, int val) {
			if(keyMap.containsKey(key)) {
				Node node = keyMap.get(key);
				node.val = val;
				moveToFront(node);
			}else {
				keyMap.put(key, insertAtFront(key, val));
			}
			
			
			if(keyMap.size() > capacity) {
				Node node = deleteFromLast();
				keyMap.remove(node.key);
			}
		}
		
	}
	
	
}
