package Leetcode;

import java.util.HashMap;

public class TwoSums_2_leetcode_167 {

	public static int[] twoSum(int[] numbers, int target) {
	     
        HashMap<Integer, Integer> hmap = new HashMap<>();
        
        for(int i=0; i<numbers.length; i++){
            
            int curr = numbers[i];
            int x = target - curr;
            
            if(hmap.containsKey(x)){
                return new int[] { hmap.get(x), i+1  };
            }
            
            hmap.put(numbers[i], i+1);
        }
        return null;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(twoSum(new int[ ] {1,2,3,4,5,6,7,8}, 9));
	}

}
