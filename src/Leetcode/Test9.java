package Leetcode;

public class Test9 {
    public static int solve(String A) {
    	
        for(int i =0 ;i<A.length()/2;i++){
        	
        	StringBuilder sb = new StringBuilder(A);
        	String newString = sb.deleteCharAt(i).toString();
            boolean flag = checkPalindrome(newString);
            
            if(flag){
                return 1;
            }
        }

        return 0;

    }

    private static boolean checkPalindrome(String newString){

        boolean isPalindrome = true;
        int i=0, j=newString.length()-1;
        
        if(newString.length()==1) {
        	return isPalindrome=false;
        }
        int z = 0;
        while(z<newString.length()){
            

            if(i>j){
                return isPalindrome;
            }

            if(newString.charAt(i) == newString.charAt(j)){
                i++;
                j--;
            }else{
                isPalindrome = false;
                return isPalindrome;
            }
            z++;
        }
		return isPalindrome;


    }

	public static void main(String[] args) {
		System.out.println(solve("atisdyvszskdzwngfzrgojbcgneihkmszxlkwftliowbybfoaezgeiggxmpmenwqtkutwfakpmwwqznrvwuwsjsnojxjaoczjdfmegsoimlnbslecfjqoxxchdrguljcpmbrpyfscctqbtulfhemygltnqjmxyhwghubvotrcqkwbkmoropasdxxpmzkixwtbgfxdoyqywkiglvqpkpbvsdcimumesnamdiaeedenbclrveokompcypjtcuvlvwfapvtevfzumlsfwvwoyzmmjhdqtiaxgveugvbxbsyzvrtqlgchxaxdomolqaozrmkycgbqoapthwtegwbjkdutylszjiecbygircucsmsasekycubgptknzyqrzrdqseadxklgwskvyklfnhxouimuslernvgmeeawybyeagybdzqlhwslbkclpolmgzzzrkdkismeunqubiupokbbqyymzwapjvlrbalydzkcvrsznwucsinigurnegmewrornrninibmxgrqlioqoxtdyibymmwbgkimssymmjgysxxmgrjkhucbwxlhdqndzyiwycrjvascocpeoaxqtvkzhmmstqghxpbmkmtmccaklgoukqzztmanxhfdejmscmmqglpljjcyqlljyvjazdxgicbyojvhqvotkyixmdnyqxniurwzhbecmznduauyfeteowdtlczalfnjdhlpxngbxymvwzcffiahouqhtzgmuvmhcbrlhjstwmfjyfatgtqvcwaenbbzxuqdnxhvizbvtwaeigjlyyzupurjjhzemmsjvihtityftmoycwwygrftzvbwiiejrlamkeuzmgbprsxvwvmuiqrzhzvhedgzmoeeycyzlsbanvzoqbqjcsajuaybhfghaxnavobsxpkljteyokuzqnkfsbevrfezdoctzygheyignokciebwutllpcwyaqdwwktxsxifwsgejjacjjjcrnwdzcgdyiqwllwzgdefxxfoulhcfbsagcjlazeukpmenbulekrbwhlcrfasiuzhzutdndnxocilurroynxreiboqzhwcdzgvjnmerazrtllgjydtoshvrrbezxqzfchuzwxcpybgkwhyygssdjgurgylxdkimnvcothpdcnjsrkzkagfdwlpnosfjeskeiezveuccyowdzwdltokbenoiocghtczyymjfmalagfmeyoluqgnrkalodoqdodggxdkytubvbqwvomntopqjhiwdawkphsvgqoosebqoxickqhlnxmsjskozgfmdnkegudaehaxbdrlcrqjsgfzffpzsjkfxajpfhscgzpolmyxtpvrybtbztnejsfgxlzusdsguxaootwakwxdqmqcfzhyhhxttblhxtycfkrvqjjadrersizybguddaxqqfsuehwsxfqziigdcylbkmhtpifxvwljbqtmnwrqmeboulcdyehwlyihddckngcmcvevnteovsmsdujerjfihyjiqlziofzklyhajvvzjmxhdizitzdbezkwpmvuayknpeuynvwsrrgtaxokkaznttoaezdjdhowwdxnjsmbezqfjghipyryeduqtxofcmrngpcrnrxbrwqzsqkvtibcmyvcxkyvbokdflzhwlhexsrvlwvgajfbdvryqvafrdyyqyougioujzjrnjftslgirjmciugfiapoutxctlhzsgnphbkavlzvrzpsnvlbeyiyvxtwipstilbbqtnounfhpsdicgsfcnbenwdqqhctjfvlyebfmdaauuvtczfgwlvdmiuifcsxbxvdpnkcxtjrsgdjrfdutxfhjbcsdjvubywmzrnblxodvuwfjyoadtqdiinaebbrudycyhtdyyhgsqltubdpmuqhfjmqypsiuzxgsqtkxuifdviyrbpxhrzxzackhcujjlmpzdnomvogukrdnoowyabxrnmhvufcnkwylndxzfvjyfrwqynetrmzfdkbgcjtjitmuaqurjbvfydsqvbrpoacjesqytxzfukbutbbvwlmrqfhrrshkxscgdgadiwwhivujbdznxyoldebphlwwxzoyssdkqxakubsvlocffsxfudxrflpseoahcuferksjjbzncmpnkepwqbvaqswwhnhxwncxiuieinwizrgbotvqusbkokffqynnxwbfbbjlrmlezdnxwnwhjcflbqohhikgoybrfakjlzglokvruigwotjccftcbktserkvsqpualrezydfxpwrqxenwgiztjexoqjtpdbshmnzuvxrexcphwywnfrwrqsfwsknpeyfcgsmvfslohmeknwcxqbhzjzdftdinpjkimrthhwueaaudfqpjjazusqjylglwnqyizqfdoprumwuqnjufftruonfgrrsyjnvcbafjvvonzhmalxjmsnskswmgkbnohgvjrprhyspxihsopcwhaafdadbhwewfefwpcrdmdcbpcvxwiicbuetwboieuzyulgvzgjjihtifunfwzcxtlytehepkjhagyretgwakxdwuioebxafwrtustldoswegohjexgvoxjetywldsvbdgzfvwieyrmmdmqrprsryoriyqfmhowyoxhebqzkvsfwkwubjmehstmiueukatrtmdpnryhzqktxmqpjzluwktljpqgphzyctzfostgscgvlhjbtovcblcvbifvpbsljpgzqhquqkvzeaggmztwbseljrqcjbaemvsvqdnudupedsywiwwovswznrknrixezenvkdkcblfaxqxnteyelnfwpdpyosfyxwexseqxirmccrnozyzerauxiedtcarqkrecofuhxxsekusdrxefkapyktludecpisqenfodzflmhqanyowfiozarucrwmpnzvucfahjtaabrinpzsndnfyfagxetxqucnurafivcfqbupdtjioqudfnmvtnegraflgtbdrbnzzlebepnammdztywamvappveeokxntvlrqrxdicotjhwhqvhhaehsftsvcdgkfadhysjoprjhcmugxaudogjgslolznerlwgwvecvhkpukeuwqusfbdjtwhifgkndygoeszjneyazfvoqgsotjvzgzwghazxuqkzlebxgxxufyeutuwflvgckpptgziqdclsuzgwxfxehvnaviljcysqiewztspqjhnfgcdwyvmxibyltbgumcmhvavellczblagsysukdouqnznxsiicdaeubthlxznmxbkzdxennhzijqgsjigdefpcdhwuruurngrtwhmiewmvythixspqknblovztjpsruivmbcsotzbbizmmtbfwolmkxordkhpivgrmhflsclmyyaifbzqgxawvncyhiumbdgwwvbqpizdbxthqrcofnqymzpqnqpsuwfuifubnofonyhhguyqgiyqexpgefrgcoufomtmzgxqivspypjepdieehonbnasttsjimcfupxmyaadhpfavpahkegvhxooyxmyrymvvkfwgthgrjyppcblmnlalcfvqigpdlxfavssuqdcjbyrgbngxrwcscfpvnbxzzhlvlgpvjpxgpstxfspjqdivulkuthudxyhmbqbkhlgryepzgssjmpyulzdjfhqoocrdfkmwjjhhpkntblyupxcyxbyzftejwpuxlppaiumnwiklpolxhpdeozuohyapahocsriimqceagdycmjhahdphhtxuvcdxjhdnqfrxqdmismjhhztotpelnzthwnuzvsuvfjzqrlgkqerjatyxpdupvrrlitnpnlbeigxuzdnxvkbaxkcxwbembhmnrjinbpsxgtvajgouuniwqqqpawrqxkynulwexbtnqgccdeebzfjajremducvguwlzbgppwnkylhuibaadzdrkaizxshaqvuqbkfgvqldlxvgcvtkidhigddgietqfkcttusgdfysyojmvmueohfnjsylswdpnxpeszordxguwldspkmbwoqueahjrdscyqwynskcijeaqkeakhltuettnjeflhfsgvnjorpeeqbkspxwhxkrrtbftdimgujhnzcqiuonvreybbjutukmjjnuodpizlrluxuatebwbaqbbwcpeiqqdtsaqrisshrtzperqbfmgcdkiwsujmpwbwjxyqebgsgfurgifihlujwccytggvoyjcgqjairkejptajcfdwszlzcrgvlphmzbdchxlvtnpbjemnklwhkopvwfutigobszelirkynyzjadbiafmgdzzqugvxzprqhlfxnpvopjtcqymekypmcqblzgrcmcitengrtbpjgtklchhatqidvhnrwjbfhbrvomuwlpyqkaqxvassegfczwydginljpgwxybcmfedzjlwcwubdamhtlpqnvpnwwssznjnqbkzzkzvpvyjffwztoyefgaaeyubsblqnjwcjglqahndfknfqxboufolwzsyxufzgfuqutxjloychsrsgpyoicsfxhkfcenvcwxoeabduwefkyxpaoewtkewfgoxwrudxgjuhtbvhfwrjgbcvfgpwaixvepzstlqyxsflrpbwtbfkjxajdigwohizkmppvubkbplzmiczticurkrhqujgvjdejmfzvjnpeetpnbqshxojeglqogegporfnqaofhpscefraruefgomvohiwfksqxlyfnponcfcxjpyhmaufjsqjfcwfhjpssxyjsyftaghapbdeydblxynpoodhueknvgajmzvllcilvbyxmkzvlwauhhhbtxctjkctkxknkncfvyvhucevvclmxwyruogsustegkumxozyqgplugjzvvvvrhrlkwujreescbtcmmfracqvjolhtitahfbdkqqoaxzinjoenlpxlcaerduyeqybnszhxklhpattfzomyjawmnzerdlwkvqkoiczlqeqxtsdowktyhhjxlovvdtenopbkmyrytdvgppdbuuzwpgqkeqlpbadomnclvnkqjzgiwfqsucfvavtywhiowtkmiatchuwynjlohqndsjrjsyxlamjkhnczdgsbekbdizjxoldvtyqcprqzegcpcehluweoppxwzdtrujuzxqibncejhfsysjskuzjuiesjzohqmytrjbsuujrxiwkvrhxmtideeasbiiggfqwngtmvdchwhtwgygphopqusfbkptgvokbdycwvhxdukdysluilayqyncaiexfsezsigmyokumljnqknqbacehmgsgcmfhauatgytmfpfgwkvicphperipyybjgbblzeiwfbivumiybkajeceudehyqkpieiyrplvvwdsqprqifsgiexwtwytqkkjxqyneymjqdnahdvyowsqnagjiwmrsbdusvmrilwzktqkbirkakbhnsazwzgcamwpdarcgtgmpoxtmurfuwxpewfvtvdgwuwhkjxqoohwqudeeyyofwmvqhomqttqkteewghvqqksscpnpzahhoyitqfsgzukepnehbprsftrbieahvpzxlhapspwlzxuyzsiyhfmcedznxyoaoeutpmayzpbxmxboqgfyvorractppuqpqybayhffsvxtogbbdbvdnddzoamqqzwixwfottbqrohpbvivhqnxretmpuygzmdiivqfobnyyulmommwxlyipfzhyfcicqsgszvmsjrccisanykwkmcuezmyirqabodsnolirtoqdqzmhntlzeciqmivgiponrtarfkjrjpwlfekoikvhsvmhlyortaieptagqbmxzmxwyqsmcrykyvpoitojwpeutvihwolfysozuinqkeywlifrdtlcqlcwfytbauuqvqbvuozkbgjnnxhdfnvubwflpbdehepwcstoqyvlvwscxdxltrrkzbtfquosooqlikkalyfdfizbjhzapwdtljlsdolvlvpglnknjkmojriwqywuxrlzszomghwkxbotfejuieyxjotopakhkcbkqnqfdjffqhlyywzanepgsrymvnwtamvkvhxpdwkydozkckwtretxhtnwpskpwutksyxiwlumwfgvwqwbtwqczvmwzgpmrazahtcxtmpkiaixmapagsvqvpafytlrqfeejtzldbbomjptvvgegiminmbzrqocgkirtwgjfxliqcjtywxuzykwtwlshuumepliafrzuldallykdjkhinlwkgysizrhzwtllrgwnlbghayiqpdvhqanyhkiuwmdnymiakryzixkjeyptjwqnqvbivxjhefhjwoeciefxfdunucguhfjljycdhvlnfcpmldvlylcrwcyyfhhyqyemxajlvwwpdsvxzrhwxqnsmtqhbrxozynnnrcegasrhzrjsjypijcomwynjjqqgvbcgmvzrqmgucsjsnpffkpdailbjcoxtneuilzwzoworefsjoxzcityqburfgivlwhqojpeyuezlzqaffeyzlxuvplevmgqpskrfussccncuyteskonshzzkmwuyeemynthjecpikmkgveoqnpejufqioscgofzbbdsbzpcvmlzozeofbwsmcrvsxzudzdwqukcihbkmmszxayphpvctksgusvvafodzsejonaodlwrjxqeqbynutsimptsqybxfokwgynxttnrfroxeousirvwjhvwrmmrpoiojiaxnuzyetkocgaaqdfbifbesdgxxtuhtwjhjekxecrqzxmydpamhfywiotjestkdgoppxzhsepbipqxtpxdxruflwfnvqqdryodunjpieeqbaemynlyttoyltxvwsrclgiuxyispudtcwkhbpncljuknypiuusxroqppaifaekcsumpftpzisgccrafirnvkdqicphcnqmsmssdtwbuygrvtqoqokmgopqzueosjywcafayukescttmfzjmydvjndfxrlymdqhuzogfaeqcrziakasbznoiycrmkwinturoqfethjdntipkkortugbygmslhlpsewonpkcvipwvhmzkglynwgeojhtjsgoorwoppvksqkdhslxbzkmyigdtulmrbvacwxknfipepqynqdnoqpfcaazyuwkkjwwiunrkvgeudlkyfwdkptxouvtwnvcdkylhzectmzmczikqvenwywzonntaabjhuxnjggvbvehybhicvitxvydwshfgjolgjhivunrrodtvblmdzsuowigrxnwdjzineounbrsetvdcruusjgqxrvyyjjohleifsoyaakgavmvrqbtkmgbbnffqhvnuzdlzhzhrbrwedghhxcnrayxxjnwuxztpoyfcwpjopkcfucaagascopbyojveeurhxfwxkkoaznagesjjknjytwqvidzmzfmymjjtvcgfpoiqmxuocgiyfnvfjjobqwmmvmnnoiqsgbzejmrusrstrtjfabgbjijwezwylyhtzuebjlkstjjwawteircdnlhcorisrqzhgxlxpdpknqnkbaqzgdaobbdnpsygbmermmefvpjnpidqidfoffgvqigglyybdcsimqrtyufnabtlzzlknmfzfiscbdusuwzripnfbfxzqzcxzsioyergmobyvjnkxiqqxmpaawjlogipdvsjvfgltgfjotffinoihejbrgmezihskgcyrlnwtnvspeckhvbubzvdxvenzagzgcwscsafnsvzsujllglpqlrxxllneytrjkiojmclbjzxicelscnngvsjywshroehrsbutvxbuuqygumaobfabmelvmfqbceoakzxugpelvgixabhbzloaklbmrwdvybkcrtzwrdzksyobsjzcuyzpgemdlksoqrqninmgdzkuparoqhzzsggamjddczqumvqfzvthimjiyupnixebohrfkdjzymityeawtyjynsruwtcvnwgpcaxlmajaypdshmnlciwoonyrtlppzekplgsxoihmwkyqfxyzkkutmmjjhdyrjzucsxqxkkhjicwrxzlltmohhztlmhapuudtnfkjuemrkhwgkuenvwwdgurgijkhczjgiyjzltkiiiowdqexyrtctckklyldbwniaftgjheallxxjxrakdccrxaijsmxujaqwioywcfcukrqwcufdwkighcwjdgqohcywxfpqdapvyskvqnecoglijehxsqbflmznbeifqmwvdjdfyukjshftlsamcuzivemraoanqexuvjqosrynfotrcxrpspkfllnuzylcrtcyxetldeftldenysdgpcfcltwsupfdzrgfdebqdwxjmleqrkhpuwshirinvevjdfadpkbzkmyxeiieskrpgsgwvmixjafijyfgwqpwjjriubjsjcqxstsdyfrgkydrbrwigriuybzabylivwrnbuuchpdxrkropjtourshbzpglhaeibkspkvaauymkpongfqgsquwecbawkbywdeaaoldofwovcfapsswoljjipoxioxcwlnrmgbyrvbcresfueskjfmfoxmlmlxgoywvedykpvtgkgnnfkuwhyheuaweiabvetkymsenpxugyzeywedmmcogottoamlhdxfeccmfcvinwoanqvwyxocwqutmhvvjfaofpepdyzhmivxrufnlzokfjwbggveganvdwvvabdhzmhpwskocwzhtltpzfequdcbzrcjgkgkzxobwuigfmgnxsegbaqfvcjprcrwenmaorjitsgdvnitjjupnfetwyjmxsxeawoiivsqfpmrkbsixtbgfwpzwlxkvvzzcdszxrsvhjiespgotiamfpgbtdsvdfaeypwdxdwewssvbmtphypwsunruhbcfzrcuxpxwqkuirfookdsjersnnbljrwarvvakvqcaavamefitflkhmgjgkjacaziyjnyivjuwmvhqseahkrifgfcargbhspukkunrxeyuudzovwhcbggbclnpthpupifnelclqaezidwudurtorgoppstlcwxojayhqzjyjfmzyjtrcoscbweygqebqacbnbjmopopwaztdzfrxgnzbarfnignhifcjkegmomicrwigtnylrhfpstmdnhafxbqmqnntrhdonknvntqvftpigkhiwxklbapqwjrspkxfpeqjbojcvjkjwjnntkhihwlkqguukcdihepmhoudychlqljtojahopfqlbjumxiazxxjfrllnqxtdsnfcgnlafzeeouyaauhmxjtdvtryhfeezhskggkkiyvkojdzdtivijpveecyquzqezfogygnysypehqyuxyvgvmnzqmfipvhjtzjprutyhkjkmmwhgcfvjbbycmomvtlcicoayozumqiafcjcqjtbtrspkrqzsxxblnzzltgtxsihzxttzmpakvhfqmhheiiksvtlmaupbsrnzcjagcybrplaegwwrznmbbzjoihdopypdwwbocnkeoifpefvqbdpxgnlfcctsnufdxfxlfwatwypmjtesfassfnevugoydmunccqfdmwvzeyznzhpuewcbeebvsuecqoexfofzosodjhwtzujxwnsbrhelowgnghbkarhwgeussmtvqjyhjrscxzlhcahdjepsjxhbvzepfufutwxwhangqlvzeptmgvicewgtcgvfefvuupstioxcxrneajwbyjvutugrhdynrarpablqlquvdmkjwapmlmwtmwvubhtleydtfktfssteqxtaawcdvnvzdeoyfkoqfknfauxdhtqczwryqnuzuizpnynmjmpvararwzlxshnsktijcabvahqtjwtblditrfebtjvzovrdouiiquvupeogesyccpvhmfjomiaoqdepsxjdevrtzpfjiuqspmkgybygswkwwsuxcqmvkgjiejwrrwiuxyfhixnszzvqdeitsnqxhxqjixofa"));
	}

}
