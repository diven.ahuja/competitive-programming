package Leetcode;

import java.util.regex.Pattern;

public class DecodeString_Leetcode_microsoft_394 {

	public static String decodeString(String s) {

		char[] charArray = s.toCharArray();

		StringBuffer sub = new StringBuffer();
		int startIndex = 0;
		int endIndex = 0;
		int num = 0;

		for (int k = 0; k < s.length(); k++) {

			if (!s.contains("["))
				return s;

			startIndex = 0;
			endIndex = 0;
			num = 0;

			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '[') {
					startIndex = i;
					num = Integer.parseInt(s.charAt((startIndex - 1)) + "");
				}
			}

			if (startIndex > 1) {
				boolean flag = true;
				if (Pattern.matches("[0-9]", s.charAt(startIndex - 2) + ""))
					num = Integer.parseInt(s.charAt((startIndex - 2)) + "" + s.charAt((startIndex - 1)) + "");
				else
					flag = false;
				if (flag && startIndex - 3 >= 0 && Pattern.matches("[0-9]", s.charAt(startIndex - 3) + ""))
					num = Integer.parseInt(s.charAt((startIndex - 3)) + "" + s.charAt((startIndex - 2)) + ""
							+ s.charAt((startIndex - 1)) + "");
			}

			for (int j = startIndex; j < s.length(); j++) {
				if (s.charAt(j) == ']') {
					endIndex = j;
					break;
				}
			}

			if (startIndex == 0 && num == 0 && endIndex == 0) {
				return s;
			}

			s = addingString(s, s.substring(startIndex, endIndex + 1), num);

		}
		return s;
	}

	public static String addingString(String s, String sub, int num) {
		String a = "";
		String old = new String(sub);
		sub = sub.substring(1, sub.length() - 1);
		for (int i = 0; i < num; i++) {
			a += sub;
		}

		s = s.replace(num + old, a);
		return s;
	}

	public static void main(String[] args) {
		System.out.println(decodeString("3[z]2[2[y]pq4[2[jk]e1[f]]]ef"
));
	}

}
