package Leetcode;

public class Sign_of_the_Product_leetcode_1822_Microsoft {

	public static int insertionSort(int nums[]) {
		int i = 0, count =0;
		while (i < nums.length) {
			if (nums[i] == 0)
				return 0;
			else if(nums[i] < 0)
				count++;
			i++;
		}

		if (count % 2 == 0 || count == 0)
			return 1;
		return -1;

	}

	public static void main(String[] args) {
		int[] arr1 = {1,5,0,2,-3 };
		System.out.println(insertionSort(arr1));
	}

}
