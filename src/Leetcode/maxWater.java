package Leetcode;
import java.util.Arrays;

public class maxWater {

	//
//  Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
//Input: arr[] = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
//      Output: 6
//
//      Explanation:
//Trap "1 unit" between first 1 and 2, "4 units" between
//      first 2 and 3 and "1 unit" between second last 1 and last 2

	// Function to return the maximum
	// water that can be stored
//	    public static int maxWater(int[] arr, int n)
//	    {
//
//	        // To store the maximum water
//	        // that can be stored
//	        int res = 0;
//	        
////	        for(int i =0; i <arr.length; i++) {  // to go through each bar
////	        	
////	        	if((i+1) <arr.length && arr[i+1] < arr[i]) {
////	        		res += 1;
////	        	}
////	        	
////	        }
//	        int[] lmax = new int[arr.length];
//	        int[] rmax = new int[arr.length];
//	        
//	        lmax = Arrays.copyOf(arr, arr.length);
//	        
//	        Arrays.sort(lmax);
//	        
//	        int index = 0;
//	        for(int i=arr.length-1; i>=0; i--) {
//	        	rmax[index] = lmax[i];
//	        	index++;
//	        }
//	        
//	        for(int j=0; j<arr.length;j++) {
//	        	res+=Math.min(rmax[j], lmax[j]);
//	        }
//	        
//	        System.out.println(lmax);
//	        System.out.println(rmax);
//	        
//	        return res;
//	    }
//
//	    // Driver code

	public static int trap(int[] height) {

//	        int res=0;
//	        
//	        for(int i=0 ; i<height.length; i++){
//	        
//	        	int lmax = height[i];
//	        	int rmax = height[i];
//	            
//	            //finding lmax
//	            for(int j = 0; j<i; j++){
//	                lmax = Math.max(lmax, height[j]);
//	            }
//	            
//	            //finding rmax
//	            for(int k = i+1; k<height.length; k++){
//	                 rmax = Math.max(rmax, height[k]);;
//	            }
//	            
//	            
//	            res += Math.min(lmax,rmax) - height[i];
//	        } //end of first for
//	        
//	        return res;  

//		int res = 0;
//		int lmax[] = new int[height.length];
//		int rmax[] = new int[height.length];
//		int n = height.length;
		
//		lmax[0] = height[0];
//		rmax[n - i - 1] = height[n - i - 1];
		
//		for (int i = 1; i < n; i++) {
//
//				lmax[i] = Math.max(lmax[i - 1], height[i]);
//				int index = n-i;
//				rmax[index-1] = Math.max(rmax[index], height[index - 1]);
//			}
//		}
//
//		for (int i = 0; i < n; i++) {
//			res += Math.min(lmax[i], rmax[i]) - height[i];
//		}
//		return res;

		int i=0, j= height.length, water = 0;
		while(i<j) {
			
			int maxLeft = height[i];
			int maxRight = height[j];
			
			if(height[i]<=height[j]) {
				maxLeft = Math.max(maxLeft, height[i]);
				water += maxLeft - height[i];
				i++;
			}else {
				maxRight = Math.max(maxRight, height[j]);
				water += maxRight - height[j];
				j--;
			}
			
		}
		return water;
		
		
		
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = { 0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1 };
		int n = arr.length;

		System.out.print(trap(arr));

	}

}
