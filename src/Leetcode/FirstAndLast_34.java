package Leetcode;

public class FirstAndLast_34 {
	
	//First Approach Brute Force
	//Second Approach Binary Search
	public static int[] searchRange(int[] nums, int target) {

		int start = 0;
		int end = nums.length-1;
		int mid = 0;

		int index[] = new int[] { -1, -1 };
		
		if(nums != null && nums.length == 1 && nums[0] != target) {
			return index;
		}else if(nums != null && nums.length == 1 && nums[0] == target) {
			index[0] = 0;
			index[1] = 0;
			return index;
		}
		
		if (nums.length > 1) {
			while (end >= start) {
				mid = (start + end) / 2;

				if (nums[mid] == target) {

					if ((mid - 1) >= 0 && nums[mid - 1] == target) {
						end = mid - 1;
					} else {
						index[0] = mid;
						end = -1;
					}

				}

				if (nums[mid] < target) {
					start = mid + 1;
				} else {
					end = mid - 1;
				}

			}

			start = 0;
			end = nums.length-1;
			mid = 0;

			while (end >= start) {
				mid = (start + end) / 2;

				if (nums[mid] == target) {

					if ((mid + 1) < nums.length && nums[mid + 1] == target) {
						start = mid + 1;
					} else {
						index[1] = mid;
						return index;
					}

				}

				if (nums[mid] > target) {
					end = mid - 1;
				} else {
					start = mid + 1;
				}

			}
		}

		return index;

//        int index[] = new int[2];
//        int count = 0;
//        
//            
//        for(int i = 0 ; i<nums.length ; i++ ){
//                if(nums[i] == target){
//                    if(count >= 2){
//                        count =  count - 1;
//                    }
//                    index[count] = i;
//                    count++;
//                }
//            }
//        
//        nums = null;
//        if(count == 1){
//            index[1] = index[0];
//            return index;
//        }
//        
//        if(count == 0){
//            index[0] = -1;
//            index[1] = -1;
//        }
//         return index;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(searchRange(new int[] {1,1}, 1));
	}

}
