package Leetcode;

public class SearchInsert_Leetcode_35 {

	public static int searchInsert(int[] nums, int target) {

		  int start = 0;
	        int end = nums.length;
	        int mid = 0;
	        
	        while (start <= end && mid < nums.length) {
				mid = (start + end) / 2;

	            
	            if(mid >= nums.length)
					return end; //over flow end edge case.. 
	            //it means the end element is also smaller than the target so the target if inserted will come at the last position obviously.
	            
				if (nums[mid] == target) {
					return mid;
				}
				if (nums[mid] > target) {
					end = mid - 1;
				} else if (nums[mid] < target) {
					start = mid + 1;
				}
			}
	        
	        return start; //over flow start edge case...
	        //means the start element is also bigger than the target so the target if inserted will come at the start position obviously.
	    }

	public static void main(String[] args) {
		System.out.println(searchInsert(new int[] {1,3,5,6}, 7));
	}

}
