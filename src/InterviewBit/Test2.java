package InterviewBit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> arr = new ArrayList<Integer>();
		arr.add(1);
		arr.add(3);
		arr.add(2);
		arr.add(6);
		arr.add(1);
		arr.add(2);
		int k=3;	
		int output=0;
		for (int i=0; i<arr.size();i++) {
			for(int j=i+1;j<arr.size();j++) {
				int sum = arr.get(i)+arr.get(j);
				if(sum%k==0) {
					output++;
				}
			}
		}
		System.out.println(output);
		
	}

}
