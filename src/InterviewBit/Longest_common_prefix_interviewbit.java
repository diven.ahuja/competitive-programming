package InterviewBit;

import java.util.ArrayList;
import java.util.Collections;

public class Longest_common_prefix_interviewbit {
	public static String longestCommonPrefix(ArrayList<String> A) {

		String res = "";
		char letter = '*';
		boolean flag = true;
		Collections.sort(A);

		for (int j = 0; j < A.get(0).length(); j++) {
			if(j >= A.get(0).length())
				break;

			letter = A.get(0).charAt(j);
			for (int i = 1; i < A.size(); i++) {
				
				if(j >= A.get(i).length())
					break;
				
				if ((letter+"").equals(A.get(i).charAt(j)+"")) {
					letter = A.get(i).charAt(j);
				}else {
					flag = false;
				}
			}
			if (flag)
				res += letter + "";

		}

		return res;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> a = new ArrayList<String>();
		a.add("abcd");
		a.add("abde");
		a.add("abcf");
		System.out.println(longestCommonPrefix(a));
	}

}
