package InterviewBit;

import java.util.ArrayList;
import java.util.Arrays;

public class MergeTwoSortedList_II_interviewbit_Summer_dsa {

	public static void merge(ArrayList<Integer> a, ArrayList<Integer> b) {

		int i=0, j=0;
		int n = a.size();
		int m = b.size();

		while( i < n && j < m ){
			if(a.get(i) < b.get(j)){
				i++;
			}else if(b.get(j) < a.get(i)){
				a.add(i, b.get(j));
				i++;
				j++;
			}else if(b.get(j) == a.get(i)) {
				a.add(i, b.get(j));
				i++;
				j++;
			}
			
		}
		if(i == n) {
			for(int z =j; z<m ; z++)
				{
				if(i <a.size() && b.get(z) < a.get(i))
					a.add(i++, b.get(z));
				else {
					a.add(i, b.get(z));
					i = i+1;
				}
				}
		}else {
			for(int z = i; z<n ; z++)
			{
				if(j <a.size() && b.get(z) < a.get(j))
					a.add(j++, b.get(z));
				else
					a.add(++j, b.get(z));
			}
		}
		
		
		
		
		System.out.println(a);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> b = new ArrayList<Integer>();
		
//		b.add(-5);
//		b.add(5);
//		b.add(8);
		b.add(2);
		ArrayList<Integer> a = new ArrayList<Integer>();
//		a.add(6);
//		a.add(9);
//		a.add(-5);
		a.add(-4);
		a.add(-3);
		a.add(0);
		
		merge(a, b);
	}

}
