package InterviewBit;

import java.util.Stack;

public class Remove_redundant_braces_interviewbit {

	public static int braces(String A) {

		Stack<Character> s = new Stack<>();

		for (int i = 0; i < A.length(); i++) {

			if (A.charAt(i) != ')') {
				s.push(A.charAt(i)); // a //(
			} else {

				if (s.peek() == '(') //
					return 1;

				while (s.peek() != '(') {
					s.pop(); // a
				}

				s.pop();
			}
		}

		return 0;
	}

	public static void main(String[] args) {
		braces("(a)");
	}

}
