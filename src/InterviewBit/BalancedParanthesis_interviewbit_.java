package InterviewBit;

import java.util.Arrays;
import java.util.Stack;

public class BalancedParanthesis_interviewbit_ {

	public static boolean solve(String A) {

		Stack<Character> square = new Stack<>();
		Stack<Character> brace = new Stack<>();
		Stack<Character> round = new Stack<>();
		boolean flag = false;
		
		for (int i = 0; i < A.length(); i++) {
			flag = false;
			
			if (A.charAt(i) == '(') {
				round.push(A.charAt(i));
			} else if (A.charAt(i) == ')' && round.size()>0) {
				round.pop();
				flag = true;
			}else if (A.charAt(i) == '{') {
				brace.push(A.charAt(i));
			} else if (A.charAt(i) == '}' && brace.size()>0) {
				flag = true;
				brace.pop();
			}else if (A.charAt(i) == '[') {
				square.push(A.charAt(i));
			} else if (A.charAt(i) == ']' && square.size()>0) {
				flag = true;
				square.pop();
			}
		}

		if (round.size() == 0 && !flag)
			return false;
		else if (round.size()>0)
			return false;

		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solve("){}]"));
	}

}
