package InterviewBit;

import java.util.regex.Pattern;

public class Vowel_Consont_substrings_interview_bit {

	public static int solve(String A) {

        int count = 0;
        String regex  ="^[AEIOUaeiou]$";
        for(int i=0; i<A.length(); i++){
            for(int j=i+1; j<A.length(); j++){
                String a = A.substring(i,j);

                if(( Pattern.matches(regex, a.charAt(0)+"") && !Pattern.matches(regex, a.charAt(a.length()-1)+"") ) 
                        || (!Pattern.matches(regex, a.charAt(0)+"") && Pattern.matches(regex, a.charAt(a.length()-1)+"")))
                    count++;
            }
        }
        return count;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solve("inttnikjmjbemrberk"));
	}

}
