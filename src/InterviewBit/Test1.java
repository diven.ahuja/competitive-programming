package InterviewBit;
/*StringSearch class*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class Test1 {
	public static void main(String[] args) throws Exception {
		String inputWord = "the";
		FileReader file = new FileReader("D:/sample.txt");
		BufferedReader br = new BufferedReader(file);
		String line;
		String listOfWords[] = new String[1000];
		ArrayList<String> listOfWords1 = new ArrayList<String>();

		while ((line = br.readLine()) != null ){
			listOfWords = line.split(" ");
			Collections.addAll(listOfWords1, listOfWords);
		}
		
		
		Search search1 = new Search(inputWord, listOfWords1, 0, 250);
		Search search2 = new Search(inputWord, listOfWords1, 250, 500);
		Search search3 = new Search(inputWord, listOfWords1, 500, 750);
		Search search4 = new Search(inputWord, listOfWords1, 750, 1000);
		
		Thread t1 = new Thread(search1);
		Thread t2 = new Thread(search2);
		Thread t3 = new Thread(search3);
		Thread t4 = new Thread(search4);
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		// wait for threads to finish
		t1.join();
		t2.join();
		t3.join();
		t4.join();
		
		file.close();
	}
}

class Search implements Runnable {
	
	String inputWord;
	ArrayList<String> wordList;
	int start,end;
	
	Search(String inputWord, ArrayList<String> wordList, int start, int end) {
		this.inputWord = inputWord;
		this.wordList = wordList;
		this.start = start;
		this.end = end;
	}

	public synchronized void run() {

		try {
//			ThreadContext.push("thread", "");
//			if(!ThreadContext.get("thread").equals("done")) {
//				for(int i=start; i<end;i++) {
//					String word = wordList.get(i);
//					if(word.equals(inputWord)) {
//						ThreadContext.push("thread", "done");
//						System.out.println("Word matched " + inputWord + " against word " + word + ". Exiting thread.");
//						break;
//					}
//				}
//			}
		} catch (Exception e) {
			System.out.println("Caught Exception!" +e);
		}
	}
}