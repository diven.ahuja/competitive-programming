package InterviewBit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class WaveArray_leetcode_and_interviewbit {
	public static void wave(int[] nums) {

		 Arrays.sort(nums);
	        int size = nums.length;
	        
			if (size % 2 == 0) {
	            size -= 1;
	        }
	        
			for (int i = 1; i < size; i+=2) {
	                int temp = nums[i];
				    nums[i] = nums[i+1];	
	                nums[i+1] = temp;
			}
		
//		Collections.sort(A);
//
//		if (A.size() % 2 == 0) {
//			for (int i = 1; i < A.size()-1; i+=2) {
//				int temp = A.get(i);   				// 2
//				A.add(i, A.get(i+1));	
//				A.remove(i+1);						// 3
//				A.add(i+1, temp);	
//				A.remove(i+2); 						// 2
//			}
//		}else {
//			for (int i = 1; i < A.size(); i+=2) {
//				int temp = A.get(i);   				// 2
//				A.add(i, A.get(i+1));				// 3
//				A.remove(i+1);
//				A.add(i+1, temp);					// 2
//				A.remove(i+2);
//			}
//		}
//		return A;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> a = new ArrayList<Integer>();
		a.add(6);
		a.add(5);
		a.add(4);
		a.add(3);
		a.add(2);
		a.add(1);
		wave(new int[] {6,5,4,3,2,1});
	}

}
