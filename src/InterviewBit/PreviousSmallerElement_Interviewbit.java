package InterviewBit;

import java.util.ArrayList;
import java.util.Stack;

public class PreviousSmallerElement_Interviewbit {
	
	//implementation done for NEXT SMALLER ELEMENT
	// do necessary changes for PREV SMALLER ELEMENT
	public static ArrayList<Integer> prevSmaller(ArrayList<Integer> A) {
		
		Stack<Integer> s = new Stack<>();
		ArrayList<Integer> res = new ArrayList<Integer>(); 
		
		for(int i=A.size()-1; i>=0; i--) {
			if(s.empty()) {
				s.push(A.get(i));
				res.add(-1);
				continue;
			}
			
			while(!s.empty() && s.peek() >= A.get(i) ) {
				s.pop();
			}
			
			if(!s.empty())
				res.add(s.peek());
			else
				res.add(-1);
			
			s.push(A.get(i));
		}
		
		ArrayList<Integer> result = new ArrayList<Integer>();
		for(int i=res.size()-1; i>=0; i--) {
			result.add(res.get(i));
		}
		
		return result;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
