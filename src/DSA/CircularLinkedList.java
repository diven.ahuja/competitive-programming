package DSA;

class LinkListNode{
	
	int data;
	LinkListNode next;
	
	LinkListNode(int data){
		this.data = data;
		this.next = null;
	}
	
}

public class CircularLinkedList {

	
	public static LinkListNode insertNode(LinkListNode tail, int element, int data){
		
		//empty List
		if(tail == null) {
			LinkListNode newNode = new LinkListNode(data);
			tail = newNode;
			newNode.next = newNode;
		}else {
			//non empty list
			//assuming element is present
			
			LinkListNode curr = tail;
			
			//searching the element
			while(curr.data != element) {
				curr = curr.next;
			}
			//element found
			LinkListNode temp= new LinkListNode(data);
			temp.next = curr.next;
			curr.next = temp;
		}
		
		return tail;
	}//insertion
	
	public static LinkListNode deleteNode(LinkListNode tail, int value) {
		
		if(tail == null) {
			return null;
		}else {
			//assuming value present in list
			LinkListNode prev = tail;
			LinkListNode curr = prev.next;
			
			while(curr.data != value) {
				prev = curr;
				curr = curr.next;
			}
			
			prev.next = curr.next;

			//1 node link list
			if(curr == prev) {
				tail = null;
			}
			
			//>=2 node link list
			else if(tail == curr) {
				tail = curr.next;
			}
			curr.next = null;
		}
		return tail;
	}
	
	
	public static void print(LinkListNode tail) {
		LinkListNode curr = tail;
		
		if(tail == null) {
			System.out.println("List is empty.");
			return;
		}
		do {
			System.out.print(tail.data +" ");
			tail = tail.next;
		}
		while(tail != curr);
		// do while is used here, 
		// because it is executed once no matter what the
		// condition. So it will be useful to print data
		// when initially there is no element
		// and when we are inserting the first element,
		//  it will solve the problem of element not
		// getting printed.
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		
		LinkListNode tail = null;
		
		//inserting into empty list
		tail = insertNode(tail, 0, 3);
		print(tail);
		
		tail = insertNode(tail, 3, 5);
		print(tail);
		
		tail = insertNode(tail, 5, 7);
		print(tail);
		
		tail = insertNode(tail, 7, 9);
		print(tail);
		
		tail = insertNode(tail, 5, 6);
		print(tail);

		tail = insertNode(tail, 9, 10);
		print(tail);

		tail = deleteNode(tail, 10);
		print(tail);
		
		tail = deleteNode(tail, 5);
		print(tail);
		
		tail = deleteNode(tail, 3);
		print(tail);

		tail = deleteNode(tail, 6);
		print(tail);
		
		tail = deleteNode(tail, 9);
		print(tail);
		
		tail = deleteNode(tail, 7);
		print(tail);
		
		tail = insertNode(tail, 0, 1);
		print(tail);
		
		tail = insertNode(tail, 1, 2);
		print(tail);
		
		tail = insertNode(tail, 2, 3);
		print(tail);
		
		tail = deleteNode(tail, 1);
		print(tail);
	}

}
