package DSA;

public class Stack_Implement_array {
	
	static class Stack {

		public int[] elements;
		private int top;
		private int size;
		
		Stack(int size){
			this.size = size;
			elements = new int[size];
			top = -1;
		}
		
		public int top() {
			if(top < 0) {
				System.out.println("No elements in Stack.");
			}else {
				return elements[top];
			}
			return 0;
		}
		
		public void push(int data) {
			if(top >= size-1) {
				System.out.println("Stack is full.");
			}else {
				top++;
				elements[top] = data;
			}
		}
		
		public void pop() {
			if(top<0) {
				System.out.println("No elements to pop.");
			}else {
				top--;
			}
		}
		
		public boolean isEmpty() {
			if(top < 0) {
				System.out.println("Stack is empty");
				return true;
			}
			return false;
		}
		
	}
	
	public static void main(String[] args) {
		Stack s = new Stack(5);
		s.top();
		s.push(0);
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		s.pop();
		s.pop();
		s.pop();
		s.pop();
		s.pop();
		System.out.println(s.top());
		System.out.println(s.isEmpty());
		s.push(4);
		System.out.println(s.isEmpty());
		
	}
}