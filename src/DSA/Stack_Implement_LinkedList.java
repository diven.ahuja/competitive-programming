package DSA;

class Stack1{
	
	static int size=-1;
	ListNode head = null;
	
	public void push(int data) {
		ListNode temp = new ListNode(data);
		size++;
		
		if (size == 0 && head == null) {
			head = temp;
		} else {
			temp.next = head;
			head = temp;
		}
	}
	
	public void pop() {
		
		if(size<0) {
			head = null;
			System.out.println("Stack1 is empty.");
		}else {
			head = head.next;
			size--;
		}
	}

	public int top() {
		
		if(size<0 && head == null) {
			System.out.println("Stack1 is empty");
		}else {
			return head.data;
		}
		
		return -1;
	}
}


public class Stack_Implement_LinkedList {

	public static void main(String[] args) {

		Stack1 head = new Stack1();
		head.push(0);
		
		head.push(1);
		head.push(2);
		System.out.println("Size: "+Stack1.size);
		head.pop();
		head.pop();
		System.out.println("Size: "+Stack1.size);
		head.pop();
		System.out.println(head.top());
	}

}
