package DSA;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class StableAlgorithm {
	
	static ArrayList<List<String>> tentativeEngagements = new ArrayList<>();
	static ArrayList<String> freeMen = new ArrayList<>();
	static HashMap<String, List<String>> menPreferredRanking = new HashMap<>();
	static HashMap<String, List<String>> womenPreferredRanking = new HashMap<>();
	static int count = 0;
	
	
	public static void main(String[] args) {

		
		menPreferredRanking.put("ryan", Arrays.asList("lizzy", "sarah", "zoey", "daniele"));
		menPreferredRanking.put("josh",  Arrays.asList("sarah", "lizzy", "daniele", "zoey" ));
		menPreferredRanking.put("blake",  Arrays.asList("sarah","daniele", "zoey", "lizzy"));
		menPreferredRanking.put("connor",  Arrays.asList("lizzy", "sarah", "zoey", "daniele"));
		
		
		womenPreferredRanking.put("lizzy",  Arrays.asList("ryan", "blake", "josh", "connor"));
		womenPreferredRanking.put("sarah",  Arrays.asList("ryan", "blake",  "connor", "josh"));
		womenPreferredRanking.put("zoey",  Arrays.asList("connor","josh", "ryan", "blake"));
		womenPreferredRanking.put("daniele",  Arrays.asList("ryan","josh", "connor", "blake"));
		

		
		for(String man : menPreferredRanking.keySet()) {
			freeMen.add(man);
		}
		
		count = freeMen.size();
		
		while(count>0) {
			for(String man : freeMen) {
				System.out.println("count: "+count);
				if(count == 0) {
					break;
				}
				begin_matching(man);
			}
		}
		
		System.out.println(tentativeEngagements);
	
	}//main

	private static void begin_matching(String man) {
		
			List<String> women = menPreferredRanking.get(man);
			
			for(String woman : women) {

				boolean taken = false;
				List<String> taken_couple = null;
				
				
				for(List<String> couple : tentativeEngagements) {
					if(couple.get(1).equals(woman)) {
						taken = true;
						taken_couple = couple;
						break;
					}
				}
				
				
				if(!taken) { // if not in the engagement list then bluntly add the couple temporarily
					System.out.println("Adding a tentative couple "+man+" & "+woman);
					tentativeEngagements.add(Arrays.asList(man,woman));
					count--;
					break;
				}else {
					System.out.println(woman +" is already taken");
					
					int potential_guy = womenPreferredRanking.get(woman).indexOf(man);
					int current_guy  = womenPreferredRanking.get(woman).indexOf(taken_couple.get(0));
					
					if(current_guy < potential_guy) {
						System.out.println(woman +" is satisfied with current arrangement with man : "+taken_couple.get(0));
					}else {
						System.out.println("Making man "+taken_couple.get(0) +" free again.");
						
						System.out.println("Updating guy from "+taken_couple.get(0) + " to potential guy " +man);
						
						tentativeEngagements.remove(taken_couple);
						tentativeEngagements.add(Arrays.asList(man, woman));
						break;
					}
					
				}
				
			}
		
	}
		
		
		
}

