package DSA;

class ListNode{
	int data;
	ListNode next;
	
	public ListNode() {
	}
	
	ListNode(int data){
		this.data = data;
		this.next = null;
	}
	
}

public class LinkList {
	
	public static void addFirst(ListNode nodes, ListNode a) {
		a.next = nodes;
		nodes = a;
	}
	
	public static void addLast(ListNode nodes, ListNode a) {
		
		ListNode temp = nodes;
		while(temp.next != null) {
			temp = temp.next;
		}
		
		temp.next = a;
	}
	
	public static void deleteFirst(ListNode nodes) {
		nodes = nodes.next;
	}
	
	public static void deleteLast(ListNode nodes) {
		
		if(nodes == null) {
			return;
		}

		if(nodes.next == null) {
			nodes = null;
			return;
		}
		
		ListNode secondLast = nodes;
		ListNode last = nodes.next;
		while(last.next != null) { //basically running the loop one time less 
			//than the size of the listNode 
			//so that the secondLast LinkListNode stop at secondLast LinkListNode IYKYK
			last = last.next;
			secondLast = secondLast.next;
		}
		
		secondLast.next = null;
		
	}
	
public static void delete(ListNode nodes, int toDelete) {
		
		if(nodes == null) {
			return;
		}

		if(nodes.next == null) {
			nodes = null;
			return;
		}
		
		ListNode current = nodes;
		ListNode previous = nodes;
		
		while(current != null ) {
			
			if(current.data == toDelete) {
				
			}
			
		}
		
	}
	
	
	
	public static void print_func(ListNode nodes) {
		ListNode temp = nodes;
		while(temp != null) {
			System.out.print(temp.data +" -> ");
			temp = temp.next;
		}
		System.out.print("null");
		System.out.println();
	}
	
	public static void main(String[] args) {
		
		ListNode lN = new ListNode(1);
		ListNode list2 = new ListNode(3);
		ListNode list3 = new ListNode(4);
		list3.next = null;
		
		lN.next = list2;
		list2.next = list3;

		ListNode listN = new ListNode(1);
		ListNode listN2 = new ListNode(2);
		ListNode listN3 = new ListNode(4);
		listN3.next = null;
		
		listN.next = listN2;
		listN2.next = listN3;
		
		ListNode a = new ListNode(8);
		LinkList linkList = new LinkList();
//		linkList.addFirst(lN, a);
		LinkList.addLast(lN, a);
		print_func(lN);
		LinkList.deleteFirst(lN);
		print_func(lN);
		
		deleteLast(lN);
		print_func(lN);
	}

}
