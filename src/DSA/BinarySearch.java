package DSA;

public class BinarySearch {

	public static int binarySearch(int nums[], int num) {
		
		int start = 0;
		int end = nums.length;
		int mid = 0;
		
		while (end>=start) {
			mid = (start + end) / 2;
			
			if (nums[mid] == num) {
				return mid+1;
			}

			if (nums[mid] < num) {
				start = mid + 1;
			}else {
				end = mid - 1;
				
			}
		}
		
		return 0;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(binarySearch(new int[] {5,8,11,24,66,69,71,75,78,79,88,94,99,115}, 99));
	}

}
