package DSA;

class Node{
	
	int data;
	Node next;
	Node prev; 
	
	Node(){
	}
	
	Node(int data){
		this.data = data;
		this.next = null;
		this.prev = null;
	}
}

public class DoubleLinkedList {

	public static Node insertAtHead(Node head, int data) {
		Node newNode = new Node(data);

		if(head == null) {
			head = newNode;
		}else {
			newNode.prev = head.prev; // we can remove this as we are just pointing
			//  newNode.prev to null
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
		return head;
	}

	public static Node insertAtTail(Node head, int data) {
		Node curr = head;
		Node newNode = new Node(data);

		if(head == null) {
			head = newNode;
			return head;
		}
		
		while(curr.next != null) {
			curr = curr.next;
		}
		
		newNode.prev = curr;
		curr.next = newNode;
		
		return head;
	}
	
	public static Node insertAtPosition(Node head, int position, int data) {
		
		Node nodeToInsert = new Node(data);
		Node temp = head;
		
		//if position is 1... insert at start
		if(position == 1 ) {
			head = insertAtHead(head, data);
			return head;
		}
		
		int count = 1;
		while(count != position-1) {
			temp = temp.next;
			count++;
		}// got the position
		
		//if position is last
		if(temp.next == null) {
			head = insertAtTail(head, data);
			return head;
		}
		
		//insert in middle
		nodeToInsert.next = temp.next;
		nodeToInsert.prev = temp;
		temp.next.prev = nodeToInsert;
		temp.next = nodeToInsert;
		
		return head;
		
	}
	
	public static Node deleteAtPosition(Node head, int pos) {
		
		Node curr = head;
		
		if(pos == 1) {
			if(curr.next == null) {
				head = null;
				return head;
			}
			curr.next.prev = curr.prev;
			head = curr.next;
			curr.next = null;
		}else {
			
			int count = 1;
			Node prev = null;
			while(count < pos) {
				prev = curr;
				curr = curr.next;
				count++;
			}
			
			//got the position
			//if last
			if(curr.next == null) {
				curr.prev = curr.next; //setting null. breaking connection
				prev.next = curr.next; //setting null
			}
			else // for middle element 
			{
				prev.next = curr.next;
				curr.next.prev = prev;
				curr.next = null;
				curr.prev = null;
			}
		}
		
		return head;	
	}
	
	
	public static int size(Node head) {
		Node curr = head;
		int s = 0;
		while(curr != null) {
			s++;
			curr = curr.next;
		}
		return s;
	}
	
	public static void print(Node head) {
		if(head == null)
			System.out.println("List is empty.");
		
		while(head != null) {
			System.out.print(head.data+" -> ");
			if(head.prev != null)
				System.out.print(" <- "+ head.prev.data +" ");
			else
				System.out.print(" <- null ");
			head = head.next;
		}

		System.out.println();
	}
	
	
	public static void main(String[] args) {
//		Node node = new Node(6);
		Node head = null;

		head = insertAtHead(head, 3);	
		print(head);
		
		head = insertAtHead(head, 2);	
		print(head);
		
		head = insertAtTail(head, 10);	
		print(head);
		
		head = insertAtPosition(head, 2, 11);
		print(head);
		
		head = insertAtPosition(head, 5, 18);
		print(head);
		
		head = insertAtPosition(head, 1, 1);	
		print(head);
		
		head = deleteAtPosition(head, 1);	
		print(head);
		
		head = deleteAtPosition(head, 5);	
		print(head);
		
		head = deleteAtPosition(head, 2);	
		print(head);
	}

}
