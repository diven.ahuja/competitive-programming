
public class DiameterOfBinaryTree {

	public static int[] negapos(int num[]) {

		int b[] = new int[num.length];
		int index = 0;

		for (int i = 0; i < num.length; i++) {
			if (num[i] >= 0) {
				b[index++] = num[i];

			}
		}
		
		for (int j = 0; j < num.length; j++) {
			if(num[j] < 0) {
				b[index++] = num[j];
			}
		}

		return b;
	}

	public static void main(String[] args) {
		negapos(new int[] { 1, -8, 6, -3, -1, 2, -7 });
	}

}
