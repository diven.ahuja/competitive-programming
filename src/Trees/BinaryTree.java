package Trees;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

class Node {
	int data;
	Node left;
	Node right;

	Node(int data) {
		this.data = data;
		this.left = null;
		this.right = null;
	}

}

public class BinaryTree {
	
	public static Node buildTree(Node root) {
		Scanner myObj = new Scanner(System.in);
		System.out.println("Enter data");
		int data = myObj.nextInt(); // Read user input

		root = new Node(data);

		if (data == -1) {
			return null;
		}

		System.out.println("Enter left child of " + data);
		root.left = buildTree(root.left);

		System.out.println("Enter right child of " + data);
		root.right = buildTree(root.right);

		return root;
	}
	
	public static void levelOrderTraversal(Node root) {
		Queue<Node> q = new ArrayDeque<>();
		q.add(root);
		q.add(new Node(Integer.MIN_VALUE)); //this acts as a separator between two levels
		
		int count = 0;
		while(!q.isEmpty()) {
			
			Node temp = q.peek();
			q.poll();
			
			if(temp.data  == Integer.MIN_VALUE) {
				System.out.println();
				count++;
				if(!q.isEmpty()) {
					q.add(new Node(Integer.MIN_VALUE));
				}
				
			}else {
				System.out.print(temp.data + " ");
				
				if(temp.left != null) {
					q.add(temp.left);
				}
				
				if(temp.right != null) {
					q.add(temp.right);
				}
			}
			
		}
		
	}
	
	public static void preOrderTraversal(Node root) {
		
		if(root == null) {
			return;
		}
		System.out.println(root.data);
		preOrderTraversal(root.left);
		preOrderTraversal(root.right);
	}

	public static void preOrderTraversal_no_recursion(Node root) {
		System.out.println();
		
		if(root == null) {
			return;
		}

		Stack<Node> s = new Stack<>();
		s.add(root);
		
		while(!s.empty()) {
			Node temp = s.pop();
			System.out.print(temp.data  + " ");

			if(temp.right != null) s.push(temp.right);
			if(temp.left != null) s.push(temp.left); 
		}
	}
	
	public static void postOrderTraversal(Node root) {
		
		if(root == null) {
			return;
		}
		
		postOrderTraversal(root.left);
		postOrderTraversal(root.right);
		System.out.print(root.data + " ");
	}

	
	public static int maxDepth_or_height(Node root){

//      with recursion
        
//      if(root == null){
//          return 0;
//      }

//      int lh = maxDepth(root.left);
//      int rh = maxDepth(root.right);
     
//      int ans = Math.max(lh, rh) + 1;
//      return ans;
     
     //without recursion
     
     // it is level order traversal.. incrementing count at new level... so it gives height 
     if(root == null)
         return 0;
     
     	Queue<Node> q = new ArrayDeque<>();
		q.add(root);
		q.add(new Node(Integer.MIN_VALUE)); //this acts as a separator between two levels
		
		int count = 0;
		while(!q.isEmpty()) {
			
			Node temp = q.peek();
			q.poll();
			
			if(temp.data  == Integer.MIN_VALUE) {
				count++;
				if(!q.isEmpty()) {
					q.add(new Node(Integer.MIN_VALUE));
				}
				
			}else {
				
				if(temp.left != null) {
					q.add(temp.left);
				}
				
				if(temp.right != null) {
					q.add(temp.right);
				}
			}
			
		}
     return count;
     
     
     
     // At the bottom it is wrong code but approach i was trying while doing without recursion was that i need to do/maintain two diff for left and right.. my thinking was in the right way a bit not 100% correct but 2% yes. 
     //look at the correct code below where we maintain two diff stacks once for parsing and other stack storing its corresponding height value.
     
//      //base case
//      if(root == null)
//          return 0;
     
//      Stack<Node> s = new Stack<>();
//      Stack<Integer> values = new Stack<>();
     
//      s.push(root);
//      values.push(1);
//      int max =0;
     
//      Node temp = root;
     
//      while(!s.empty()){
         
//          temp = s.pop();
//          int tempValue = values.pop();
//          max = Math.max(tempValue, max);
         
//          if(temp!=null && temp.left!=null) {
//              s.push(temp.left);
//              values.push(tempValue+1);   
//          }
//          if(temp!=null && temp.right!=null){ 
//              s.push(temp.right);
//              values.push(tempValue+1);
//          }
//      }
     
//      return max;
     
     // wrong code
//      if(root == null)
//          return 0;
     
//      List<Node> lefty = new ArrayList<>();
//      List<Node> righty = new ArrayList<>();
     
//      Node temp = root;
//      Node curr = root;
     
//      lefty.add(root);
//      righty.add(root);
     
//      while(curr!=null || temp!=null){
         
//          if(temp!=null && temp.left != null){
//              lefty.add(temp.left);
             
//          }

//          if(curr!=null && curr.right != null){
//              righty.add(curr.right);
             
//          }
//          if(temp!=null)
//          temp = temp.left;
//          if(curr!=null)
//          curr = curr.right;
//      }
     
//      return Math.max(lefty.size(), righty.size());
     
	}
	
	public static void postOrderTraversal_no_recursion(Node root) {
		System.out.println();
		//LRN
		
		if (root == null) {
			return;
		}

		Stack<Node> s = new Stack<>();
		s.push(root);
		
		while(!s.empty()) {
			Node temp = s.peek();
			
			if(temp.right !=null) s.push(temp.right);
			if(temp.left !=null) s.push(temp.left);
			
			if(temp.left == null && temp.right == null) {
				System.out.print(temp.data + " ");
				s.pop();
			}

			temp.left = null;
			temp.right = null;
		}

	//		while(!s.empty()) {
//			Node temp = s.pop();
//			System.out.print(temp.data  + " "); //getting postorder in reverse
//
//			if(temp.left != null) s.push(temp.left); 
//			if(temp.right != null) s.push(temp.right);
//		}
		
	}
	
	public static void inOrderTraversal(Node root) {
		if(root == null) {
			return;
		}
		
		inOrderTraversal(root.left);
		System.out.print(root.data + " ");
		inOrderTraversal(root.right);
	}
	
	public static void inOrderTraversal_no_recursion(Node root) {
		System.out.println();
//		Stack<Node> s = new Stack<>();
//		Node curr = root;
//		
//		while(!s.empty() || curr!=null) {
//			
//	            while (curr != null) {
//	                s.push(curr);
//	                curr = curr.left;
//	            }
//	            Node node = s.pop();
//	            System.out.print(node.data + " ");
//	            curr = node.right;
//		}
		
		Stack<Node> s = new Stack<>();
        Node curr = root;
        
        while(!s.empty() || curr != null){
            
            while(curr !=null){
                s.push(curr);
                curr = curr.left;
            }
            
            curr = s.pop();
//            a.add(curr);
            System.out.print(curr.data + " ");
            curr = curr.right;
            
        }
		
	}
	
	//HW is to do these three traversal without recursion
	// also, learn reverse level order traversal and implement it
	
	public static void main(String[] args) {
		Node root = new Node(1);
		Node three = new Node(3);
		Node seven = new Node(7);
		Node ele = new Node(11);
		Node five = new Node(5);
		Node sevteen = new Node(17);
		
		root.left = three;
		three.left = seven;
		three.right = ele;

		root.right = five;
		five.left = sevteen;
		
		
		
//		1 3 7 -1 -1 11 -1 -1 5 17 -1 -1 -1
//		root = buildTree(root);
//		levelOrderTraversal(root);
//		postOrderTraversal(root);
//		postOrderTraversal_no_recursion(root);
//		preOrderTraversal_no_recursion(root);
//		preOrderTraversal(root);
//		inOrderTraversal(root);
//		inOrderTraversal_no_recursion(root);
//		inOrderTraversal(root);
		System.out.println(maxDepth_or_height(root));
	}
}
