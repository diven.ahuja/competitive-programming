package Trie;

import java.util.ArrayList;
import java.util.Arrays;

public class TrieImplementation {

	static class TrieNode {

		char val;
		TrieNode children[] = new TrieNode[26];
		boolean isTerminal;

		public TrieNode(char ch) {
			val = ch;
			for (int i = 0; i < 26; i++) {
				children[i] = null;
			}
			isTerminal = false;
		}
	}

	static class Trie {

		TrieNode root;

		public Trie() {
			root = new TrieNode('\0');
		}

		public void insert(String w) {
			insertUtil(root, w);
		}

		private void insertUtil(TrieNode root, String w) {
			// base case // meaning string is travelled and last char is marked as terminal
			if (w.length() == 0) {
				root.isTerminal = true;
				return;
			}
			// Pehla character which i want to process.. uska index nikal rahe hai
			int index = w.charAt(0) - 'A';
			TrieNode child;

			// if char present in trie
			if (root.children[index] != null) {
				child = root.children[index];
			} else {
				child = new TrieNode(w.charAt(0));
				root.children[index] = child;
			}
			// recursion
			insertUtil(child, w.substring(1));
		}

		public boolean searchWord(String w) {
			return searchUtil(root, w);
		}

		private boolean searchUtil(TrieNode root, String w) {
			if (w.length() == 0) {
				return root.isTerminal;
			}

			int index = w.charAt(0) - 'A';
			TrieNode child;

			if (root.children[index] != null) {
				child = root.children[index];
			} else {
				return false;
			}

			return searchUtil(child, w.substring(1));
		}

		public boolean removeWord(String w) {
			return removeUtil(root, w);
		}

		private boolean removeUtil(TrieNode root, String w) {
			if (w.length() == 0) {
				if (root.isTerminal) {
					root.isTerminal = false;
					return true;
				}
				return false;
			}

			int index = w.charAt(0) - 'A';

			TrieNode child;

			if (root.children[index] != null) {
				child = root.children[index];
			} else {
				return false;
			}
			boolean res = removeUtil(child, w.substring(1));
//			if(res)					// TODO: currently we are not hard deleting the word.. we keep it in the trie and just mark its terminal as false, so it not found
//				root.children[index] = null;
			return res;
		}

		private boolean prefixUtil(TrieNode root, String word) {
			if (word.length() == 0) {
				return true;
			}

			TrieNode child;
			int index = word.charAt(0) - 'A';

			if (root.children[index] != null) {
				child = root.children[index];
			} else {
				return false;
			}

			return prefixUtil(child, word.substring(1));
		}

		public boolean startsWith(String prefix) {
			return prefixUtil(root, prefix);
		}

	}

	static String res = "";
	static int go = 0;
	static int depth = 0;
	static String og = "";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Trie tn = new Trie();
//		tn.insert("ZEBRA");
//		tn.insert("DOG");
//		tn.insert("DUCK");
//		tn.insert("DOVE");

//		tn.insert("BEARCAT");
//		tn.insert("BERT");
		
		tn.insert("ZEBRA");
		tn.insert("DOG");
		tn.insert("DUCK");
		tn.insert("DOT");
		
		
        ArrayList<String> fin = new ArrayList<>();
        
        for(String word : Arrays.asList("ZEBRA", "DOG", "DUCK", "DOT")){
//        for(String word : Arrays.asList("ZEBRA", "DOG", "DUCK", "DOVE")){
//        for(String word : Arrays.asList("BEARCAT", "BERT")){	
        	og = new String(word);
        	int index = word.charAt(0) - 'A';
            help(tn.root.children[index], word.substring(1));
            fin.add(res);
            res = "";
            go = 0;
            depth=0;
        }
		
        System.out.println(fin);
//		System.out.println(tn.searchWord("TIME"));
//		System.out.println(tn.removeWord("TIME"));
//		System.out.println(tn.searchWord("TIME"));
//		System.out.println(tn.searchWord("TIMZEE"));
	}

	public static void help(TrieNode root, String word) {
		if(root == null || word.length() == 0) {
				res = og.substring(0, go + 1);
			return;
		}
		depth++;
		
		int index = word.charAt(0) - 'A';
		for(int i=0; i<26; i++) {
			if(root.children[index] != null && root.children[i] != null && index != i)
				go = depth;
		}
		
		TrieNode child = null;
		if(root.children[index]!=null) {
			child = root.children[index];
		}
			
		help(child, word.substring(1));
	}
	
}
