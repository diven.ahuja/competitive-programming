
public class Reverse {
	public static void main(String[] args) {
		
		String plain = "sos";
		char[] list = plain.toCharArray();
		int length = list.length;
		String reverse="";
		
		for (int i = length-1; i >= 0; i--) {
			reverse += list[i];
			length--;
		}
		System.out.println(reverse);
	}
}
