package GFG;

public class MinNoOfJumps {

	static int minJumps(int[] arr) {
		int jump = 0;
		int i = 0;

		while (i < arr.length) {

			if (arr[i] == 0 && i != arr.length - 1)
				return -1;

			i = i + arr[i];
			jump++;
		}

		return jump;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		minJumps(new int[] {2, 3, 1, 1, 2, 4, 2, 0, 1, 1});
	}

}
