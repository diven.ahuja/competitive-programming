package Sorting;

import java.util.Random;

public class MergeSort {

	public static void merge(int arr[], int s, int e) {

		int mid = (s+e)/2;
		
		//forming left and right array
		int l1 = mid - s + 1;
		int l2 = e - mid;
		
		int left[] = new int[l1];
		int right[] = new int[l2];
		
		int arrayStartIndex = s;

		int i = 0;
		while(i < l1) {
			left[i++] = arr[arrayStartIndex++];
		}
		
		i = 0;
		while(i < l2) {
			right[i++] = arr[arrayStartIndex++];
		}
		
		//merge two arrays
		i = 0;
		int j=0;
		arrayStartIndex = s;
		
		while(i < l1 && j < l2) {
			if(left[i] < right[j]) {
				arr[arrayStartIndex++] = left[i++];
			}else {
				arr[arrayStartIndex++] = right[j++];
			}
		}
		
		//just in case if any left out elements
		while(i<l1) {
			arr[arrayStartIndex++] = left[i++];
		}
		
		//just in case if any left out elements
		while(j<l2) {
			arr[arrayStartIndex++] = right[j++];
		}
		
		//after sorting
//		for(int num: arr) {
//			System.out.print(num + " ");
//		}
//		System.out.println();
	}
	
	public static void mergeSort(int arr[], int s, int e) {
		
		if(s >= e)
			return;
		
		int mid = (s+e)/2;
		
		// dividing going on
		mergeSort(arr, s, mid);
		mergeSort(arr, mid+1, e);

		//sorting and merging
		merge(arr, s, e);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long startTime = System.nanoTime();
		
		int arr[] = new int[100000];
		
		Random rand = new Random();
		for (int j = 0; j<arr.length; j++)
		{
		    arr[j] = rand.nextInt(arr.length);
		}
		
		mergeSort(arr, 0, arr.length-1);
		
//		for(int num: arr) {
//			System.out.print(num + " ");
//		}
//		System.out.println();
		
		long endTime = System.nanoTime();
		
		System.out.println("Total Time in ms : "+ (endTime - startTime));
	}

}
