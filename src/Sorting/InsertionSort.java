package Sorting;

public class InsertionSort {

	public static void insert(int arr[], int n) {

		for (int i = 1; i < n; i++) {
			int temp = arr[i];
			int j = i - 1;

			for (; j >= 0; j--) {
				if (temp < arr[j]) {
					// shift
					arr[j + 1] = arr[j];
				} else {
					break;
				}
			}

			arr[j + 1] = temp;
		}
		System.out.println(arr);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		insert(new int[] { 4, 1, 3, 9, 7 }, 5);
	}

}
