package DynamicProgramming;

import java.util.ArrayList;
import java.util.Arrays;

public class KnapsackProblem_InterviewBit {

public static int f(int ind, int W, ArrayList<Integer> val, ArrayList<Integer> wt, int[][] dp){
        
        if(ind == 0){
            if(wt.get(0) <= W)
                return val.get(0);
            return 0;
        }
        
        if(dp[ind][W] != -1)
            return dp[ind][W];
        
        int notTake = f(ind - 1, W, val, wt, dp);
        
        int take = Integer.MIN_VALUE;
        
        if(wt.get(ind) <= W){
            take = val.get(ind) + f(ind - 1, W-wt.get(ind), val, wt, dp);
        }
        
        return dp[ind][W] = Math.max(take, notTake);
    }
    
    
    //Top down approach/ Recursion + Memoization
    // public static int solve(ArrayList<Integer> values, ArrayList<Integer> weights, int W) {
    //     int n = weights.size();
        
    //     // Initializing dp array to -1
    //     int[][] dp = new int[n][W+1];
    //     for (int i = 0; i < n; i++) {
    //         Arrays.fill(dp[i], -1);
    //     }
        
    //     return f(n-1, W, values, weights, dp);
    // }
    
    // Tabulation Bottom up approach
    public static int solve(ArrayList<Integer> val, ArrayList<Integer> wt, int W) {
        int n = wt.size();
        
        // Initializing dp array to 0
        int[][] dp = new int[n][W+1];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i], 0);
        }
        
        
        for(int k = wt.get(0); k <= W; k++)
            dp[0][k] = val.get(0);
        
        // just imagine how many options do i have it reaches index 1, which is the last index
        // ans: all from 1 - n, it can be anything 
        for(int ind = 1; ind < n; ind++){
            // just imagine how many option do i have if it reaches weight 0, which is the last index
            // ans : all from 0 to all weights, it can be anything 
            for(int j = 0; j <= W; j++){

                    int notTake = dp[ind - 1][j];
        
                    int take = Integer.MIN_VALUE;
                    
                    if(wt.get(ind) <= j){
                        take = val.get(ind) + dp[ind - 1][ j - wt.get(ind)];
                    }
                    
                    dp[ind][j] = Math.max(take, notTake);
            }
        }
        
        
        return dp[n-1][W];
    }
	
    
    // Tabulation Bottom up approach // Abdul Bari
    public static int solveTab(ArrayList<Integer> val, ArrayList<Integer> wt, int W) {
        
    	int n = wt.size();
    	
    	// adding zero to start of both the arrays for making a proper tabular structure, so this will act as PREV .. 
    	// and the actual 0th row will act as CURR, so we can get 0 values from them in the 
    	// next row.. just in case if we decide not to take that weight.
    	wt.add(0, 0);
    	val.add(0,0);
    	
    	int [][]dp = new int[n+1][W+1];
    	
    	for(int i=0; i<=n; i++) {
    		for(int j=0 ; j<=W; j++) {
    		
    			if(i==0 || j==0)
    				dp[i][j] = 0;
    			
    			else if(wt.get(i) <= j) {
    				// we take max of either prev_row or we calculate current_val + dp[i-1][j-wt[i]] -> if it exists
    				dp[i][j] = Math.max(dp[i-1][j] , val.get(i) + dp[i - 1][j - wt.get(i)] );
    			}
    			else {
    				// we decide not to take.. so we take the element from prev row as is.
    				dp[i][j] = dp[i-1][j];
    			}
    		}
    	}
    	
    	return dp[n][W];
    	
    }
    
	public static void main(String[] args) {
		
		ArrayList<Integer> weights = new ArrayList<Integer>(Arrays.asList( 96, 43, 28, 37, 92, 5, 3, 54, 93));
		ArrayList<Integer> values = new ArrayList<Integer>(Arrays.asList(359, 963, 465, 706, 146, 282, 828, 962, 492 ));
		
		int W = 6;
		
		System.out.println(solveTab(values, weights, 383));
		
	}

}
